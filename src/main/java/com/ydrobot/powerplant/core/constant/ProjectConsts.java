package com.ydrobot.powerplant.core.constant;

/**
 * 项目常量
 */
public final class ProjectConsts {
	public static final String BASE_PACKAGE = "com.ydrobot.powerplant";// 项目基础包名称，根据自己公司的项目修改

	public static final String MODEL_PACKAGE = BASE_PACKAGE + ".model";// Model所在包
	public static final String MAPPER_PACKAGE = BASE_PACKAGE + ".dao";// Mapper所在包
	public static final String SERVICE_PACKAGE = BASE_PACKAGE + ".service";// Service所在包
	public static final String SERVICE_IMPL_PACKAGE = SERVICE_PACKAGE + ".impl";// ServiceImpl所在包
	public static final String CONTROLLER_PACKAGE = BASE_PACKAGE + ".web";// Controller所在包

	public static final String MAPPER_INTERFACE_REFERENCE = BASE_PACKAGE + ".core.mapper.Mapper";// Mapper插件基础接口的完全限定名

	public static final String SECRET = "YDROBOT";

	// 模型级别 1:省份 10:市(地区) 100:供电公司 1000:变电站 1010:变电站分区 1100:间隔 1150:设备类型 1200:设备
	public static final Integer DEVICE_PROVINCE_MODEL_LEVEL = 1;
	public static final Integer DEVICE_CITY_MODEL_LEVEL = 10;
	public static final Integer DEVICE_COMPANY_MODEL_LEVEL = 100;
	public static final Integer DEVICE_STAND_MODEL_LEVEL = 1000;
	public static final Integer DEVICE_AREA_MODEL_LEVEL = 1010;
	public static final Integer DEVICE_INTERVAL_MODEL_LEVEL = 1100;
	public static final Integer DEVICE_TYPE_MODEL_LEVEL = 1150;
	public static final Integer DEVICE_MODEL_LEVEL = 1200;

	// 任务类型 1:历史任务 2:定期任务 3:周期任务 4:间隔任务
	public static final Integer TASK_TYPE_HISTORY = 1;
	public static final Integer TASK_TYPE_REGULAR = 2;
	public static final Integer TASK_TYPE_CYCLE = 3;
	public static final Integer TASK_TYPE_INTERVAL = 4;

	// 资源路径
	public static final String PREFIX_RESOURCE = "opt";
	public static final String PREFIX_CAMERA = "camera_pic";
	public static final String PREFIX_FLIR = "flir_pic";
	public static final String PREFIX_FILE = "file";
	public static final String PREFIX_RECON = "recon_image";

	// 三相状态 1:三相对比报警 0:三相温差
	public static final Byte THREE_PHASE_CONTRAST = 1;
	public static final Byte THREE_PHASE_TEMP_DIF = 0;

	// 审核状态 0:未审核 1:已审核
	public static final Byte CHECK_STATUS_YES = 1;
	public static final Byte CHECK_STATUS_NO = 0;

	// 结果状态 0:正常 1:异常
	public static final Byte RESULT_STATUS_YES = 1;
	public static final Byte RESULT_STATUS_NO = 0;

	// 删除状态 0:未删除 1:已删除
	public static final Byte DELETE_STATUS_YES = 1;
	public static final Byte DELETE_STATUS_NO = 0;

	// 更新状态 1:已更新
	public static final Byte UPDATE_STATUS_YES = 1;
}
