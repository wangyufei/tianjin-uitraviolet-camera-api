package com.ydrobot.powerplant.core.constant;

public class TableNameConsts {

	// 点位报警记录表 pointAlarmHistory
	public static final String SYS_POINT_ALARM_HISTORY = "sys_point_alarm_history";

	// 点位报警记录表 pointAlarmHistory
	public static final String POINT_ALARM_HISTORY = "point_alarm_history";

	// 点位结果记录表 pointHistory
	public static final String POINT_HISTORY = "point_history";

	// 任务表
	public static final String TASK = "task";

	// 任务执行记录表
	public static final String TASK_HISTORY = "task_history";

	// 对象报警记录表 objectAlarmHistory
	public static final String OBJECT_ALARM_HISTORY = "object_alarm_history";

	// 点位评价表
	public static final String POINT_APPRAISE = "point_appraise";

	// 点位评价记录表
	public static final String POINT_APPRAISE_HISTORY = "point_appraise_history";

}
