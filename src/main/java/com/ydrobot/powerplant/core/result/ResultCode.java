package com.ydrobot.powerplant.core.result;

/**
 * 响应码枚举，参考HTTP状态码的语义
 */
public enum ResultCode {
	SUCCESS(1, "SUCCESS"), // 成功
	FAIL(400, "访问失败"), // 失败
	UNAUTHORIZED(401, "token无效"), // 未认证（token错误）
	NOT_FOUND(404, "此接口不存在"), // 接口不存在
	VALID_ERROR(422, "缺少必填参数"), // 缺少必填参数
	INTERNAL_SERVER_ERROR(500, "系统繁忙,请稍后再试"), // 服务器内部错误
	INVALID_PARAM(10000, "参数错误"), // 参数错误
	LOGIN_FAIL(40001, "账号或密码错误"), // 账号或密码错误
	FILE_MAX(40002, "上传的文件太大了"), //
	FILE_EXTS(40003, "上传的文件类型无效"), //
	FILE_SAVE_FAIL(40004, "图片保存失败"), //
	TASK_NOT_FOUND(40402, "任务不存在"), // 任务不存在
	DATA_NOT_FOUND(40403, "数据不存在"), // 数据不存在
	ACCOUNT_EXPIRE(40004, "账号已过期"), // 账号已过期
	ACCOUNT_NOT_FOUND(40005, "账号不存在"), //
	LOGIN_FAIL_NUM_OVERRUN(40006, "登录错误次数超限，请半小时之后再次登录"), // 账号或密码错误;
	ORG_EXIST_USER(40007, "该组织下有用户，不可删除"), // 账号或密码错误;
	SQL_IMPORT_FAIL(40008, "SQL文件导入失败！"), ROUTE_POS_DELETE_FAIL(40009, "原始点无法删除！"),; // ;
	private int code;
	private String message;

	ResultCode(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
}
