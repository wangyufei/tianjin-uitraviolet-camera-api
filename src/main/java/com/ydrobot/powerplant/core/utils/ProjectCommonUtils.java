package com.ydrobot.powerplant.core.utils;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;

public class ProjectCommonUtils {

	/**
	 * 获取拆分之后的List
	 * 
	 * @param str
	 * @param regex
	 * @return
	 */
	public static List<String> getSplitVal(String str, String regex) {
		List<String> list = new ArrayList<String>();

		if (str == null) {
			return list;
		}

		String strArr[] = str.split(regex);
		for (String val : strArr) {
			list.add(val);
		}

		return list;
	}

	/**
	 * 获取拆分之后的List
	 * 
	 * @param str
	 * @param regex
	 * @return
	 */
	public static List<Integer> getSplitValInt(String str, String regex) {
		List<Integer> list = new ArrayList<Integer>();

		if (str == null) {
			return list;
		}

		String strArr[] = str.split(regex);
		for (String val : strArr) {
			list.add(Integer.valueOf(val));
		}

		return list;
	}

	/**
	 * 移除字符串最后一个逗号
	 * 
	 * @param str
	 * @return
	 */
	public static String removeExtraComma(String str) {
		if (str == "") {
			return str;
		}
		return str.substring(0, str.length() - 1);
	}

	/**
	 * 获取ip地址
	 * 
	 * @param request
	 * @return
	 */
	public static String getIpAddress(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		// 如果是多级代理，那么取第一个ip为客户端ip
		if (ip != null && ip.indexOf(",") != -1) {
			ip = ip.substring(0, ip.indexOf(",")).trim();
		}

		return ip;
	}

	/**
	 * 通过时间进行排序
	 * 
	 * @param list
	 */
	public static void listSortByTime(List<Map<String, Object>> list) {
		Collections.sort(list, new Comparator<Map<String, Object>>() {
			@Override
			public int compare(Map<String, Object> o1, Map<String, Object> o2) {
				try {
					Date dt1 = (Date) o1.get("executTime");
					Date dt2 = (Date) o2.get("executTime");
					if (dt1.getTime() > dt2.getTime()) {
						return 1;
					} else if (dt1.getTime() < dt2.getTime()) {
						return -1;
					} else {
						return 0;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return 0;
			}
		});
	}

	/**
	 * base64字符串转换成图片
	 * 
	 * @param imgStr
	 *            base64字符串
	 * @param imgFilePath
	 *            图片存放路径
	 * @return
	 */
	public static boolean Base64ToImage(String imgStr, String imgFilePath) {

		if (StringUtils.isEmpty(imgStr)) {
			return false;
		}

		Base64 base64 = new Base64();
		try {
			@SuppressWarnings("static-access")
			byte[] b = base64.decodeBase64(imgStr.getBytes());
			for (int i = 0; i < b.length; ++i) {
				if (b[i] < 0) {// 调整异常数据
					b[i] += 256;
				}
			}

			OutputStream out = new FileOutputStream(imgFilePath);
			out.write(b);
			out.flush();
			out.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * 点到直线的最短距离的判断 点（x0,y0） 到由两点组成的线段（x1,y1） ,( x2,y2 )
	 * 
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param x0
	 * @param y0
	 * @return
	 */
	public static boolean pointToLine(Float x1, Float y1, Float x2, Float y2, Float x0, Float y0) {
		boolean bRange = false;
		double a = lineSpace(x1, y1, x2, y2);// 线段的长度
		
		// P点是否在线S,E段中    
	    double r = ((x0 - x1) * (x2 - x1) + (x0 - y1) * (y2 - y1)) / Math.pow(a, 2);
	    
	    if (r > 0 && r < 1){
	        bRange = true;
	    }
	    
	    return bRange;
	}

	/**
	 * 计算两点之间的距离
	 * 
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @return
	 */
	private static double lineSpace(Float x1, Float y1, Float x2, Float y2) {
		double lineLength = 0;
		lineLength = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
		return lineLength;
	}
}
