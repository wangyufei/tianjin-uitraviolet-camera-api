package com.ydrobot.powerplant.core.utils;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ProjectSqlUtils {

	@Value("${jdbc.username}")
	private String username;

	@Value("${jdbc.password}")
	private String password;

	@Value("${jdbc.host}")
	private String host;

	@Value("${jdbc.port}")
	private String port;

	@Value("${jdbc.importDatabaseName}")
	private String importDatabaseName;

	/**
	 * 根据属性文件的配置把指定位置的指定文件内容导入到指定的数据库中 在命令窗口进行mysql的数据库导入一般分三步走： 第一步是登到到mysql； mysql
	 * -uusername -ppassword -hhost -Pport -DdatabaseName;如果在登录的时候指定了数据库名则会
	 * 直接转向该数据库，这样就可以跳过第二步，直接第三步； 第二步是切换到导入的目标数据库；use importDatabaseName；
	 * 第三步是开始从目标文件导入数据到目标数据库；source importPath；
	 * 
	 * @param properties
	 * @throws IOException
	 */
	public void importSql(String sqlFilePath) throws IOException {
		Runtime runtime = Runtime.getRuntime();
		// 因为在命令窗口进行mysql数据库的导入一般分三步走，所以所执行的命令将以字符串数组的形式出现
		String cmdarray[] = getImportCommand(sqlFilePath);// 根据属性文件的配置获取数据库导入所需的命令，组成一个数组
		// runtime.exec(cmdarray);//这里也是简单的直接抛出异常
		Process process = runtime.exec(cmdarray[0]);
		// 执行了第一条命令以后已经登录到mysql了，所以之后就是利用mysql的命令窗口
		// 进程执行后面的代码
		OutputStream os = process.getOutputStream();
		OutputStreamWriter writer = new OutputStreamWriter(os);
		// 命令1和命令2要放在一起执行
		writer.write(cmdarray[1] + "\r\n" + cmdarray[2]);
		writer.flush();
		writer.close();
		os.close();
	}

	/**
	 * 根据属性文件的配置，分三步走获取从目标文件导入数据到目标数据库所需的命令 如果在登录的时候指定了数据库名则会
	 * 直接转向该数据库，这样就可以跳过第二步，直接第三步；
	 * 
	 * @param properties
	 * @return
	 */
	private String[] getImportCommand(String sqlFilePath) {
		// 第一步，获取登录命令语句
		String loginCommand = new StringBuffer().append("mysql -u").append(username).append(" -p").append(password)
				.append(" -h").append(host).append(" -P").append(port).toString();
		// 第二步，获取切换数据库到目标数据库的命令语句
		String switchCommand = new StringBuffer("use ").append(importDatabaseName).toString();
		// 第三步，获取导入的命令语句
		String importCommand = new StringBuffer("source ").append(sqlFilePath).toString();
		// 需要返回的命令语句数组
		String[] commands = new String[] { loginCommand, switchCommand, importCommand };
		return commands;
	}
}
