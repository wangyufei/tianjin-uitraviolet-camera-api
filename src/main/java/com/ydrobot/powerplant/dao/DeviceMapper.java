package com.ydrobot.powerplant.dao;

import java.util.List;

import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.Device;

public interface DeviceMapper extends Mapper<Device> {
	
	/**
	 * 获取变电站设备树
	 * 
	 * @param statModelLevel
	 * @return
	 */
	@Select("select id,name,model_level,parent_id from device "
			+ "where model_level >= 1000 order by name asc")
	@ResultMap("com.ydrobot.powerplant.dao.DeviceMapper.BaseResultMap")
	List<Device> getStationTree();
	
	/**
	 * 删除所有数据
	 */
	@Select("delete * from device")
	void deleteAll();
}