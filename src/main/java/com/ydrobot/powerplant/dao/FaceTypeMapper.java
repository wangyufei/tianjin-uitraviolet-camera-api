package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.FaceType;

public interface FaceTypeMapper extends Mapper<FaceType> {
}