package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.FullInspectStopPoint;

public interface FullInspectStopPointMapper extends Mapper<FullInspectStopPoint> {
}