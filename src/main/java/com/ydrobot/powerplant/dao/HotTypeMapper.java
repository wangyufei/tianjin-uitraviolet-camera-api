package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.HotType;

public interface HotTypeMapper extends Mapper<HotType> {
}