package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.InspectTypeChoose;

public interface InspectTypeChooseMapper extends Mapper<InspectTypeChoose> {
}