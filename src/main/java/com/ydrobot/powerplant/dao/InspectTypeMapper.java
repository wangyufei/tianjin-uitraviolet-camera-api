package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.InspectType;

public interface InspectTypeMapper extends Mapper<InspectType> {
}