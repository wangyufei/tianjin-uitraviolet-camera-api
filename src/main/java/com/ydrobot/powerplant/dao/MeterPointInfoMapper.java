package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.MeterPointInfo;

public interface MeterPointInfoMapper extends Mapper<MeterPointInfo> {
}