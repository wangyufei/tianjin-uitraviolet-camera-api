package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.MeterType;

public interface MeterTypeMapper extends Mapper<MeterType> {
}