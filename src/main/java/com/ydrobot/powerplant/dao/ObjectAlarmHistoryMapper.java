package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.ObjectAlarmHistory;

public interface ObjectAlarmHistoryMapper extends Mapper<ObjectAlarmHistory> {
}