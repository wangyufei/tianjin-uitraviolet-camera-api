package com.ydrobot.powerplant.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.Permission;
import com.ydrobot.powerplant.model.condition.PermissionQueryParam;

public interface PermissionMapper extends Mapper<Permission> {
	
	/**
	 * 获取权限列表
	 * @param queryParam
	 * @return
	 */
	public List<Permission> getList(@Param("queryParam") PermissionQueryParam queryParam);
}