package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.PermissionRole;

public interface PermissionRoleMapper extends Mapper<PermissionRole> {
}