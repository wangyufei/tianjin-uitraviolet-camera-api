package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.PmsDevice;

public interface PmsDeviceMapper extends Mapper<PmsDevice> {
}