package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.PmsDeviceScada;

public interface PmsDeviceScadaMapper extends Mapper<PmsDeviceScada> {
}