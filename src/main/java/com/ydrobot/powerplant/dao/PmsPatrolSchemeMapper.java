package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.PmsPatrolScheme;

public interface PmsPatrolSchemeMapper extends Mapper<PmsPatrolScheme> {
}