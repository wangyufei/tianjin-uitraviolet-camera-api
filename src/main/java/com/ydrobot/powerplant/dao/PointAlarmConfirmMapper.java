package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.PointAlarmConfirm;

public interface PointAlarmConfirmMapper extends Mapper<PointAlarmConfirm> {
}