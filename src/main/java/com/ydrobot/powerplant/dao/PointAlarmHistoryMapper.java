package com.ydrobot.powerplant.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.Point;
import com.ydrobot.powerplant.model.PointAlarmHistory;
import com.ydrobot.powerplant.model.condition.PointAlarmHistoryQueryParam;
import com.ydrobot.powerplant.model.dto.PointAlarmHistoryResult;

public interface PointAlarmHistoryMapper extends Mapper<PointAlarmHistory> {

	/**
	 * 获取点位报警记录列表
	 * @param queryParam
	 * @return
	 */
	List<PointAlarmHistoryResult> getList(@Param("queryParam") PointAlarmHistoryQueryParam queryParam);
	
    /**
     * 获取任务执行记录的正常点位数
     * @param taskHistoryId
     * @return
     */
    @Select("select count(*) from point_history "
    		+ "where task_history_id = #{taskHistoryId} "
    		+ "and recon_status = 0 "
    		+ "and id not in (select distinct point_history_id as point_history_id from point_alarm_history where task_history_id = #{taskHistoryId} and result_status = 1)")
    int getNormalPointNum(@Param("taskHistoryId") Integer taskHistoryId);

    /**
     * 获取报警点数
     * @param taskHistoryId
     * @return
     */
    @Select("select count(DISTINCT point_history_id) from point_alarm_history where task_history_id = #{taskHistoryId}  and result_status = 1")
    int getAlarmPointNum(@Param("taskHistoryId") Integer taskHistoryId);

    /**
     * 获取某个任务的所有报警点数
     * @param taskHistoryId
     * @return
     */
    @Select("select count(*) from point_alarm_history where task_history_id = #{taskHistoryId}  and result_status = 1")
    int getAllAlarmPointNum(@Param("taskHistoryId") Integer taskHistoryId);

    /**
     * 获取结果错误点数
     * @param taskHistoryId
     * @return
     */
    @Select("select count(*) from point_alarm_history where point_history_id = #{pointHistoryId} and result_status = 1")
    int getResultErrorPointNum(@Param("pointHistoryId") Integer pointHistoryId);

    /**
     * 获取异常点数
     * @param taskHistoryId
     * @return
     */
    @Select("select count(DISTINCT point_id) as num from point_history where task_history_id = #{taskHistoryId} and recon_status = 1")
    int getAbnormalPointNum(@Param("taskHistoryId") Integer taskHistoryId);

    /**
     * 获取某条点位历史的最大报警等级
     * @param pointHistoryId
     * @return
     */
    @Select("select * from point_alarm_history where point_history_id = #{pointHistoryId} order by alarm_level desc limit 1")
    @ResultMap("com.ydrobot.powerplant.dao.PointAlarmHistoryMapper.BaseResultMap")
    PointAlarmHistory getMaxAlarmLevel(@Param("pointHistoryId") Integer pointHistoryId);

    /**
     * 获取多条点位历史的最大报警等级
     * @param pointHistoryId
     * @return
     */
    @Select("select * from point_alarm_history where point_history_id in (${pointHistoryIds}) order by alarm_level desc limit 1")
    @ResultMap("com.ydrobot.powerplant.dao.PointAlarmHistoryMapper.BaseResultMap")
    PointAlarmHistory getMaxAlarmLevelByManyPointHistory(@Param("pointHistoryIds") String pointHistoryIds);
    
    /**
     * 获取二次确认任务执行记录id
     * @param pointAlarmHistoryId
     * @return
     */
    @Select("select th.id from point_alarm_confirm as pac left join task_history as th on pac.task_id = th.task_id where point_alarm_history_id = #{pointAlarmHistoryId} order by pac.id desc limit 1")
    Integer getPointAlarmConfirmTaskHistoryId(@Param("pointAlarmHistoryId") Integer pointAlarmHistoryId);
    
    /**
     * 获取二次确认任务id
     * @param pointAlarmHistoryId
     * @return
     */
    @Select("select task_id from point_alarm_confirm where point_alarm_history_id = #{pointAlarmHistoryId} order by id desc limit 1")
    Integer getPointAlarmConfirmTaskId(@Param("pointAlarmHistoryId") Integer pointAlarmHistoryId);
    
    /**
     * 通过报警记录id获取部件信息
     * @param pointAlarmHistoryId
     * @return
     */
    @Select("select p.* from point_alarm_history as pah left join point as p on pah.point_id = p.id where pah.id = #{pointAlarmHistoryId}")
    @ResultMap("com.ydrobot.powerplant.dao.PointMapper.BaseResultMap")
    Point getPointByPointAlarmHistoryId(@Param("pointAlarmHistoryId") Integer pointAlarmHistoryId);
    
    /**
     * 通过任务记录id和对象id获取报警数
     * @param taskHistoryId
     * @param objectId
     * @return
     */
    @Select("select count(*) from point_alarm_history as pah "
    		+ "left join point as p on p.id = pah.point_id "
    		+ "where pah.task_history_id = #{taskHistoryId}  and pah.result_status = 1 and p.object_id = #{objectId}")
    Integer getAlarmNumByTaskHistoryIdAndObjectId(@Param("taskHistoryId") Integer taskHistoryId,@Param("objectId") Integer objectId);
}