package com.ydrobot.powerplant.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.PointAppraiseHistory;
import com.ydrobot.powerplant.model.condition.PointAppraiseHistoryQueryParam;
import com.ydrobot.powerplant.model.dto.PointAppraiseHistoryResult;

public interface PointAppraiseHistoryMapper extends Mapper<PointAppraiseHistory> {
	
	/**
	 * 获取评价记录列表
	 * @param queryParam
	 * @return
	 */
	public List<PointAppraiseHistoryResult> getList(@Param("queryParam") PointAppraiseHistoryQueryParam queryParam);
	
	/**
	 * 获取某个对象的最近一次异常部件评价记录
	 * @param objectId
	 * @return
	 */
	@Select("select * from (select * from (select pah.* from point_appraise_history as pah left join point as p on pah.point_id = p.id where p.object_id = #{objectId} order by pah.appraise_time desc limit 100000) as r group by r.point_id) as t where t.appraise_status!=0 limit 1 ")
	@ResultMap("com.ydrobot.powerplant.dao.PointAppraiseHistoryMapper.BaseResultMap")
	public PointAppraiseHistory getObjectLastPointAppraiseHistory(@Param("objectId") Integer objectId);
}