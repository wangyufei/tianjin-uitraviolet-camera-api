package com.ydrobot.powerplant.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.PointAppraise;
import com.ydrobot.powerplant.model.dto.PointAppraiseResult;

public interface PointAppraiseMapper extends Mapper<PointAppraise> {
	
	/**
	 * 通过对象id获取部件评价列表
	 * @param objectId
	 * @return
	 */
	List<PointAppraiseResult> getPointAppraiseByObjectId(@Param("objectId") Integer objectId);
}