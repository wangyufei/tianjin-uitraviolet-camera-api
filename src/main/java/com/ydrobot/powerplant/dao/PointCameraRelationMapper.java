package com.ydrobot.powerplant.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.PointCameraRelation;
import com.ydrobot.powerplant.model.condition.PointCameraQueryParam;
import com.ydrobot.powerplant.model.dto.PointCameraResult;

public interface PointCameraRelationMapper extends Mapper<PointCameraRelation> {
	
	/**
	 * 获取点位摄像头关系列表
	 * @param queryParam
	 * @return
	 */
	List<PointCameraResult> getList(@Param("queryParam") PointCameraQueryParam queryParam);
}