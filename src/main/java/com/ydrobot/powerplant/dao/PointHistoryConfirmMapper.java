package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.PointHistoryConfirm;

public interface PointHistoryConfirmMapper extends Mapper<PointHistoryConfirm> {
}