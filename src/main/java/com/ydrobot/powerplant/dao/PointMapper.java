package com.ydrobot.powerplant.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.Point;
import com.ydrobot.powerplant.model.dto.AbnormalAppraisePointResult;
import com.ydrobot.powerplant.model.dto.PointResult;

public interface PointMapper extends Mapper<Point> {
	
	/**
	 * 获取点详情
	 * @param pointId
	 * @return
	 */
	PointResult getDetail(@Param("pointId") Integer pointId);
	
    /**
     * 获取正常点
     * @return
     */
    @Select("select id,name,object_id,is_use,recon_type_id,meter_type_id,face_type_id,max_alarm_level,unit from point where is_delete = 0")
    @ResultMap("com.ydrobot.powerplant.dao.PointMapper.BaseResultMap")
    List<Point> getNormalPoint();

    /**
     * 获取异常点
     * @return
     */
    @Select("select * from point where is_delete = 0 and is_abnormal = 1")
    @ResultMap("com.ydrobot.powerplant.dao.PointMapper.BaseResultMap")
    List<Point> getAbnormalPoint();

    /**
     * 通过设备id集合获取最大报警等级
     * @param deviceIds
     * @return
     */
    @Select("select max(max_alarm_level) as count from point where object_id in (${deviceIds})")
    Integer getMaxAlarmLevelByDeviceIds(@Param("deviceIds") String deviceIds);
    
    /**
     * 通过设备间隔id获取最大报警等级
     * @param deviceIntervalId
     * @return
     */
    @Select("select max(max_alarm_level) as count from point as p left join object as o on p.object_id = o.id where device_id = #{deviceIntervalId}")
    Integer getMaxAlarmLevelByDeviceIntervalId(@Param("deviceIntervalId") Integer deviceIntervalId);
    
    /**
     * 通过对象id获取最大报警等级
     * @param objectId
     * @return
     */
    @Select("select max(max_alarm_level) as count from point where object_id = #{objectId}")
    Integer getMaxAlarmLevelByObjectId(@Param("objectId") Integer objectId);

    /**
     * 获取任务巡检点
     * @param taskId
     * @return
     */
    @Select("select p.* from point as p left join task_point as tp on p.id=tp.point_id where tp.task_id=#{taskId}")
    @ResultMap("com.ydrobot.powerplant.dao.PointMapper.BaseResultMap")
    List<Point> getTaskPoint(@Param("taskId") Integer taskId);

    /**
     * 获取对象列表
     * @return
     */
    @Select("select id,object_sub_id,object_id from point where type = 1")
    @ResultMap("com.ydrobot.powerplant.dao.PointMapper.BaseResultMap")
    List<Point> getObjectList();
    
    /**
     * 获取异常评价组件列表
     * @return
     */
    List<AbnormalAppraisePointResult> getAbnormalAppraisePoint();
}