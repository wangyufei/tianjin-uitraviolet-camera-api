package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.PointRelationSetting;

public interface PointRelationSettingMapper extends Mapper<PointRelationSetting> {
}