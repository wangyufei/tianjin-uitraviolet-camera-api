package com.ydrobot.powerplant.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.PointWatchPoint;

public interface PointWatchPointMapper extends Mapper<PointWatchPoint> {
	
	/**
	 * 通过组件id删除组件与观察点位的关系
	 * @param pointId
	 * @return
	 */
	@Select("delete from point_watch_point where point_id = #{pointId}")
	int deleteByPointId(@Param("pointId") Integer pointId);
}