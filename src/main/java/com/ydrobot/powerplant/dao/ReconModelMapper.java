package com.ydrobot.powerplant.dao;

import java.util.List;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.ReconModel;
import com.ydrobot.powerplant.model.dto.ReconModelResult;

public interface ReconModelMapper extends Mapper<ReconModel> {

	/**
	 * 获取识别模型列表
	 * 
	 * @return
	 */
	List<ReconModelResult> getList();
}