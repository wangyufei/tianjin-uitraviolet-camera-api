package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.ReconModelSub;

public interface ReconModelSubMapper extends Mapper<ReconModelSub> {
}