package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.ReconType;

public interface ReconTypeMapper extends Mapper<ReconType> {
}