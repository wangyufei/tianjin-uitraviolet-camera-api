package com.ydrobot.powerplant.dao;

import java.util.List;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.Robot;
import com.ydrobot.powerplant.model.dto.RobotResult;

public interface RobotMapper extends Mapper<Robot> {
	
	/**
	 * 获取机器人列表
	 * @return
	 */
	List<RobotResult> getList();
	
}