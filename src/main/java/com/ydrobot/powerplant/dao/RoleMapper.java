package com.ydrobot.powerplant.dao;

import java.util.List;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.Role;
import com.ydrobot.powerplant.model.dto.RoleResult;

public interface RoleMapper extends Mapper<Role> {
	
	/**
	 * 获取角色列表
	 * @return
	 */
	List<RoleResult> getList();
}