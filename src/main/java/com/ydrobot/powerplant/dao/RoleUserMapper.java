package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.RoleUser;

public interface RoleUserMapper extends Mapper<RoleUser> {
}