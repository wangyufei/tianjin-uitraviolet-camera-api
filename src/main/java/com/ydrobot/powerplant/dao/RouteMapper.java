package com.ydrobot.powerplant.dao;

import java.util.List;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.Route;
import com.ydrobot.powerplant.model.dto.RouteResult;

public interface RouteMapper extends Mapper<Route> {
	
	/**
	 * 获取道路列表
	 * @return
	 */
	List<RouteResult> getList();
}