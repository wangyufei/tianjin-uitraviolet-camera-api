package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.RoutePos;

public interface RoutePosMapper extends Mapper<RoutePos> {
}