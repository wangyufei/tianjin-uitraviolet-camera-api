package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.SysAlarmType;

public interface SysAlarmTypeMapper extends Mapper<SysAlarmType> {
}