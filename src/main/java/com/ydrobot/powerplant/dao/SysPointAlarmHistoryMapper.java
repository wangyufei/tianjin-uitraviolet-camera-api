package com.ydrobot.powerplant.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.SysPointAlarmHistory;
import com.ydrobot.powerplant.model.condition.SysPointAlarmHistoryQueryParam;
import com.ydrobot.powerplant.model.dto.SysPointAlarmHistoryResult;

public interface SysPointAlarmHistoryMapper extends Mapper<SysPointAlarmHistory> {
	
	/**
	 * 获取系统点位报警记录
	 * @param queryParam
	 * @return
	 */
	List<SysPointAlarmHistoryResult> getList(@Param("queryParam") SysPointAlarmHistoryQueryParam queryParam);
}