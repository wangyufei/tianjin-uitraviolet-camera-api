package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.SysPointAlarmSetting;

public interface SysPointAlarmSettingMapper extends Mapper<SysPointAlarmSetting> {
}