package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.SysPoint;

public interface SysPointMapper extends Mapper<SysPoint> {
}