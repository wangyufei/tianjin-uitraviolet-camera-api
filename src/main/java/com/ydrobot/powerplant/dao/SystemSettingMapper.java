package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.SystemSetting;

public interface SystemSettingMapper extends Mapper<SystemSetting> {
}