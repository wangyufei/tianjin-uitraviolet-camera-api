package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.TaskCycle;

public interface TaskCycleMapper extends Mapper<TaskCycle> {
}