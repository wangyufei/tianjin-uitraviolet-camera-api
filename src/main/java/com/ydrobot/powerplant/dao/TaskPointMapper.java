package com.ydrobot.powerplant.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.TaskPoint;

public interface TaskPointMapper extends Mapper<TaskPoint> {

    @Select("select DISTINCT object_id from task_point where task_id = #{taskId}")
    @ResultMap("com.ydrobot.powerplant.dao.TaskPointMapper.BaseResultMap")
    List<TaskPoint> findByTaskId(@Param("taskId") Integer taskId);
}