package com.ydrobot.powerplant.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.TaskRegular;

public interface TaskRegularMapper extends Mapper<TaskRegular> {

    @Select("select * from task_regular where robot_id = #{robotId} and execute_time >= #{startTime} and execute_time < #{endTime}")
    @ResultMap("com.ydrobot.powerplant.dao.TaskRegularMapper.BaseResultMap")
    List<TaskRegular> findByRobotIdAndTaskStartDateBetween(@Param("robotId") Integer robotId,
            @Param("startTime") String startTime, @Param("endTime") String endTime);
}