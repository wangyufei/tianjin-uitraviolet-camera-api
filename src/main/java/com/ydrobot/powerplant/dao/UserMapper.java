package com.ydrobot.powerplant.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.Permission;
import com.ydrobot.powerplant.model.User;
import com.ydrobot.powerplant.model.condition.UserQueryParam;
import com.ydrobot.powerplant.model.dto.UserResult;

public interface UserMapper extends Mapper<User> {
	
	/**
	 * 获取用户列表
	 * @param queryParam
	 * @param organizationIdList
	 * @return
	 */
	List<UserResult> getList(@Param("queryParam") UserQueryParam queryParam, @Param("organizationIdList") List<Integer> organizationIdList);
	
	/**
	 * 获取用户权限列表
	 * @param userId
	 * @return
	 */
	List<Permission> getUserPermissions(@Param("userId") Integer userId);
}