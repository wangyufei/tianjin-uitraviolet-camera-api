package com.ydrobot.powerplant.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.WatchPoint;

public interface WatchPointMapper extends Mapper<WatchPoint> {
	
	/**
	 * 通过点位id获取的观察点
	 * @param pointId
	 * @return
	 */
	@Select("select wp.* from watch_point as wp left join point_watch_point as pwp on wp.id = pwp.watch_point_id where pwp.point_id = #{pointId}")
	@ResultMap("com.ydrobot.powerplant.dao.WatchPointMapper.BaseResultMap")
	List<WatchPoint> findByPointId(@Param("pointId") Integer pointId);
	
}