package com.ydrobot.powerplant.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.ydrobot.powerplant.common.ThreadCache;
import com.ydrobot.powerplant.core.constant.ProjectConsts;
import com.ydrobot.powerplant.core.utils.ProjectMD5Utils;
import com.ydrobot.powerplant.model.User;
import com.ydrobot.powerplant.service.UserService;

/**
 * Created by Wyf on 2019/04/12.
 */
public class UiLoginFilter extends BaseFilter implements Filter {

	private String[] prefixIignores = { "/ui/users/login", "/ui/task-historys/exports", "/ui/files",
			"/ui/files/base64image", "/ui/task-historys/point-cloud-object-data", "/ui/task-historys/points" };

	@Autowired
	private UserService userService;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;

		if (canIgnore(request,response)) {
			filterChain.doFilter(request, response);
			return;
		}

		sessionHandle(filterChain, request, response);
	}

	private void sessionHandle(FilterChain filterChain, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		String token = request.getHeader("token");
		User user = null;
		if (StringUtils.isNotBlank(token)) {
			user = userService.getRedisUserByToken(token);
			if (user != null) {
				ThreadCache.setUser(user);// 线程缓层当前用户
			}
		}
		if (user != null) {
			filterChain.doFilter(request, response);
		} else {
			writeExpirie(response, null);
		}
	}

	@Override
	public void destroy() {

	}

	private boolean canIgnore(HttpServletRequest request,HttpServletResponse response) throws IOException {
		boolean isExcludedPage = false;
		// 判断是否在过滤url之外
		for (String page : prefixIignores) {
			if ((request.getServletPath().contains(page))) {
				isExcludedPage = true;
				break;
			}
		}

		// 判断是否是智慧云台发出的请求
		String sign = request.getHeader("sign");
		String timestamp = request.getHeader("timestamp");

		if (sign != null && timestamp != null) {
			String signnew = ProjectMD5Utils.getMD5String(ProjectConsts.SECRET + "&&" + timestamp);
			if (signnew.equals(sign)) {
				isExcludedPage = true;
				// 设置默认登录用户
				if (ThreadCache.getUser() == null) {
					ThreadCache.setUser(userService.getUserByAccount("admin"));
				}
			}else {
				writeFail(response, "签名错误!");
			}
		}

		return isExcludedPage;
	}
}
