package com.ydrobot.powerplant.model;

import javax.persistence.*;

@Table(name = "alarm_type")
public class AlarmType {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 名称 key
     */
    private String name;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取名称 key
     *
     * @return name - 名称 key
     */
    public String getName() {
        return name;
    }

    /**
     * 设置名称 key
     *
     * @param name 名称 key
     */
    public void setName(String name) {
        this.name = name;
    }
}