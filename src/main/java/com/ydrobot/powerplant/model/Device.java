package com.ydrobot.powerplant.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.ydrobot.powerplant.model.dto.PointTreeInfoResult;

public class Device {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 模型级别 1:省份,10:市(地区),100:供电公司,1000:变电站,1010:变电站分区,1100:间隔，1150:设备类型，1200:小类设备
     */
    @Column(name = "model_level")
    private Integer modelLevel;

    /**
     * 父类id 0:根节点
     */
    @Column(name = "parent_id")
    private Integer parentId;

    @Transient
    private List<Device> children;

    @Transient
    private List<PointTreeInfoResult> points;
    
    @Transient
    private Integer realId;

    @Transient
    private Integer alarmLevel = 0;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取名称
     *
     * @return name - 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置名称
     *
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取模型级别 1:省份,10:市(地区),100:供电公司,1000:变电站,1010:变电站分区,1100:间隔，1150:设备类型，1200:小类设备
     *
     * @return model_level - 模型级别 1:省份,10:市(地区),100:供电公司,1000:变电站,1010:变电站分区,1100:间隔，1150:设备类型，1200:小类设备
     */
    public Integer getModelLevel() {
        return modelLevel;
    }

    /**
     * 设置模型级别 1:省份,10:市(地区),100:供电公司,1000:变电站,1010:变电站分区,1100:间隔，1150:设备类型，1200:小类设备
     *
     * @param modelLevel 模型级别 1:省份,10:市(地区),100:供电公司,1000:变电站,1010:变电站分区,1100:间隔，1150:设备类型，1200:小类设备
     */
    public void setModelLevel(Integer modelLevel) {
        this.modelLevel = modelLevel;
    }

    /**
     * 获取父类id 0:根节点
     *
     * @return parent_id - 父类id 0:根节点
     */
    public Integer getParentId() {
        return parentId;
    }

    /**
     * 设置父类id 0:根节点
     *
     * @param parentId 父类id 0:根节点
     */
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public List<Device> getChildren() {
        return children;
    }

    public void setChildren(List<Device> children) {
        this.children = children;
    }

    public List<PointTreeInfoResult> getPoints() {
		return points;
	}

	public void setPoints(List<PointTreeInfoResult> points) {
		this.points = points;
	}

	public Integer getAlarmLevel() {
        return alarmLevel;
    }

    public void setAlarmLevel(Integer alarmLevel) {
        this.alarmLevel = alarmLevel;
    }

	public Integer getRealId() {
		return realId;
	}

	public void setRealId(Integer realId) {
		this.realId = realId;
	}
}