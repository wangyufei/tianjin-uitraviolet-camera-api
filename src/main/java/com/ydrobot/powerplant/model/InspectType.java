package com.ydrobot.powerplant.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Table(name = "inspect_type")
public class InspectType {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 等级
     */
    private Integer level;

    /**
     * 父级id，根节点0
     */
    @Column(name = "parent_id")
    private Integer parentId;

    @Transient
    private List<InspectType> children;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取名称
     *
     * @return name - 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置名称
     *
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取等级
     *
     * @return level - 等级
     */
    public Integer getLevel() {
        return level;
    }

    /**
     * 设置等级
     *
     * @param level 等级
     */
    public void setLevel(Integer level) {
        this.level = level;
    }

    /**
     * 获取父级id，根节点0
     *
     * @return parent_id - 父级id，根节点0
     */
    public Integer getParentId() {
        return parentId;
    }

    /**
     * 设置父级id，根节点0
     *
     * @param parentId 父级id，根节点0
     */
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public List<InspectType> getChildren() {
        return children;
    }

    public void setChildren(List<InspectType> children) {
        this.children = children;
    }

}