package com.ydrobot.powerplant.model;

import javax.persistence.*;

@Table(name = "inspect_type_choose")
public class InspectTypeChoose {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 巡检类型id
     */
    @Column(name = "inspect_type_id")
    private Integer inspectTypeId;

    /**
     * 筛选条件id
     */
    @Column(name = "choose_id")
    private Integer chooseId;

    /**
     * 筛选类型 0:设备区域、1:设备类型、2:识别类型，3:表计类型，4:设备外观类型
     */
    @Column(name = "choose_type")
    private Byte chooseType;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取巡检类型id
     *
     * @return inspect_type_id - 巡检类型id
     */
    public Integer getInspectTypeId() {
        return inspectTypeId;
    }

    /**
     * 设置巡检类型id
     *
     * @param inspectTypeId 巡检类型id
     */
    public void setInspectTypeId(Integer inspectTypeId) {
        this.inspectTypeId = inspectTypeId;
    }

    /**
     * 获取筛选条件id
     *
     * @return choose_id - 筛选条件id
     */
    public Integer getChooseId() {
        return chooseId;
    }

    /**
     * 设置筛选条件id
     *
     * @param chooseId 筛选条件id
     */
    public void setChooseId(Integer chooseId) {
        this.chooseId = chooseId;
    }

    /**
     * 获取筛选类型 0:设备区域、1:设备类型、2:识别类型，3:表计类型，4:设备外观类型
     *
     * @return choose_type - 筛选类型 0:设备区域、1:设备类型、2:识别类型，3:表计类型，4:设备外观类型
     */
    public Byte getChooseType() {
        return chooseType;
    }

    /**
     * 设置筛选类型 0:设备区域、1:设备类型、2:识别类型，3:表计类型，4:设备外观类型
     *
     * @param chooseType 筛选类型 0:设备区域、1:设备类型、2:识别类型，3:表计类型，4:设备外观类型
     */
    public void setChooseType(Byte chooseType) {
        this.chooseType = chooseType;
    }
}