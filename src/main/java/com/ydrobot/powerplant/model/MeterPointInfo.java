package com.ydrobot.powerplant.model;

import javax.persistence.*;

@Table(name = "meter_point_info")
public class MeterPointInfo {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 点位id
     */
    @Column(name = "point_id")
    private Integer pointId;

    /**
     * 量程下限
     */
    @Column(name = "range_min")
    private Float rangeMin;

    /**
     * 量程上限
     */
    @Column(name = "range_max")
    private Float rangeMax;

    /**
     * 起始角度
     */
    @Column(name = "angle_min")
    private Float angleMin;

    /**
     * 截止角度
     */
    @Column(name = "angle_max")
    private Float angleMax;

    /**
     * 表计类型id
     */
    @Column(name = "meter_type_id")
    private Integer meterTypeId;

    @Column(name = "offset_value")
    private Integer offsetValue;

    /**
     * 方向 0:down 1:up
     */
    private Integer direction;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取点位id
     *
     * @return point_id - 点位id
     */
    public Integer getPointId() {
        return pointId;
    }

    /**
     * 设置点位id
     *
     * @param pointId 点位id
     */
    public void setPointId(Integer pointId) {
        this.pointId = pointId;
    }

    /**
     * 获取量程下限
     *
     * @return range_min - 量程下限
     */
    public Float getRangeMin() {
        return rangeMin;
    }

    /**
     * 设置量程下限
     *
     * @param rangeMin 量程下限
     */
    public void setRangeMin(Float rangeMin) {
        this.rangeMin = rangeMin;
    }

    /**
     * 获取量程上限
     *
     * @return range_max - 量程上限
     */
    public Float getRangeMax() {
        return rangeMax;
    }

    /**
     * 设置量程上限
     *
     * @param rangeMax 量程上限
     */
    public void setRangeMax(Float rangeMax) {
        this.rangeMax = rangeMax;
    }

    /**
     * 获取起始角度
     *
     * @return angle_min - 起始角度
     */
    public Float getAngleMin() {
        return angleMin;
    }

    /**
     * 设置起始角度
     *
     * @param angleMin 起始角度
     */
    public void setAngleMin(Float angleMin) {
        this.angleMin = angleMin;
    }

    /**
     * 获取截止角度
     *
     * @return angle_max - 截止角度
     */
    public Float getAngleMax() {
        return angleMax;
    }

    /**
     * 设置截止角度
     *
     * @param angleMax 截止角度
     */
    public void setAngleMax(Float angleMax) {
        this.angleMax = angleMax;
    }

    /**
     * 获取表计类型id
     *
     * @return meter_type_id - 表计类型id
     */
    public Integer getMeterTypeId() {
        return meterTypeId;
    }

    /**
     * 设置表计类型id
     *
     * @param meterTypeId 表计类型id
     */
    public void setMeterTypeId(Integer meterTypeId) {
        this.meterTypeId = meterTypeId;
    }

    /**
     * @return offset_value
     */
    public Integer getOffsetValue() {
        return offsetValue;
    }

    /**
     * @param offsetValue
     */
    public void setOffsetValue(Integer offsetValue) {
        this.offsetValue = offsetValue;
    }

    /**
     * 获取方向 0:down 1:up
     *
     * @return direction - 方向 0:down 1:up
     */
    public Integer getDirection() {
        return direction;
    }

    /**
     * 设置方向 0:down 1:up
     *
     * @param direction 方向 0:down 1:up
     */
    public void setDirection(Integer direction) {
        this.direction = direction;
    }
}