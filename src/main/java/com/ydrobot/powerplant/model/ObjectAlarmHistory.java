package com.ydrobot.powerplant.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "object_alarm_history")
public class ObjectAlarmHistory {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 对象id
     */
    @Column(name = "object_id")
    private Integer objectId;

    /**
     * 任务id
     */
    @Column(name = "task_id")
    private Integer taskId;

    /**
     * 任务执行记录id
     */
    @Column(name = "task_history_id")
    private Integer taskHistoryId;

    /**
     * 机器人id
     */
    @Column(name = "robot_id")
    private Integer robotId;

    /**
     * 告警类型id
     */
    @Column(name = "alarm_type_id")
    private Integer alarmTypeId;

    /**
     * 告警等级 0:正常 1:预警 2:一般告警 3:严重告警 4:危机告警
     */
    @Column(name = "alarm_level")
    private Byte alarmLevel;

    /**
     * 审核状态 0:未审核 1:已审核
     */
    @Column(name = "check_status")
    private Byte checkStatus;

    /**
     * 审核时间
     */
    @Column(name = "check_time")
    private Date checkTime;

    /**
     * 审核人id
     */
    @Column(name = "operater_id")
    private Integer operaterId;

    /**
     * 审核意见
     */
    @Column(name = "check_desc")
    private String checkDesc;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;
    
    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取对象id
     *
     * @return object_id - 对象id
     */
    public Integer getObjectId() {
        return objectId;
    }

    /**
     * 设置对象id
     *
     * @param objectId 对象id
     */
    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    /**
     * 获取任务id
     *
     * @return task_id - 任务id
     */
    public Integer getTaskId() {
        return taskId;
    }

    /**
     * 设置任务id
     *
     * @param taskId 任务id
     */
    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    /**
     * 获取任务执行记录id
     *
     * @return task_history_id - 任务执行记录id
     */
    public Integer getTaskHistoryId() {
        return taskHistoryId;
    }

    /**
     * 设置任务执行记录id
     *
     * @param taskHistoryId 任务执行记录id
     */
    public void setTaskHistoryId(Integer taskHistoryId) {
        this.taskHistoryId = taskHistoryId;
    }

    /**
     * 获取机器人id
     *
     * @return robot_id - 机器人id
     */
    public Integer getRobotId() {
        return robotId;
    }

    /**
     * 设置机器人id
     *
     * @param robotId 机器人id
     */
    public void setRobotId(Integer robotId) {
        this.robotId = robotId;
    }

    /**
     * 获取告警类型id
     *
     * @return alarm_type_id - 告警类型id
     */
    public Integer getAlarmTypeId() {
        return alarmTypeId;
    }

    /**
     * 设置告警类型id
     *
     * @param alarmTypeId 告警类型id
     */
    public void setAlarmTypeId(Integer alarmTypeId) {
        this.alarmTypeId = alarmTypeId;
    }

    /**
     * 获取告警等级 0:正常 1:预警 2:一般告警 3:严重告警 4:危机告警
     *
     * @return alarm_level - 告警等级 0:正常 1:预警 2:一般告警 3:严重告警 4:危机告警
     */
    public Byte getAlarmLevel() {
        return alarmLevel;
    }

    /**
     * 设置告警等级 0:正常 1:预警 2:一般告警 3:严重告警 4:危机告警
     *
     * @param alarmLevel 告警等级 0:正常 1:预警 2:一般告警 3:严重告警 4:危机告警
     */
    public void setAlarmLevel(Byte alarmLevel) {
        this.alarmLevel = alarmLevel;
    }

    /**
     * 获取审核状态 0:未审核 1:已审核
     *
     * @return check_status - 审核状态 0:未审核 1:已审核
     */
    public Byte getCheckStatus() {
        return checkStatus;
    }

    /**
     * 设置审核状态 0:未审核 1:已审核
     *
     * @param checkStatus 审核状态 0:未审核 1:已审核
     */
    public void setCheckStatus(Byte checkStatus) {
        this.checkStatus = checkStatus;
    }

    /**
     * 获取审核时间
     *
     * @return check_time - 审核时间
     */
    public Date getCheckTime() {
        return checkTime;
    }

    /**
     * 设置审核时间
     *
     * @param checkTime 审核时间
     */
    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    /**
     * 获取审核人id
     *
     * @return operater_id - 审核人id
     */
    public Integer getOperaterId() {
        return operaterId;
    }

    /**
     * 设置审核人id
     *
     * @param operaterId 审核人id
     */
    public void setOperaterId(Integer operaterId) {
        this.operaterId = operaterId;
    }

    /**
     * 获取审核意见
     *
     * @return check_desc - 审核意见
     */
    public String getCheckDesc() {
        return checkDesc;
    }

    /**
     * 设置审核意见
     *
     * @param checkDesc 审核意见
     */
    public void setCheckDesc(String checkDesc) {
        this.checkDesc = checkDesc;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}