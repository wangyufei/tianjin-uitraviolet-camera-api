package com.ydrobot.powerplant.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "operation_log")
public class OperationLog {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 日志类型 0:系统 1:机器人 2:客户端
     */
    private Byte type;

    /**
     * 日志内容
     */
    private String content;

    /**
     * 操作人id
     */
    @Column(name = "operater_id")
    private Integer operaterId;

    /**
     * 设备id
     */
    @Column(name = "device_id")
    private Integer deviceId;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取日志类型 0:系统 1:机器人 2:客户端
     *
     * @return type - 日志类型 0:系统 1:机器人 2:客户端
     */
    public Byte getType() {
        return type;
    }

    /**
     * 设置日志类型 0:系统 1:机器人 2:客户端
     *
     * @param type 日志类型 0:系统 1:机器人 2:客户端
     */
    public void setType(Byte type) {
        this.type = type;
    }

    /**
     * 获取日志内容
     *
     * @return content - 日志内容
     */
    public String getContent() {
        return content;
    }

    /**
     * 设置日志内容
     *
     * @param content 日志内容
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 获取操作人id
     *
     * @return operater_id - 操作人id
     */
    public Integer getOperaterId() {
        return operaterId;
    }

    /**
     * 设置操作人id
     *
     * @param operaterId 操作人id
     */
    public void setOperaterId(Integer operaterId) {
        this.operaterId = operaterId;
    }

    /**
     * 获取设备id
     *
     * @return device_id - 设备id
     */
    public Integer getDeviceId() {
        return deviceId;
    }

    /**
     * 设置设备id
     *
     * @param deviceId 设备id
     */
    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}