package com.ydrobot.powerplant.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "path_planning")
public class PathPlanning {
    /**
     * 自增id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 任务id
     */
    @Column(name = "task_id")
    private Integer taskId;

    /**
     * 任务执行记录id
     */
    @Column(name = "task_history_id")
    private Integer taskHistoryId;

    /**
     * 标志位 0:原点出发 1:非原点出发 2:一键返航
     */
    private Byte flag;

    /**
     * 机器人id
     */
    @Column(name = "robot_id")
    private Integer robotId;

    /**
     * 路径
     */
    private String path;
    
    /**
     * 执行记录
     */
    private String record;

    /**
     * 获取自增id
     *
     * @return id - 自增id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置自增id
     *
     * @param id 自增id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取任务id
     *
     * @return task_id - 任务id
     */
    public Integer getTaskId() {
        return taskId;
    }

    /**
     * 设置任务id
     *
     * @param taskId 任务id
     */
    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public Integer getTaskHistoryId() {
        return taskHistoryId;
    }

    public void setTaskHistoryId(Integer taskHistoryId) {
        this.taskHistoryId = taskHistoryId;
    }

    /**
     * 获取标志位 0:原点出发 1:非原点出发 2:一键返航
     *
     * @return flag - 标志位 0:原点出发 1:非原点出发 2:一键返航
     */
    public Byte getFlag() {
        return flag;
    }

    /**
     * 设置标志位 0:原点出发 1:非原点出发 2:一键返航
     *
     * @param flag 标志位 0:原点出发 1:非原点出发 2:一键返航
     */
    public void setFlag(Byte flag) {
        this.flag = flag;
    }

    /**
     * 获取机器人id
     *
     * @return robot_id - 机器人id
     */
    public Integer getRobotId() {
        return robotId;
    }

    /**
     * 设置机器人id
     *
     * @param robotId 机器人id
     */
    public void setRobotId(Integer robotId) {
        this.robotId = robotId;
    }

    /**
     * 获取路径
     *
     * @return path - 路径
     */
    public String getPath() {
        return path;
    }

    /**
     * 设置路径
     *
     * @param path 路径
     */
    public void setPath(String path) {
        this.path = path;
    }

	public String getRecord() {
		return record;
	}

	public void setRecord(String record) {
		this.record = record;
	}
}