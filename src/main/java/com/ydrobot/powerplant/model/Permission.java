package com.ydrobot.powerplant.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

public class Permission {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 类型 0:目录 1:子集
     */
    private Byte type;

    /**
     * 显示名称
     */
    @Column(name = "display_name")
    private String displayName;

    /**
     * 路由地址
     */
    @Column(name = "route_address")
    private String routeAddress;

    /**
     * 父级id
     */
    @Column(name = "parent_id")
    private Integer parentId;

    @Column(name = "update_time")
    private Date updateTime;

    @Transient
    private List<Permission> children;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取名称
     *
     * @return name - 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置名称
     *
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取类型 0:目录 1:子集
     *
     * @return type - 类型 0:目录 1:子集
     */
    public Byte getType() {
        return type;
    }

    /**
     * 设置类型 0:目录 1:子集
     *
     * @param type 类型 0:目录 1:子集
     */
    public void setType(Byte type) {
        this.type = type;
    }

    /**
     * 获取显示名称
     *
     * @return display_name - 显示名称
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * 设置显示名称
     *
     * @param displayName 显示名称
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * 获取路由地址
     *
     * @return route_address - 路由地址
     */
    public String getRouteAddress() {
        return routeAddress;
    }

    /**
     * 设置路由地址
     *
     * @param routeAddress 路由地址
     */
    public void setRouteAddress(String routeAddress) {
        this.routeAddress = routeAddress;
    }

    /**
     * 获取父级id
     *
     * @return parent_id - 父级id
     */
    public Integer getParentId() {
        return parentId;
    }

    /**
     * 设置父级id
     *
     * @param parentId 父级id
     */
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public List<Permission> getChildren() {
        return children;
    }

    public void setChildren(List<Permission> children) {
        this.children = children;
    }

}