package com.ydrobot.powerplant.model;

import javax.persistence.*;

@Table(name = "pms_device_scada")
public class PmsDeviceScada {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 对象id
     */
    @Column(name = "object_id")
    private String objectId;

    /**
     * 设备编码
     */
    @Column(name = "device_sn")
    private String deviceSn;

    /**
     * 设备名称
     */
    @Column(name = "device_name")
    private String deviceName;

    /**
     * 设备类型
     */
    @Column(name = "device_type")
    private String deviceType;

    /**
     * 电压等级
     */
    @Column(name = "voltage_grade")
    private String voltageGrade;

    /**
     * 电流
     */
    private String electricity;

    /**
     * 电压
     */
    private String voltage;

    /**
     * 有功
     */
    private String active;

    /**
     * 无功
     */
    private String reactive;

    /**
     * 温度
     */
    private String temp;

    /**
     * 湿度
     */
    private String humidity;

    /**
     * 风速
     */
    @Column(name = "wind_speed")
    private String windSpeed;

    /**
     * 时间
     */
    @Column(name = "create_time")
    private String createTime;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取对象id
     *
     * @return object_id - 对象id
     */
    public String getObjectId() {
        return objectId;
    }

    /**
     * 设置对象id
     *
     * @param objectId 对象id
     */
    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    /**
     * 获取设备编码
     *
     * @return device_sn - 设备编码
     */
    public String getDeviceSn() {
        return deviceSn;
    }

    /**
     * 设置设备编码
     *
     * @param deviceSn 设备编码
     */
    public void setDeviceSn(String deviceSn) {
        this.deviceSn = deviceSn;
    }

    /**
     * 获取设备名称
     *
     * @return device_name - 设备名称
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     * 设置设备名称
     *
     * @param deviceName 设备名称
     */
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    /**
     * 获取设备类型
     *
     * @return device_type - 设备类型
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * 设置设备类型
     *
     * @param deviceType 设备类型
     */
    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    /**
     * 获取电压等级
     *
     * @return voltage_grade - 电压等级
     */
    public String getVoltageGrade() {
        return voltageGrade;
    }

    /**
     * 设置电压等级
     *
     * @param voltageGrade 电压等级
     */
    public void setVoltageGrade(String voltageGrade) {
        this.voltageGrade = voltageGrade;
    }

    /**
     * 获取电流
     *
     * @return electricity - 电流
     */
    public String getElectricity() {
        return electricity;
    }

    /**
     * 设置电流
     *
     * @param electricity 电流
     */
    public void setElectricity(String electricity) {
        this.electricity = electricity;
    }

    /**
     * 获取电压
     *
     * @return voltage - 电压
     */
    public String getVoltage() {
        return voltage;
    }

    /**
     * 设置电压
     *
     * @param voltage 电压
     */
    public void setVoltage(String voltage) {
        this.voltage = voltage;
    }

    /**
     * 获取有功
     *
     * @return active - 有功
     */
    public String getActive() {
        return active;
    }

    /**
     * 设置有功
     *
     * @param active 有功
     */
    public void setActive(String active) {
        this.active = active;
    }

    /**
     * 获取无功
     *
     * @return reactive - 无功
     */
    public String getReactive() {
        return reactive;
    }

    /**
     * 设置无功
     *
     * @param reactive 无功
     */
    public void setReactive(String reactive) {
        this.reactive = reactive;
    }

    /**
     * 获取温度
     *
     * @return temp - 温度
     */
    public String getTemp() {
        return temp;
    }

    /**
     * 设置温度
     *
     * @param temp 温度
     */
    public void setTemp(String temp) {
        this.temp = temp;
    }

    /**
     * 获取湿度
     *
     * @return humidity - 湿度
     */
    public String getHumidity() {
        return humidity;
    }

    /**
     * 设置湿度
     *
     * @param humidity 湿度
     */
    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    /**
     * 获取风速
     *
     * @return wind_speed - 风速
     */
    public String getWindSpeed() {
        return windSpeed;
    }

    /**
     * 设置风速
     *
     * @param windSpeed 风速
     */
    public void setWindSpeed(String windSpeed) {
        this.windSpeed = windSpeed;
    }

    /**
     * 获取时间
     *
     * @return create_time - 时间
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * 设置时间
     *
     * @param createTime 时间
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}