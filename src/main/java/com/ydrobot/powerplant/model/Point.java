package com.ydrobot.powerplant.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

public class Point {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 点位名称
     */
    private String name;

    /**
     * 显示名称
     */
    @Column(name = "display_name")
    private String displayName;

    /**
     * 对象id
     */
    @Column(name = "object_id")
    private Integer objectId;

    /**
     * 识别类型id
     */
    @Column(name = "recon_type_id")
    private Integer reconTypeId;

    /**
     * 表计类型id
     */
    @Column(name = "meter_type_id")
    private Integer meterTypeId;

    /**
     * 外观类型id
     */
    @Column(name = "face_type_id")
    private Integer faceTypeId;

    /**
     * 发热类型id
     */
    @Column(name = "hot_type_id")
    private Integer hotTypeId;

    /**
     * 保存类型id  1:红外  2:可见光 3:音频
     */
    @Column(name = "save_type_id")
    private Integer saveTypeId;

    /**
     * 0:采集 1:计算
     */
    private Byte nis;

    /**
     * 0:不报警 1:报警
     */
    @Column(name = "is_alarm")
    private Byte isAlarm;

    /**
     * 单位
     */
    private String unit;

    /**
     * 系数
     */
    private Integer coef;

    /**
     * 是否启用  0:不启用 1:启用
     */
    @Column(name = "is_use")
    private Byte isUse;

    /**
     * 0:前端不报警显示 1:前端报警显示
     */
    @Column(name = "is_show")
    private Byte isShow;

    /**
     * 0:不保存 1:保存
     */
    @Column(name = "is_save")
    private Byte isSave;

    /**
     * 默认值
     */
    @Column(name = "default_value")
    private Float defaultValue;

    /**
     * x坐标
     */
    private Float x;

    /**
     * y坐标
     */
    private Float y;

    /**
     * z坐标
     */
    private Float z;

    /**
     * 是否删除 0:正常 1:已删除
     */
    @Column(name = "is_delete")
    private Byte isDelete;

    /**
     * 最大报警等级
     */
    @Column(name = "max_alarm_level")
    private Byte maxAlarmLevel;

    /**
     * 识别模型子类id
     */
    @Column(name = "recon_model_sub_id")
    private Integer reconModelSubId;

    /**
     * 点位是否异常 0:正常 1:异常
     */
    @Column(name = "is_abnormal")
    private Byte isAbnormal;

    @Transient
    private Byte alarmLevel;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Integer getObjectId() {
		return objectId;
	}

	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}

	public Integer getReconTypeId() {
		return reconTypeId;
	}

	public void setReconTypeId(Integer reconTypeId) {
		this.reconTypeId = reconTypeId;
	}

	public Integer getMeterTypeId() {
		return meterTypeId;
	}

	public void setMeterTypeId(Integer meterTypeId) {
		this.meterTypeId = meterTypeId;
	}

	public Integer getFaceTypeId() {
		return faceTypeId;
	}

	public void setFaceTypeId(Integer faceTypeId) {
		this.faceTypeId = faceTypeId;
	}

	public Integer getHotTypeId() {
		return hotTypeId;
	}

	public void setHotTypeId(Integer hotTypeId) {
		this.hotTypeId = hotTypeId;
	}

	public Integer getSaveTypeId() {
		return saveTypeId;
	}

	public void setSaveTypeId(Integer saveTypeId) {
		this.saveTypeId = saveTypeId;
	}

	public Byte getNis() {
		return nis;
	}

	public void setNis(Byte nis) {
		this.nis = nis;
	}

	public Byte getIsAlarm() {
		return isAlarm;
	}

	public void setIsAlarm(Byte isAlarm) {
		this.isAlarm = isAlarm;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Integer getCoef() {
		return coef;
	}

	public void setCoef(Integer coef) {
		this.coef = coef;
	}

	public Byte getIsUse() {
		return isUse;
	}

	public void setIsUse(Byte isUse) {
		this.isUse = isUse;
	}

	public Byte getIsShow() {
		return isShow;
	}

	public void setIsShow(Byte isShow) {
		this.isShow = isShow;
	}

	public Byte getIsSave() {
		return isSave;
	}

	public void setIsSave(Byte isSave) {
		this.isSave = isSave;
	}

	public Float getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(Float defaultValue) {
		this.defaultValue = defaultValue;
	}

	public Float getX() {
		return x;
	}

	public void setX(Float x) {
		this.x = x;
	}

	public Float getY() {
		return y;
	}

	public void setY(Float y) {
		this.y = y;
	}

	public Float getZ() {
		return z;
	}

	public void setZ(Float z) {
		this.z = z;
	}

	public Byte getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Byte isDelete) {
		this.isDelete = isDelete;
	}

	public Byte getMaxAlarmLevel() {
		return maxAlarmLevel;
	}

	public void setMaxAlarmLevel(Byte maxAlarmLevel) {
		this.maxAlarmLevel = maxAlarmLevel;
	}

	public Integer getReconModelSubId() {
		return reconModelSubId;
	}

	public void setReconModelSubId(Integer reconModelSubId) {
		this.reconModelSubId = reconModelSubId;
	}

	public Byte getIsAbnormal() {
		return isAbnormal;
	}

	public void setIsAbnormal(Byte isAbnormal) {
		this.isAbnormal = isAbnormal;
	}

	public Byte getAlarmLevel() {
		return alarmLevel;
	}

	public void setAlarmLevel(Byte alarmLevel) {
		this.alarmLevel = alarmLevel;
	}

}