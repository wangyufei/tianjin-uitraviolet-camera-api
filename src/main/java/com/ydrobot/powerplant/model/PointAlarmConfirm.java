package com.ydrobot.powerplant.model;

import javax.persistence.*;

@Table(name = "point_alarm_confirm")
public class PointAlarmConfirm {
    /**
     * 自增id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 部件告警记录id
     */
    @Column(name = "point_alarm_history_id")
    private Integer pointAlarmHistoryId;

    /**
     * 任务id
     */
    @Column(name = "task_id")
    private Integer taskId;
    
    /**
     * 报警类型id
     */
    @Column(name = "alarm_type_id")
    private Integer alarmTypeId;

    /**
     * 获取自增id
     *
     * @return id - 自增id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置自增id
     *
     * @param id 自增id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取部件告警记录id
     *
     * @return point_alarm_history_id - 部件告警记录id
     */
    public Integer getPointAlarmHistoryId() {
        return pointAlarmHistoryId;
    }

    /**
     * 设置部件告警记录id
     *
     * @param pointAlarmHistoryId 部件告警记录id
     */
    public void setPointAlarmHistoryId(Integer pointAlarmHistoryId) {
        this.pointAlarmHistoryId = pointAlarmHistoryId;
    }

    /**
     * 获取任务id
     *
     * @return task_id - 任务id
     */
    public Integer getTaskId() {
        return taskId;
    }

    /**
     * 设置任务id
     *
     * @param taskId 任务id
     */
    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

	public Integer getAlarmTypeId() {
		return alarmTypeId;
	}

	public void setAlarmTypeId(Integer alarmTypeId) {
		this.alarmTypeId = alarmTypeId;
	}
}