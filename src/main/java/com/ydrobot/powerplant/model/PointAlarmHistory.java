package com.ydrobot.powerplant.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "point_alarm_history")
public class PointAlarmHistory {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 巡检点位历史记录id
     */
    @Column(name = "point_history_id")
    private Integer pointHistoryId;

    /**
     * 任务执行记录id
     */
    @Column(name = "task_history_id")
    private Integer taskHistoryId;

    /**
     * 机器人id
     */
    @Column(name = "robot_id")
    private Integer robotId;

    /**
     * 巡检点id
     */
    @Column(name = "point_id")
    private Integer pointId;

    /**
     * 告警类型id
     */
    @Column(name = "alarm_type_id")
    private Integer alarmTypeId;

    /**
     * 识别时间
     */
    @Column(name = "recon_time")
    private Date reconTime;

    /**
     * 告警等级 0:正常 1:预警 2:一般告警 3:严重告警 4:危机告警
     */
    @Column(name = "alarm_level")
    private Byte alarmLevel;

    /**
     * 结果状态 0:正确 1:错误
     */
    @Column(name = "result_status")
    private Byte resultStatus;

    /**
     * 异常标识 0:正常 1:异常
     */
    @Column(name = "error_flag")
    private Byte errorFlag;

    /**
     * 审核状态 0:未审核 1:已审核
     */
    @Column(name = "check_status")
    private Byte checkStatus;

    /**
     * 审核时间
     */
    @Column(name = "check_time")
    private Date checkTime;

    /**
     * 审核人id
     */
    @Column(name = "operater_id")
    private Integer operaterId;

    /**
     * 审核意见
     */
    @Column(name = "check_desc")
    private String checkDesc;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取巡检点位历史记录id
     *
     * @return point_history_id - 巡检点位历史记录id
     */
    public Integer getPointHistoryId() {
        return pointHistoryId;
    }

    /**
     * 设置巡检点位历史记录id
     *
     * @param pointHistoryId 巡检点位历史记录id
     */
    public void setPointHistoryId(Integer pointHistoryId) {
        this.pointHistoryId = pointHistoryId;
    }

    /**
     * 获取任务执行记录id
     *
     * @return task_history_id - 任务执行记录id
     */
    public Integer getTaskHistoryId() {
        return taskHistoryId;
    }

    /**
     * 设置任务执行记录id
     *
     * @param taskHistoryId 任务执行记录id
     */
    public void setTaskHistoryId(Integer taskHistoryId) {
        this.taskHistoryId = taskHistoryId;
    }

    /**
     * 获取机器人id
     *
     * @return robot_id - 机器人id
     */
    public Integer getRobotId() {
        return robotId;
    }

    /**
     * 设置机器人id
     *
     * @param robotId 机器人id
     */
    public void setRobotId(Integer robotId) {
        this.robotId = robotId;
    }

    /**
     * 获取巡检点id
     *
     * @return point_id - 巡检点id
     */
    public Integer getPointId() {
        return pointId;
    }

    /**
     * 设置巡检点id
     *
     * @param pointId 巡检点id
     */
    public void setPointId(Integer pointId) {
        this.pointId = pointId;
    }

    /**
     * 获取告警类型id
     *
     * @return alarm_type_id - 告警类型id
     */
    public Integer getAlarmTypeId() {
        return alarmTypeId;
    }

    /**
     * 设置告警类型id
     *
     * @param alarmTypeId 告警类型id
     */
    public void setAlarmTypeId(Integer alarmTypeId) {
        this.alarmTypeId = alarmTypeId;
    }

    /**
     * 获取识别时间
     *
     * @return recon_time - 识别时间
     */
    public Date getReconTime() {
        return reconTime;
    }

    /**
     * 设置识别时间
     *
     * @param reconTime 识别时间
     */
    public void setReconTime(Date reconTime) {
        this.reconTime = reconTime;
    }

    /**
     * 获取告警等级 0:正常 1:预警 2:一般告警 3:严重告警 4:危机告警
     *
     * @return alarm_level - 告警等级 0:正常 1:预警 2:一般告警 3:严重告警 4:危机告警
     */
    public Byte getAlarmLevel() {
        return alarmLevel;
    }

    /**
     * 设置告警等级 0:正常 1:预警 2:一般告警 3:严重告警 4:危机告警
     *
     * @param alarmLevel 告警等级 0:正常 1:预警 2:一般告警 3:严重告警 4:危机告警
     */
    public void setAlarmLevel(Byte alarmLevel) {
        this.alarmLevel = alarmLevel;
    }

    /**
     * 获取结果状态 0:正确 1:错误
     *
     * @return result_status - 结果状态 0:正确 1:错误
     */
    public Byte getResultStatus() {
        return resultStatus;
    }

    /**
     * 设置结果状态 0:正确 1:错误
     *
     * @param resultStatus 结果状态 0:正确 1:错误
     */
    public void setResultStatus(Byte resultStatus) {
        this.resultStatus = resultStatus;
    }

    /**
     * 获取异常标识 0:正常 1:异常
     *
     * @return error_flag - 异常标识 0:正常 1:异常
     */
    public Byte getErrorFlag() {
        return errorFlag;
    }

    /**
     * 设置异常标识 0:正常 1:异常
     *
     * @param errorFlag 异常标识 0:正常 1:异常
     */
    public void setErrorFlag(Byte errorFlag) {
        this.errorFlag = errorFlag;
    }

    /**
     * 获取审核状态 0:未审核 1:已审核
     *
     * @return check_status - 审核状态 0:未审核 1:已审核
     */
    public Byte getCheckStatus() {
        return checkStatus;
    }

    /**
     * 设置审核状态 0:未审核 1:已审核
     *
     * @param checkStatus 审核状态 0:未审核 1:已审核
     */
    public void setCheckStatus(Byte checkStatus) {
        this.checkStatus = checkStatus;
    }

    /**
     * 获取审核时间
     *
     * @return check_time - 审核时间
     */
    public Date getCheckTime() {
        return checkTime;
    }

    /**
     * 设置审核时间
     *
     * @param checkTime 审核时间
     */
    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    /**
     * 获取审核人id
     *
     * @return operater_id - 审核人id
     */
    public Integer getOperaterId() {
        return operaterId;
    }

    /**
     * 设置审核人id
     *
     * @param operaterId 审核人id
     */
    public void setOperaterId(Integer operaterId) {
        this.operaterId = operaterId;
    }

    /**
     * 获取审核意见
     *
     * @return check_desc - 审核意见
     */
    public String getCheckDesc() {
        return checkDesc;
    }

    /**
     * 设置审核意见
     *
     * @param checkDesc 审核意见
     */
    public void setCheckDesc(String checkDesc) {
        this.checkDesc = checkDesc;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    
}