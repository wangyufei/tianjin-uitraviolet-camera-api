package com.ydrobot.powerplant.model;

import javax.persistence.*;

@Table(name = "point_alarm_setting")
public class PointAlarmSetting {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 点位id
     */
    @Column(name = "point_id")
    private Integer pointId;

	/**
	 * 告警类型id 与alarm_type表id相关连
	 * 
	 * 1：超温报警 2：升温报警 3：三相对比报警 4：三相温差报警 5：越值报警（关心上下限）
	 * 
	 * 
	 */
	@Column(name = "alarm_type_id")
	private Integer alarmTypeId;

    /**
     * 告警名称
     */
    @Column(name = "alarm_name")
    private String alarmName;

    /**
     * 告警等级： 0正常，1预告，2一般告警，3严重告警，4危急告警
     */
    @Column(name = "alarm_level")
    private Byte alarmLevel;

    /**
     * 限值
     */
    @Column(name = "limit_value")
    private Float limitValue;

    /**
     * 值类型  1:数值  2:百分比
     */
    @Column(name = "value_type")
    private Byte valueType;

    /**
     * 0:下限 1:上限
     */
    @Column(name = "up_or_down")
    private Byte upOrDown;

    /**
     * 是否有效 0:无效 1:有效
     */
    @Column(name = "is_effective")
    private Byte isEffective;

    /**
     * 是否启用环境温度 0:不启用 1:启用
     */
    @Column(name = "is_open_environment_temp")
    private Byte isOpenEnvironmentTemp;

    /**
     * 参考温度
     */
    @Column(name = "reference_temp")
    private Float referenceTemp;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取点位id
     *
     * @return point_id - 点位id
     */
    public Integer getPointId() {
        return pointId;
    }

    /**
     * 设置点位id
     *
     * @param pointId 点位id
     */
    public void setPointId(Integer pointId) {
        this.pointId = pointId;
    }

    /**
     * 获取告警类型id  与alarm_type表id相关连

1：超温报警
2：升温报警
3：三相对比报警
4：三相温差报警
5：越值报警（关心上下限）


     *
     * @return alarm_type_id - 告警类型id  与alarm_type表id相关连

1：超温报警
2：升温报警
3：三相对比报警
4：三相温差报警
5：越值报警（关心上下限）


     */
    public Integer getAlarmTypeId() {
        return alarmTypeId;
    }

    /**
     * 设置告警类型id  与alarm_type表id相关连

1：超温报警
2：升温报警
3：三相对比报警
4：三相温差报警
5：越值报警（关心上下限）


     *
     * @param alarmTypeId 告警类型id  与alarm_type表id相关连

1：超温报警
2：升温报警
3：三相对比报警
4：三相温差报警
5：越值报警（关心上下限）


     */
    public void setAlarmTypeId(Integer alarmTypeId) {
        this.alarmTypeId = alarmTypeId;
    }

    /**
     * 获取告警名称
     *
     * @return alarm_name - 告警名称
     */
    public String getAlarmName() {
        return alarmName;
    }

    /**
     * 设置告警名称
     *
     * @param alarmName 告警名称
     */
    public void setAlarmName(String alarmName) {
        this.alarmName = alarmName;
    }

    /**
     * 获取告警等级： 0正常，1预告，2一般告警，3严重告警，4危急告警
     *
     * @return alarm_level - 告警等级： 0正常，1预告，2一般告警，3严重告警，4危急告警
     */
    public Byte getAlarmLevel() {
        return alarmLevel;
    }

    /**
     * 设置告警等级： 0正常，1预告，2一般告警，3严重告警，4危急告警
     *
     * @param alarmLevel 告警等级： 0正常，1预告，2一般告警，3严重告警，4危急告警
     */
    public void setAlarmLevel(Byte alarmLevel) {
        this.alarmLevel = alarmLevel;
    }

    /**
     * 获取限值
     *
     * @return limit_value - 限值
     */
    public Float getLimitValue() {
        return limitValue;
    }

    /**
     * 设置限值
     *
     * @param limitValue 限值
     */
    public void setLimitValue(Float limitValue) {
        this.limitValue = limitValue;
    }

    /**
     * 获取值类型  1:数值  2:百分比
     *
     * @return value_type - 值类型  1:数值  2:百分比
     */
    public Byte getValueType() {
        return valueType;
    }

    /**
     * 设置值类型  1:数值  2:百分比
     *
     * @param valueType 值类型  1:数值  2:百分比
     */
    public void setValueType(Byte valueType) {
        this.valueType = valueType;
    }

    /**
     * 获取0:下限 1:上限
     *
     * @return up_or_down - 0:下限 1:上限
     */
    public Byte getUpOrDown() {
        return upOrDown;
    }

    /**
     * 设置0:下限 1:上限
     *
     * @param upOrDown 0:下限 1:上限
     */
    public void setUpOrDown(Byte upOrDown) {
        this.upOrDown = upOrDown;
    }

    /**
     * 获取是否有效 0:无效 1:有效
     *
     * @return is_effective - 是否有效 0:无效 1:有效
     */
    public Byte getIsEffective() {
        return isEffective;
    }

    /**
     * 设置是否有效 0:无效 1:有效
     *
     * @param isEffective 是否有效 0:无效 1:有效
     */
    public void setIsEffective(Byte isEffective) {
        this.isEffective = isEffective;
    }

    /**
     * 获取是否启用环境温度 0:不启用 1:启用
     *
     * @return is_open_environment_temp - 是否启用环境温度 0:不启用 1:启用
     */
    public Byte getIsOpenEnvironmentTemp() {
        return isOpenEnvironmentTemp;
    }

    /**
     * 设置是否启用环境温度 0:不启用 1:启用
     *
     * @param isOpenEnvironmentTemp 是否启用环境温度 0:不启用 1:启用
     */
    public void setIsOpenEnvironmentTemp(Byte isOpenEnvironmentTemp) {
        this.isOpenEnvironmentTemp = isOpenEnvironmentTemp;
    }

    /**
     * 获取参考温度
     *
     * @return reference_temp - 参考温度
     */
    public Float getReferenceTemp() {
        return referenceTemp;
    }

    /**
     * 设置参考温度
     *
     * @param referenceTemp 参考温度
     */
    public void setReferenceTemp(Float referenceTemp) {
        this.referenceTemp = referenceTemp;
    }
}