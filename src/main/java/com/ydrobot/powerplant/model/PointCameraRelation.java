package com.ydrobot.powerplant.model;

import javax.persistence.*;

@Table(name = "point_camera_relation")
public class PointCameraRelation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 部件id
     */
    @Column(name = "point_id")
    private Integer pointId;

    /**
     * 预置位id
     */
    @Column(name = "preset_id")
    private Integer presetId;

    /**
     * 机器人id
     */
    @Column(name = "robot_id")
    private Integer robotId;

    /**
     * 倍数
     */
    private Integer multiple;

    /**
     * 变焦
     */
    private Integer zoom;

    /**
     * 名称
     */
    private String name;
    
    /**
     * 紫外变倍
     */
    private Integer ultravioletMultiple;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取部件id
     *
     * @return point_id - 部件id
     */
    public Integer getPointId() {
        return pointId;
    }

    /**
     * 设置部件id
     *
     * @param pointId 部件id
     */
    public void setPointId(Integer pointId) {
        this.pointId = pointId;
    }

    /**
     * 获取预置位id
     *
     * @return preset_id - 预置位id
     */
    public Integer getPresetId() {
        return presetId;
    }

    /**
     * 设置预置位id
     *
     * @param presetId 预置位id
     */
    public void setPresetId(Integer presetId) {
        this.presetId = presetId;
    }

    /**
     * 获取机器人id
     *
     * @return robot_id - 机器人id
     */
    public Integer getRobotId() {
        return robotId;
    }

    /**
     * 设置机器人id
     *
     * @param robotId 机器人id
     */
    public void setRobotId(Integer robotId) {
        this.robotId = robotId;
    }

    /**
     * 获取倍数
     *
     * @return multiple - 倍数
     */
    public Integer getMultiple() {
        return multiple;
    }

    /**
     * 设置倍数
     *
     * @param multiple 倍数
     */
    public void setMultiple(Integer multiple) {
        this.multiple = multiple;
    }

    /**
     * 获取变焦
     *
     * @return zoom - 变焦
     */
    public Integer getZoom() {
        return zoom;
    }

    /**
     * 设置变焦
     *
     * @param zoom 变焦
     */
    public void setZoom(Integer zoom) {
        this.zoom = zoom;
    }

    /**
     * 获取名称
     *
     * @return name - 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置名称
     *
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name;
    }

	public Integer getUltravioletMultiple() {
		return ultravioletMultiple;
	}

	public void setUltravioletMultiple(Integer ultravioletMultiple) {
		this.ultravioletMultiple = ultravioletMultiple;
	}
    
    
}