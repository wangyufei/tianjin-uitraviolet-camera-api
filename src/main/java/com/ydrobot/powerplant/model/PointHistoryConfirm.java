package com.ydrobot.powerplant.model;

import javax.persistence.*;

@Table(name = "point_history_confirm")
public class PointHistoryConfirm {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 点位记录id
     */
    @Column(name = "point_history_id")
    private Integer pointHistoryId;

    /**
     * 任务id
     */
    @Column(name = "task_id")
    private Integer taskId;

    /**
     * 值
     */
    private Float value;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取点位记录id
     *
     * @return point_history_id - 点位记录id
     */
    public Integer getPointHistoryId() {
        return pointHistoryId;
    }

    /**
     * 设置点位记录id
     *
     * @param pointHistoryId 点位记录id
     */
    public void setPointHistoryId(Integer pointHistoryId) {
        this.pointHistoryId = pointHistoryId;
    }

    /**
     * 获取任务id
     *
     * @return task_id - 任务id
     */
    public Integer getTaskId() {
        return taskId;
    }

    /**
     * 设置任务id
     *
     * @param taskId 任务id
     */
    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    /**
     * 获取值
     *
     * @return value - 值
     */
    public Float getValue() {
        return value;
    }

    /**
     * 设置值
     *
     * @param value 值
     */
    public void setValue(Float value) {
        this.value = value;
    }
}