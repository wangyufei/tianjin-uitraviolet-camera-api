package com.ydrobot.powerplant.model;

import javax.persistence.*;

@Table(name = "point_relation_setting")
public class PointRelationSetting {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 点位id
     */
    @Column(name = "point_id")
    private Integer pointId;

    /**
     * 三相温差关联点
     */
    @Column(name = "reference_point_id")
    private Integer referencePointId;

    /**
     * 类型 0:三相温差 1:三相对比
     */
    private Byte type;

    /**
     * 依赖点
     */
    @Column(name = "depend_point")
    private String dependPoint;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取点位id
     *
     * @return point_id - 点位id
     */
    public Integer getPointId() {
        return pointId;
    }

    /**
     * 设置点位id
     *
     * @param pointId 点位id
     */
    public void setPointId(Integer pointId) {
        this.pointId = pointId;
    }

    /**
     * 获取三相温差关联点
     *
     * @return reference_point_id - 三相温差关联点
     */
    public Integer getReferencePointId() {
        return referencePointId;
    }

    /**
     * 设置三相温差关联点
     *
     * @param referencePointId 三相温差关联点
     */
    public void setReferencePointId(Integer referencePointId) {
        this.referencePointId = referencePointId;
    }

    /**
     * 获取类型 0:三相温差 1:三相对比
     *
     * @return type - 类型 0:三相温差 1:三相对比
     */
    public Byte getType() {
        return type;
    }

    /**
     * 设置类型 0:三相温差 1:三相对比
     *
     * @param type 类型 0:三相温差 1:三相对比
     */
    public void setType(Byte type) {
        this.type = type;
    }

    /**
     * 获取依赖点
     *
     * @return depend_point - 依赖点
     */
    public String getDependPoint() {
        return dependPoint;
    }

    /**
     * 设置依赖点
     *
     * @param dependPoint 依赖点
     */
    public void setDependPoint(String dependPoint) {
        this.dependPoint = dependPoint;
    }
}