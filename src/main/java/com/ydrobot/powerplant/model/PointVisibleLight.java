package com.ydrobot.powerplant.model;

import javax.persistence.*;

@Table(name = "point_visible_light")
public class PointVisibleLight {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 点位id
     */
    @Column(name = "point_id")
    private Integer pointId;

    /**
     * 可见光目标大小的高度
     */
    private Float height;

    /**
     * 可见光目标大小的宽度
     */
    private Float width;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取点位id
     *
     * @return point_id - 点位id
     */
    public Integer getPointId() {
        return pointId;
    }

    /**
     * 设置点位id
     *
     * @param pointId 点位id
     */
    public void setPointId(Integer pointId) {
        this.pointId = pointId;
    }

    /**
     * 获取可见光目标大小的高度
     *
     * @return height - 可见光目标大小的高度
     */
    public Float getHeight() {
        return height;
    }

    /**
     * 设置可见光目标大小的高度
     *
     * @param height 可见光目标大小的高度
     */
    public void setHeight(Float height) {
        this.height = height;
    }

    /**
     * 获取可见光目标大小的宽度
     *
     * @return width - 可见光目标大小的宽度
     */
    public Float getWidth() {
        return width;
    }

    /**
     * 设置可见光目标大小的宽度
     *
     * @param width 可见光目标大小的宽度
     */
    public void setWidth(Float width) {
        this.width = width;
    }
}