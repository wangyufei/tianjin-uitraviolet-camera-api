package com.ydrobot.powerplant.model;

import javax.persistence.*;

@Table(name = "point_watch_point")
public class PointWatchPoint {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 组件id
     */
    @Column(name = "point_id")
    private Integer pointId;

    /**
     * 观察点id
     */
    @Column(name = "watch_point_id")
    private Integer watchPointId;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取组件id
     *
     * @return point_id - 组件id
     */
    public Integer getPointId() {
        return pointId;
    }

    /**
     * 设置组件id
     *
     * @param pointId 组件id
     */
    public void setPointId(Integer pointId) {
        this.pointId = pointId;
    }

    /**
     * 获取观察点id
     *
     * @return watch_point_id - 观察点id
     */
    public Integer getWatchPointId() {
        return watchPointId;
    }

    /**
     * 设置观察点id
     *
     * @param watchPointId 观察点id
     */
    public void setWatchPointId(Integer watchPointId) {
        this.watchPointId = watchPointId;
    }
}