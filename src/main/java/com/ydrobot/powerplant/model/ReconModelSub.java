package com.ydrobot.powerplant.model;

import javax.persistence.*;

@Table(name = "recon_model_sub")
public class ReconModelSub {
    /**
     * id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 识别模型id
     */
    @Column(name = "recon_model_id")
    private Integer reconModelId;

    /**
     * 名称KEY
     */
    private String name;

    /**
     * 显示名称
     */
    @Column(name = "display_name")
    private String displayName;

    /**
     * 描述
     */
    private String description;

    /**
     * 获取id
     *
     * @return id - id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置id
     *
     * @param id id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取识别模型id
     *
     * @return recon_model_id - 识别模型id
     */
    public Integer getReconModelId() {
        return reconModelId;
    }

    /**
     * 设置识别模型id
     *
     * @param reconModelId 识别模型id
     */
    public void setReconModelId(Integer reconModelId) {
        this.reconModelId = reconModelId;
    }

    /**
     * 获取名称KEY
     *
     * @return name - 名称KEY
     */
    public String getName() {
        return name;
    }

    /**
     * 设置名称KEY
     *
     * @param name 名称KEY
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取显示名称
     *
     * @return display_name - 显示名称
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * 设置显示名称
     *
     * @param displayName 显示名称
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * 获取描述
     *
     * @return description - 描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置描述
     *
     * @param description 描述
     */
    public void setDescription(String description) {
        this.description = description;
    }
}