package com.ydrobot.powerplant.model;

import javax.persistence.*;

@Table(name = "recon_model_sub_img")
public class ReconModelSubImg {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 表计模型id
     */
    @Column(name = "recon_model_sub_id")
    private Integer reconModelSubId;

    /**
     * 图片
     */
    private String picture;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取表计模型id
     *
     * @return recon_model_sub_id - 表计模型id
     */
    public Integer getReconModelSubId() {
        return reconModelSubId;
    }

    /**
     * 设置表计模型id
     *
     * @param reconModelSubId 表计模型id
     */
    public void setReconModelSubId(Integer reconModelSubId) {
        this.reconModelSubId = reconModelSubId;
    }

    /**
     * 获取图片
     *
     * @return picture - 图片
     */
    public String getPicture() {
        return picture;
    }

    /**
     * 设置图片
     *
     * @param picture 图片
     */
    public void setPicture(String picture) {
        this.picture = picture;
    }
}