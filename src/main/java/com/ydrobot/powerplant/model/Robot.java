package com.ydrobot.powerplant.model;

import java.util.Date;
import javax.persistence.*;

public class Robot {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 机器人名称
     */
    private String name;

    /**
     * 变电站id
     */
    @Column(name = "device_id")
    private Integer deviceId;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取机器人名称
     *
     * @return name - 机器人名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置机器人名称
     *
     * @param name 机器人名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取变电站id
     *
     * @return device_id - 变电站id
     */
    public Integer getDeviceId() {
        return deviceId;
    }

    /**
     * 设置变电站id
     *
     * @param deviceId 变电站id
     */
    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}