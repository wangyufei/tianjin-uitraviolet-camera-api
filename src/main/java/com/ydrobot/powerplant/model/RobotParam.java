package com.ydrobot.powerplant.model;

import javax.persistence.*;

@Table(name = "robot_param")
public class RobotParam {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 机器人id
     */
    @Column(name = "robot_id")
    private Integer robotId;

    /**
     * 参数类型 0:机器人通讯中断及告警设置 1:云台控制初始化设置 2:机器人控制初始化设置
     */
    private Byte type;

    /**
     * 参数名称
     */
    private String name;

    /**
     * 参数显示名称
     */
    @Column(name = "display_name")
    private String displayName;

    /**
     * 参数值
     */
    private String value;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取机器人id
     *
     * @return robot_id - 机器人id
     */
    public Integer getRobotId() {
        return robotId;
    }

    /**
     * 设置机器人id
     *
     * @param robotId 机器人id
     */
    public void setRobotId(Integer robotId) {
        this.robotId = robotId;
    }

    /**
     * 获取参数类型 0:机器人通讯中断及告警设置 1:云台控制初始化设置 2:机器人控制初始化设置
     *
     * @return type - 参数类型 0:机器人通讯中断及告警设置 1:云台控制初始化设置 2:机器人控制初始化设置
     */
    public Byte getType() {
        return type;
    }

    /**
     * 设置参数类型 0:机器人通讯中断及告警设置 1:云台控制初始化设置 2:机器人控制初始化设置
     *
     * @param type 参数类型 0:机器人通讯中断及告警设置 1:云台控制初始化设置 2:机器人控制初始化设置
     */
    public void setType(Byte type) {
        this.type = type;
    }

    /**
     * 获取参数名称
     *
     * @return name - 参数名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置参数名称
     *
     * @param name 参数名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取参数显示名称
     *
     * @return display_name - 参数显示名称
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * 设置参数显示名称
     *
     * @param displayName 参数显示名称
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * 获取参数值
     *
     * @return value - 参数值
     */
    public String getValue() {
        return value;
    }

    /**
     * 设置参数值
     *
     * @param value 参数值
     */
    public void setValue(String value) {
        this.value = value;
    }
}