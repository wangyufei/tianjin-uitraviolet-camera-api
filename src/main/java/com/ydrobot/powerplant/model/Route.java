package com.ydrobot.powerplant.model;

import javax.persistence.*;

public class Route {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String entity;

    private String layer;

    private Integer color;

    @Column(name = "line_type")
    private String lineType;

    private Float elevation;

    @Column(name = "line_wt")
    private Float lineWt;

    @Column(name = "ref_name")
    private String refName;

    @Column(name = "line_wt1")
    private Float lineWt1;

    private String standard;

    private Integer start;

    private Integer end;

    @Column(name = "camera_pose")
    private String cameraPose;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return entity
     */
    public String getEntity() {
        return entity;
    }

    /**
     * @param entity
     */
    public void setEntity(String entity) {
        this.entity = entity;
    }

    /**
     * @return layer
     */
    public String getLayer() {
        return layer;
    }

    /**
     * @param layer
     */
    public void setLayer(String layer) {
        this.layer = layer;
    }

    /**
     * @return color
     */
    public Integer getColor() {
        return color;
    }

    /**
     * @param color
     */
    public void setColor(Integer color) {
        this.color = color;
    }

    /**
     * @return line_type
     */
    public String getLineType() {
        return lineType;
    }

    /**
     * @param lineType
     */
    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    /**
     * @return elevation
     */
    public Float getElevation() {
        return elevation;
    }

    /**
     * @param elevation
     */
    public void setElevation(Float elevation) {
        this.elevation = elevation;
    }

    /**
     * @return line_wt
     */
    public Float getLineWt() {
        return lineWt;
    }

    /**
     * @param lineWt
     */
    public void setLineWt(Float lineWt) {
        this.lineWt = lineWt;
    }

    /**
     * @return ref_name
     */
    public String getRefName() {
        return refName;
    }

    /**
     * @param refName
     */
    public void setRefName(String refName) {
        this.refName = refName;
    }

    /**
     * @return line_wt1
     */
    public Float getLineWt1() {
        return lineWt1;
    }

    /**
     * @param lineWt1
     */
    public void setLineWt1(Float lineWt1) {
        this.lineWt1 = lineWt1;
    }

    /**
     * @return standard
     */
    public String getStandard() {
        return standard;
    }

    /**
     * @param standard
     */
    public void setStandard(String standard) {
        this.standard = standard;
    }

    /**
     * @return start
     */
    public Integer getStart() {
        return start;
    }

    /**
     * @param start
     */
    public void setStart(Integer start) {
        this.start = start;
    }

    /**
     * @return end
     */
    public Integer getEnd() {
        return end;
    }

    /**
     * @param end
     */
    public void setEnd(Integer end) {
        this.end = end;
    }

    /**
     * @return camera_pose
     */
    public String getCameraPose() {
        return cameraPose;
    }

    /**
     * @param cameraPose
     */
    public void setCameraPose(String cameraPose) {
        this.cameraPose = cameraPose;
    }
}