package com.ydrobot.powerplant.model;

import javax.persistence.*;

@Table(name = "sys_alarm_type")
public class SysAlarmType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 报警类型名称
     */
    private String name;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取报警类型名称
     *
     * @return name - 报警类型名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置报警类型名称
     *
     * @param name 报警类型名称
     */
    public void setName(String name) {
        this.name = name;
    }
}