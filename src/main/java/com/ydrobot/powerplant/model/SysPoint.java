package com.ydrobot.powerplant.model;

import javax.persistence.*;

@Table(name = "sys_point")
public class SysPoint {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 名称
     */
    private String name;
    
    /**
     * 显示名称
     */
    @Column(name = "display_name")
    private String displayName;

    /**
     * 0:前端不报警显示 1:前端报警显示
     */
    @Column(name = "is_show")
    private Byte isShow;

    /**
     * 0:不保存 1:保存
     */
    @Column(name = "is_save")
    private Byte isSave;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取名称
     *
     * @return name - key值 比如1001
     */
    public String getName() {
        return name;
    }

    /**
     * 设置名称
     *
     * @param name key值
     */
    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
     * 获取0:前端不报警显示 1:前端报警显示
     *
     * @return is_show - 0:前端不报警显示 1:前端报警显示
     */
    public Byte getIsShow() {
        return isShow;
    }

    /**
     * 设置0:前端不报警显示 1:前端报警显示
     *
     * @param isShow 0:前端不报警显示 1:前端报警显示
     */
    public void setIsShow(Byte isShow) {
        this.isShow = isShow;
    }

    /**
     * 获取0:不保存 1:保存
     *
     * @return is_save - 0:不保存 1:保存
     */
    public Byte getIsSave() {
        return isSave;
    }

    /**
     * 设置0:不保存 1:保存
     *
     * @param isSave 0:不保存 1:保存
     */
    public void setIsSave(Byte isSave) {
        this.isSave = isSave;
    }
}