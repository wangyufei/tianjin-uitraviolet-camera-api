package com.ydrobot.powerplant.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "sys_point_alarm_history")
public class SysPointAlarmHistory {
	
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 机器人id
     */
    @Column(name = "robot_id")
    private Integer robotId;

    /**
     * 系统点位id
     */
    @Column(name = "sys_point_id")
    private Integer sysPointId;

    /**
     * 值
     */
    private Float value;

    /**
     * 审核意见
     */
    private String description;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;
    
    /**
     * 告警类型id
     */
    @Column(name = "alarm_type_id")
    private Integer alarmTypeId;
    
    /**
     * 告警等级 0:正常 1:预警 2:一般告警 3:严重告警 4:危机告警
     */
    @Column(name = "alarm_level")
    private Byte alarmLevel;
    
    /**
     * 审核状态 0:未审核 1:已审核
     */
    @Column(name = "check_status")
    private Byte checkStatus;

    /**
     * 审核时间
     */
    @Column(name = "check_time")
    private Date checkTime;
    
    /**
     * 审核人id
     */
    @Column(name = "operater_id")
    private Integer operaterId;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取机器人id
     *
     * @return robot_id - 机器人id
     */
    public Integer getRobotId() {
        return robotId;
    }

    /**
     * 设置机器人id
     *
     * @param robotId 机器人id
     */
    public void setRobotId(Integer robotId) {
        this.robotId = robotId;
    }

    /**
     * 获取系统点位id
     *
     * @return sys_point_id - 系统点位id
     */
    public Integer getSysPointId() {
        return sysPointId;
    }

    /**
     * 设置系统点位id
     *
     * @param sysPointId 系统点位id
     */
    public void setSysPointId(Integer sysPointId) {
        this.sysPointId = sysPointId;
    }

    /**
     * 获取值
     *
     * @return value - 值
     */
    public Float getValue() {
        return value;
    }

    /**
     * 设置值
     *
     * @param value 值
     */
    public void setValue(Float value) {
        this.value = value;
    }

    /**
     * 获取描述
     * @return
     */
    public String getDescription() {
		return description;
	}

    /**
     * 设置描述
     * @return
     */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

	public Integer getAlarmTypeId() {
		return alarmTypeId;
	}

	public void setAlarmTypeId(Integer alarmTypeId) {
		this.alarmTypeId = alarmTypeId;
	}

	public Byte getAlarmLevel() {
		return alarmLevel;
	}

	public void setAlarmLevel(Byte alarmLevel) {
		this.alarmLevel = alarmLevel;
	}

	public Byte getCheckStatus() {
		return checkStatus;
	}

	public void setCheckStatus(Byte checkStatus) {
		this.checkStatus = checkStatus;
	}

	public Date getCheckTime() {
		return checkTime;
	}

	public void setCheckTime(Date checkTime) {
		this.checkTime = checkTime;
	}

	public Integer getOperaterId() {
		return operaterId;
	}

	public void setOperaterId(Integer operaterId) {
		this.operaterId = operaterId;
	}
    
    
}