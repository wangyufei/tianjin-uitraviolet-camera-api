package com.ydrobot.powerplant.model;

import javax.persistence.*;

@Table(name = "sys_point_alarm_setting")
public class SysPointAlarmSetting {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	/**
	 * 系统点位id
	 */
	@Column(name = "sys_point_id")
	private Integer sysPointId;

	/**
	 * 限值
	 */
	@Column(name = "limit_value")
	private Float limitValue;

	/**
	 * 值类型 0:数值 1:百分比 2:bool值
	 */
	@Column(name = "value_type")
	private Integer valueType;

	/**
	 * 0:上限 1:下限
	 */
	@Column(name = "up_or_down")
	private Byte upOrDown;

	/**
	 * 告警类型id 与alarm_type表id相关连
	 * 
	 * 1：超温报警 2：升温报警 3：三相对比报警 4：三相温差报警 5：越值报警（关心上下限）
	 * 
	 */
	@Column(name = "alarm_type_id")
	private Integer alarmTypeId;
	
	/**
     * 告警等级： 0正常，1预告，2一般告警，3严重告警，4危急告警
     */
    @Column(name = "alarm_level")
    private Byte alarmLevel;
    
    /**
     * 报警描述
     */
    private String description;

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 获取系统点位id
	 *
	 * @return sys_point_id - 系统点位id
	 */
	public Integer getSysPointId() {
		return sysPointId;
	}

	/**
	 * 设置系统点位id
	 *
	 * @param sysPointId
	 *            系统点位id
	 */
	public void setSysPointId(Integer sysPointId) {
		this.sysPointId = sysPointId;
	}

	/**
	 * 获取限值
	 *
	 * @return limit_value - 限值
	 */
	public Float getLimitValue() {
		return limitValue;
	}

	/**
	 * 设置限值
	 *
	 * @param limitValue
	 *            限值
	 */
	public void setLimitValue(Float limitValue) {
		this.limitValue = limitValue;
	}

	/**
	 * 获取值类型 0:数值 1:百分比 2:bool值
	 *
	 * @return value_type - 值类型 0:数值 1:百分比 2:bool值
	 */
	public Integer getValueType() {
		return valueType;
	}

	/**
	 * 设置值类型 0:数值 1:百分比 2:bool值
	 *
	 * @param valueType
	 *            值类型 0:数值 1:百分比 2:bool值
	 */
	public void setValueType(Integer valueType) {
		this.valueType = valueType;
	}

	/**
	 * 获取0:上限 1:下限
	 *
	 * @return up_or_down - 0:上限 1:下限
	 */
	public Byte getUpOrDown() {
		return upOrDown;
	}

	/**
	 * 设置0:上限 1:下限
	 *
	 * @param upOrDown
	 *            0:上限 1:下限
	 */
	public void setUpOrDown(Byte upOrDown) {
		this.upOrDown = upOrDown;
	}

	public Integer getAlarmTypeId() {
		return alarmTypeId;
	}

	public void setAlarmTypeId(Integer alarmTypeId) {
		this.alarmTypeId = alarmTypeId;
	}

	public Byte getAlarmLevel() {
		return alarmLevel;
	}

	public void setAlarmLevel(Byte alarmLevel) {
		this.alarmLevel = alarmLevel;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}