package com.ydrobot.powerplant.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "task_history")
public class TaskHistory {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 机器人id
     */
    @Column(name = "robot_id")
    private Integer robotId;

    /**
     * 任务id
     */
    @Column(name = "task_id")
    private Integer taskId;

    /**
     * 任务开始时间
     */
    @Column(name = "task_start_time")
    private Date taskStartTime;

    /**
     * 任务结束时间
     */
    @Column(name = "task_end_time")
    private Date taskEndTime;

    /**
     * 任务状态 0:已执行 1:终止 2:暂停 3:正在执行 4:未执行 5:超期 6:预执行
     */
    @Column(name = "task_status")
    private Byte taskStatus;

    /**
     * 温度
     */
    private Short temp;

    /**
     * 湿度
     */
    private Short hum;

    /**
     * 风速
     */
    private Short wind;

    /**
     * 天气情况
     */
    private String weather;

    /**
     * 审核状态 0:未审核 1:已审核
     */
    @Column(name = "check_status")
    private Byte checkStatus;

    /**
     * 审核意见
     */
    @Column(name = "check_desc")
    private String checkDesc;

    /**
     * 审核时间
     */
    @Column(name = "check_time")
    private Date checkTime;

    /**
     * 操作人id
     */
    @Column(name = "operater_id")
    private Integer operaterId;

    /**
     * 预估总运行时间
     */
    @Column(name = "total_run_time")
    private Integer totalRunTime;

    /**
     * 累计运行时间
     */
    @Column(name = "cumulative_run_time")
    private Integer cumulativeRunTime;

    /**
     * 重置开始时间
     */
    @Column(name = "restart_start_time")
    private Date restartStartTime;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取机器人id
     *
     * @return robot_id - 机器人id
     */
    public Integer getRobotId() {
        return robotId;
    }

    /**
     * 设置机器人id
     *
     * @param robotId 机器人id
     */
    public void setRobotId(Integer robotId) {
        this.robotId = robotId;
    }

    /**
     * 获取任务id
     *
     * @return task_id - 任务id
     */
    public Integer getTaskId() {
        return taskId;
    }

    /**
     * 设置任务id
     *
     * @param taskId 任务id
     */
    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    /**
     * 获取任务开始时间
     *
     * @return task_start_time - 任务开始时间
     */
    public Date getTaskStartTime() {
        return taskStartTime;
    }

    /**
     * 设置任务开始时间
     *
     * @param taskStartTime 任务开始时间
     */
    public void setTaskStartTime(Date taskStartTime) {
        this.taskStartTime = taskStartTime;
    }

    /**
     * 获取任务结束时间
     *
     * @return task_end_time - 任务结束时间
     */
    public Date getTaskEndTime() {
        return taskEndTime;
    }

    /**
     * 设置任务结束时间
     *
     * @param taskEndTime 任务结束时间
     */
    public void setTaskEndTime(Date taskEndTime) {
        this.taskEndTime = taskEndTime;
    }

    /**
     * 获取任务状态 0:已执行 1:终止 2:暂停 3:正在执行 4:未执行 5:超期 6:预执行
     *
     * @return task_status - 任务状态 0:已执行 1:终止 2:暂停 3:正在执行 4:未执行 5:超期 6:预执行
     */
    public Byte getTaskStatus() {
        return taskStatus;
    }

    /**
     * 设置任务状态 0:已执行 1:终止 2:暂停 3:正在执行 4:未执行 5:超期 6:预执行
     *
     * @param taskStatus 任务状态 0:已执行 1:终止 2:暂停 3:正在执行 4:未执行 5:超期 6:预执行
     */
    public void setTaskStatus(Byte taskStatus) {
        this.taskStatus = taskStatus;
    }

    /**
     * 获取温度
     *
     * @return temp - 温度
     */
    public Short getTemp() {
        return temp;
    }

    /**
     * 设置温度
     *
     * @param temp 温度
     */
    public void setTemp(Short temp) {
        this.temp = temp;
    }

    /**
     * 获取湿度
     *
     * @return hum - 湿度
     */
    public Short getHum() {
        return hum;
    }

    /**
     * 设置湿度
     *
     * @param hum 湿度
     */
    public void setHum(Short hum) {
        this.hum = hum;
    }

    /**
     * 获取风速
     *
     * @return wind - 风速
     */
    public Short getWind() {
        return wind;
    }

    /**
     * 设置风速
     *
     * @param wind 风速
     */
    public void setWind(Short wind) {
        this.wind = wind;
    }

    /**
     * 获取天气情况
     *
     * @return weather - 天气情况
     */
    public String getWeather() {
        return weather;
    }

    /**
     * 设置天气情况
     *
     * @param weather 天气情况
     */
    public void setWeather(String weather) {
        this.weather = weather;
    }

    /**
     * 获取审核状态 0:未审核 1:已审核
     *
     * @return check_status - 审核状态 0:未审核 1:已审核
     */
    public Byte getCheckStatus() {
        return checkStatus;
    }

    /**
     * 设置审核状态 0:未审核 1:已审核
     *
     * @param checkStatus 审核状态 0:未审核 1:已审核
     */
    public void setCheckStatus(Byte checkStatus) {
        this.checkStatus = checkStatus;
    }

    /**
     * 获取审核意见
     *
     * @return check_desc - 审核意见
     */
    public String getCheckDesc() {
        return checkDesc;
    }

    /**
     * 设置审核意见
     *
     * @param checkDesc 审核意见
     */
    public void setCheckDesc(String checkDesc) {
        this.checkDesc = checkDesc;
    }

    /**
     * 获取审核时间
     *
     * @return check_time - 审核时间
     */
    public Date getCheckTime() {
        return checkTime;
    }

    /**
     * 设置审核时间
     *
     * @param checkTime 审核时间
     */
    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    /**
     * 获取操作人id
     *
     * @return operater_id - 操作人id
     */
    public Integer getOperaterId() {
        return operaterId;
    }

    /**
     * 设置操作人id
     *
     * @param operaterId 操作人id
     */
    public void setOperaterId(Integer operaterId) {
        this.operaterId = operaterId;
    }

    /**
     * 获取预估总运行时间
     *
     * @return total_run_time - 预估总运行时间
     */
    public Integer getTotalRunTime() {
        return totalRunTime;
    }

    /**
     * 设置预估总运行时间
     *
     * @param totalRunTime 预估总运行时间
     */
    public void setTotalRunTime(Integer totalRunTime) {
        this.totalRunTime = totalRunTime;
    }

    /**
     * 获取累计运行时间
     *
     * @return cumulative_run_time - 累计运行时间
     */
    public Integer getCumulativeRunTime() {
        return cumulativeRunTime;
    }

    /**
     * 设置累计运行时间
     *
     * @param cumulativeRunTime 累计运行时间
     */
    public void setCumulativeRunTime(Integer cumulativeRunTime) {
        this.cumulativeRunTime = cumulativeRunTime;
    }

    /**
     * 获取重置开始时间
     *
     * @return restart_start_time - 重置开始时间
     */
    public Date getRestartStartTime() {
        return restartStartTime;
    }

    /**
     * 设置重置开始时间
     *
     * @param restartStartTime 重置开始时间
     */
    public void setRestartStartTime(Date restartStartTime) {
        this.restartStartTime = restartStartTime;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}