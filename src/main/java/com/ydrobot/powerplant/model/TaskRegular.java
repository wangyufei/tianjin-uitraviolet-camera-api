package com.ydrobot.powerplant.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "task_regular")
public class TaskRegular {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 任务id
     */
    @Column(name = "task_id")
    private Integer taskId;

    /**
     * 任务优先级
     */
    @Column(name = "task_level")
    private Integer taskLevel;

    @Column(name = "robot_id")
    private Integer robotId;

    /**
     * 执行时间
     */
    @Column(name = "execute_time")
    private Date executeTime;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取任务id
     *
     * @return task_id - 任务id
     */
    public Integer getTaskId() {
        return taskId;
    }

    /**
     * 设置任务id
     *
     * @param taskId 任务id
     */
    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    /**
     * 获取任务优先级
     *
     * @return task_level - 任务优先级
     */
    public Integer getTaskLevel() {
        return taskLevel;
    }

    /**
     * 设置任务优先级
     *
     * @param taskLevel 任务优先级
     */
    public void setTaskLevel(Integer taskLevel) {
        this.taskLevel = taskLevel;
    }

    /**
     * @return robot_id
     */
    public Integer getRobotId() {
        return robotId;
    }

    /**
     * @param robotId
     */
    public void setRobotId(Integer robotId) {
        this.robotId = robotId;
    }

    /**
     * 获取执行时间
     *
     * @return execute_time - 执行时间
     */
    public Date getExecuteTime() {
        return executeTime;
    }

    /**
     * 设置执行时间
     *
     * @param executeTime 执行时间
     */
    public void setExecuteTime(Date executeTime) {
        this.executeTime = executeTime;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}