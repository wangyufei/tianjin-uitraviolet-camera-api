package com.ydrobot.powerplant.model;

import java.util.Date;
import javax.persistence.*;

public class User {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 工号
     */
    private String sn;

    /**
     * 名字
     */
    private String name;

    /**
     * 账号
     */
    private String account;

    /**
     * 密码
     */
    private String password;

    /**
     * 电话
     */
    private String mobile;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 所属组织id
     */
    @Column(name = "organization_id")
    private Integer organizationId;

    /**
     * 登录IP
     */
    @Column(name = "login_ip")
    private String loginIp;

    /**
     * 最后一次登录时间
     */
    @Column(name = "last_login_time")
    private Date lastLoginTime;

    /**
     * 0:不开启 1:开启
     */
    @Column(name = "is_open_sms")
    private Byte isOpenSms;

    /**
     * 有效时间
     */
    @Column(name = "validity_time")
    private Date validityTime;

    /**
     * 登陆失败次数
     */
    @Column(name = "login_fail_num")
    private Integer loginFailNum;

    /**
     * 登陆禁用时间
     */
    @Column(name = "login_forbidden_time")
    private Date loginForbiddenTime;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取工号
     *
     * @return sn - 工号
     */
    public String getSn() {
        return sn;
    }

    /**
     * 设置工号
     *
     * @param sn 工号
     */
    public void setSn(String sn) {
        this.sn = sn;
    }

    /**
     * 获取名字
     *
     * @return name - 名字
     */
    public String getName() {
        return name;
    }

    /**
     * 设置名字
     *
     * @param name 名字
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取账号
     *
     * @return account - 账号
     */
    public String getAccount() {
        return account;
    }

    /**
     * 设置账号
     *
     * @param account 账号
     */
    public void setAccount(String account) {
        this.account = account;
    }

    /**
     * 获取密码
     *
     * @return password - 密码
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置密码
     *
     * @param password 密码
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 获取电话
     *
     * @return mobile - 电话
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 设置电话
     *
     * @param mobile 电话
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 获取邮箱
     *
     * @return email - 邮箱
     */
    public String getEmail() {
        return email;
    }

    /**
     * 设置邮箱
     *
     * @param email 邮箱
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 获取所属组织id
     *
     * @return organization_id - 所属组织id
     */
    public Integer getOrganizationId() {
        return organizationId;
    }

    /**
     * 设置所属组织id
     *
     * @param organizationId 所属组织id
     */
    public void setOrganizationId(Integer organizationId) {
        this.organizationId = organizationId;
    }

    /**
     * 获取登录IP
     *
     * @return login_ip - 登录IP
     */
    public String getLoginIp() {
        return loginIp;
    }

    /**
     * 设置登录IP
     *
     * @param loginIp 登录IP
     */
    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    /**
     * 获取最后一次登录时间
     *
     * @return last_login_time - 最后一次登录时间
     */
    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    /**
     * 设置最后一次登录时间
     *
     * @param lastLoginTime 最后一次登录时间
     */
    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    /**
     * 获取0:不开启 1:开启
     *
     * @return is_open_sms - 0:不开启 1:开启
     */
    public Byte getIsOpenSms() {
        return isOpenSms;
    }

    /**
     * 设置0:不开启 1:开启
     *
     * @param isOpenSms 0:不开启 1:开启
     */
    public void setIsOpenSms(Byte isOpenSms) {
        this.isOpenSms = isOpenSms;
    }

    /**
     * 获取有效时间
     *
     * @return validity_time - 有效时间
     */
    public Date getValidityTime() {
        return validityTime;
    }

    /**
     * 设置有效时间
     *
     * @param validityTime 有效时间
     */
    public void setValidityTime(Date validityTime) {
        this.validityTime = validityTime;
    }

    /**
     * 获取登陆失败次数
     *
     * @return login_fail_num - 登陆失败次数
     */
    public Integer getLoginFailNum() {
        return loginFailNum;
    }

    /**
     * 设置登陆失败次数
     *
     * @param loginFailNum 登陆失败次数
     */
    public void setLoginFailNum(Integer loginFailNum) {
        this.loginFailNum = loginFailNum;
    }

    /**
     * 获取登陆禁用时间
     *
     * @return login_forbidden_time - 登陆禁用时间
     */
    public Date getLoginForbiddenTime() {
        return loginForbiddenTime;
    }

    /**
     * 设置登陆禁用时间
     *
     * @param loginForbiddenTime 登陆禁用时间
     */
    public void setLoginForbiddenTime(Date loginForbiddenTime) {
        this.loginForbiddenTime = loginForbiddenTime;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}