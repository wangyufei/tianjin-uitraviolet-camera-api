package com.ydrobot.powerplant.model.condition;

import io.swagger.annotations.ApiModelProperty;

public class DeviceIntervalQueryParam {

    @ApiModelProperty(name = "deviceAreaId", value = "设备区域id")
    private Integer deviceAreaId;

    @ApiModelProperty(name = "intervalName", value = "间隔名称")
    private String intervalName;

    public Integer getDeviceAreaId() {
        return deviceAreaId;
    }

    public void setDeviceAreaId(Integer deviceAreaId) {
        this.deviceAreaId = deviceAreaId;
    }

    public String getIntervalName() {
        return intervalName;
    }

    public void setIntervalName(String intervalName) {
        this.intervalName = intervalName;
    }

}
