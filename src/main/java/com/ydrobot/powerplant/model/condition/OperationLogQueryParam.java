package com.ydrobot.powerplant.model.condition;

import io.swagger.annotations.ApiModelProperty;

public class OperationLogQueryParam {

    @ApiModelProperty(name = "type", value = "日志类型 0:系统 1:机器人 2:客户端")
    private Integer type;

    @ApiModelProperty(name = "startTime", value = "开始时间")
    private String startTime;

    @ApiModelProperty(name = "endTime", value = "结束时间")
    private String endTime;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

}
