package com.ydrobot.powerplant.model.condition;

import io.swagger.annotations.ApiModelProperty;

public class PointAppraiseQueryParam {

	@ApiModelProperty(name = "stationId", value = "站点id")
    private Integer stationId;
	
	@ApiModelProperty(name = "objectId", value = "对象id")
    private Integer objectId;

	public Integer getStationId() {
		return stationId;
	}

	public void setStationId(Integer stationId) {
		this.stationId = stationId;
	}

	public Integer getObjectId() {
		return objectId;
	}

	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}

}
