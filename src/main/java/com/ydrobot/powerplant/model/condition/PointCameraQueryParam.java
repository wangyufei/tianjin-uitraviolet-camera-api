package com.ydrobot.powerplant.model.condition;

import io.swagger.annotations.ApiModelProperty;

public class PointCameraQueryParam {

    @ApiModelProperty(name = "robotId", value = "机器人id")
    private Integer robotId;
    
    @ApiModelProperty(name = "pointId", value = "点位id 多个用逗号分隔")
    private String pointId;

	public Integer getRobotId() {
		return robotId;
	}

	public void setRobotId(Integer robotId) {
		this.robotId = robotId;
	}

	public String getPointId() {
		return pointId;
	}

	public void setPointId(String pointId) {
		this.pointId = pointId;
	}

	
}
