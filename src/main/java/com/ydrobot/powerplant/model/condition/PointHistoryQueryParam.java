package com.ydrobot.powerplant.model.condition;

import io.swagger.annotations.ApiModelProperty;

public class PointHistoryQueryParam {

    @ApiModelProperty(name = "page", value = "当前页")
    private Integer page = 1;

    @ApiModelProperty(name = "size", value = "每页显示数量")
    private Integer size = 10;

    @ApiModelProperty(name = "robotId", value = "机器人id")
    private Integer robotId;

    @ApiModelProperty(name = "taskHistoryId", value = "任务执行记录id")
    private Integer taskHistoryId;

    @ApiModelProperty(name = "pointIds", value = "点位集合，多个逗号分隔")
    private String pointIds;

    @ApiModelProperty(name = "startTime", value = "开始时间")
    private String startTime;

    @ApiModelProperty(name = "endTime", value = "结束时间")
    private String endTime;

    @ApiModelProperty(name = "saveType", value = "保存类型 1:红外  2:可见光 3:音频")
    private Integer saveType;

    @ApiModelProperty(name = "checkStatus", value = "审核状态 0:未审核 1:已审核")
    private Integer checkStatus;

    @ApiModelProperty(name = "reconStatus", value = "识别状态 0:正确 1:错误 2:失败")
    private Byte reconStatus;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getPointIds() {
        return pointIds;
    }

    public void setPointIds(String pointIds) {
        this.pointIds = pointIds;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getSaveType() {
        return saveType;
    }

    public void setSaveType(Integer saveType) {
        this.saveType = saveType;
    }

    public Integer getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }

    public Integer getTaskHistoryId() {
        return taskHistoryId;
    }

    public void setTaskHistoryId(Integer taskHistoryId) {
        this.taskHistoryId = taskHistoryId;
    }

    public Byte getReconStatus() {
        return reconStatus;
    }

    public void setReconStatus(Byte reconStatus) {
        this.reconStatus = reconStatus;
    }

    public Integer getRobotId() {
        return robotId;
    }

    public void setRobotId(Integer robotId) {
        this.robotId = robotId;
    }

}
