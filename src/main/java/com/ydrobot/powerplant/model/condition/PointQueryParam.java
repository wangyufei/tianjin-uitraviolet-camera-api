package com.ydrobot.powerplant.model.condition;

import io.swagger.annotations.ApiModelProperty;

public class PointQueryParam {

    @ApiModelProperty(name = "isDelete", value = "是否删除 0:正常 1:删除")
    private Byte isDelete;
    
    @ApiModelProperty(name = "objectId", value = "对象id")
    private Integer objectId;

    public Byte getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Byte isDelete) {
        this.isDelete = isDelete;
    }

	public Integer getObjectId() {
		return objectId;
	}

	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}

    
}
