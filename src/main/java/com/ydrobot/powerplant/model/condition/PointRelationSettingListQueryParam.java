package com.ydrobot.powerplant.model.condition;

import io.swagger.annotations.ApiModelProperty;

public class PointRelationSettingListQueryParam {

    @ApiModelProperty(name = "page", value = "当前页")
    private Integer page = 1;

    @ApiModelProperty(name = "size", value = "每页显示数量")
    private Integer size = 10;

    @ApiModelProperty(name = "pointIds", value = "点位id集合,多个逗号分隔")
    private String pointIds;

    @ApiModelProperty(name = "type", value = "类型 0:三相温差 1:三相对比")
    private Byte type;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getPointIds() {
        return pointIds;
    }

    public void setPointIds(String pointIds) {
        this.pointIds = pointIds;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

}
