package com.ydrobot.powerplant.model.condition;

import io.swagger.annotations.ApiModelProperty;

public class PointTreeQueryParam {
    
	@ApiModelProperty(name = "stationId", value = "变电站id")
    private Integer stationId;
	
    @ApiModelProperty(name = "objectIds", value = "对象id")
    private String objectIds;

	public Integer getStationId() {
		return stationId;
	}

	public void setStationId(Integer stationId) {
		this.stationId = stationId;
	}

	public String getObjectIds() {
		return objectIds;
	}

	public void setObjectIds(String objectIds) {
		this.objectIds = objectIds;
	}
    
}
