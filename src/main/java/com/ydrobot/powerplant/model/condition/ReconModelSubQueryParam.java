package com.ydrobot.powerplant.model.condition;

import io.swagger.annotations.ApiModelProperty;

public class ReconModelSubQueryParam {

    @ApiModelProperty(name = "reconModelId", value = "识别模型id")
    private Integer reconModelId;

    public Integer getReconModelId() {
        return reconModelId;
    }

    public void setReconModelId(Integer reconModelId) {
        this.reconModelId = reconModelId;
    }
}
