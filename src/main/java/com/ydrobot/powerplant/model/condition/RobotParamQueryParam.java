package com.ydrobot.powerplant.model.condition;

import io.swagger.annotations.ApiModelProperty;

public class RobotParamQueryParam {

    @ApiModelProperty(name = "robotId", value = "机器人id")
    private Integer robotId;

    @ApiModelProperty(name = "type", value = "参数类型 0:机器人通讯中断及告警设置 1:云台控制初始化设置 2:机器人控制初始化设置")
    private Integer type;

    @ApiModelProperty(name = "name", value = "属性key")
    private String name;

    public Integer getRobotId() {
        return robotId;
    }

    public void setRobotId(Integer robotId) {
        this.robotId = robotId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
