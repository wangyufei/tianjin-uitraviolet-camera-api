package com.ydrobot.powerplant.model.condition;

import io.swagger.annotations.ApiModelProperty;

public class SysPointAlarmHistoryQueryParam {

	@ApiModelProperty(name = "sysPointId", value = "系统点位id")
    private Integer sysPointId;
	
    @ApiModelProperty(name = "robotId", value = "机器人id")
    private Integer robotId;

    @ApiModelProperty(name = "startTime", value = "开始时间")
    private String startTime;

    @ApiModelProperty(name = "endTime", value = "结束时间")
    private String endTime;
    
    @ApiModelProperty(name = "checkStatus", value = "审核状态 0:未审核 1:已审核")
    private Integer checkStatus;

	public Integer getSysPointId() {
		return sysPointId;
	}

	public void setSysPointId(Integer sysPointId) {
		this.sysPointId = sysPointId;
	}

	public Integer getRobotId() {
		return robotId;
	}

	public void setRobotId(Integer robotId) {
		this.robotId = robotId;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Integer getCheckStatus() {
		return checkStatus;
	}

	public void setCheckStatus(Integer checkStatus) {
		this.checkStatus = checkStatus;
	}
}
