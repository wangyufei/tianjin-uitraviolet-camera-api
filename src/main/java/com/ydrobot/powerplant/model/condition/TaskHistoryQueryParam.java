package com.ydrobot.powerplant.model.condition;

import io.swagger.annotations.ApiModelProperty;

public class TaskHistoryQueryParam {

    @ApiModelProperty(name = "robotId", value = "机器人id")
    private Integer robotId;

    @ApiModelProperty(name = "taskId", value = "任务id")
    private Integer taskId;

    @ApiModelProperty(name = "startTime", value = "开始时间")
    private String startTime;

    @ApiModelProperty(name = "endTime", value = "结束时间")
    private String endTime;

    @ApiModelProperty(name = "inspectTypeId", value = "巡检类型id,多个用逗号分隔")
    private String inspectTypeId;

    @ApiModelProperty(name = "taskName", value = "任务名称")
    private String taskName;

    @ApiModelProperty(name = "taskStatus", value = "任务状态")
    private Integer taskStatus;

    @ApiModelProperty(name = "checkStatus", value = "审核状态 0:未审核 1:已审核")
    private Integer checkStatus;

    public Integer getRobotId() {
        return robotId;
    }

    public void setRobotId(Integer robotId) {
        this.robotId = robotId;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getInspectTypeId() {
        return inspectTypeId;
    }

    public void setInspectTypeId(String inspectTypeId) {
        this.inspectTypeId = inspectTypeId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Integer getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(Integer taskStatus) {
        this.taskStatus = taskStatus;
    }

    public Integer getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }

}
