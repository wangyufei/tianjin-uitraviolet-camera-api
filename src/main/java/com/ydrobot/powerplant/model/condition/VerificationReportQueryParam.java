package com.ydrobot.powerplant.model.condition;

import io.swagger.annotations.ApiModelProperty;

public class VerificationReportQueryParam {
	
	@ApiModelProperty(name = "taskHistoryId", required = true, value = "任务执行记录id")
	private Integer taskHistoryId;

	public Integer getTaskHistoryId() {
		return taskHistoryId;
	}

	public void setTaskHistoryId(Integer taskHistoryId) {
		this.taskHistoryId = taskHistoryId;
	}
	
}
