package com.ydrobot.powerplant.model.datasync;

import java.util.Date;

public class AlarmHistorySync {

	/**
     * 巡检点位历史记录id
     */
    private Integer pointHistoryId;

    /**
     * 任务id
     */
    private Integer taskId;

    /**
     * 任务执行记录id
     */
    private Integer taskHistoryId;

    /**
     * 机器人id
     */
    private Integer robotId;

    /**
     * 对象id
     */
    private Integer objectId;

    /**
     * 对象名称
     */
    private String objectName;

    /**
     * 对象类型id
     */
    private Integer objectTypeId;

    /**
     * 巡检点id
     */
    private Integer pointId;

    /**
     * 部件名称
     */
    private String pointName;

    /**
     * 告警类型id
     */
    private Integer alarmTypeId;

    /**
     * 识别时间
     */
    private Date reconTime;

    /**
     * 告警等级 0:正常 1:预警 2:一般告警 3:严重告警 4:危机告警
     */
    private Byte alarmLevel;

    /**
     * 结果状态 0:结果正常 1:结果异常
     */
    private Byte resultStatus;

    /**
     * 异常标识 0:正常 1:异常
     */
    private Byte errorFlag;

    /**
     * 审核状态 0:未审核 1:已审核
     */
    private Byte checkStatus;

    /**
     * 审核时间
     */
    private Date checkTime;

    /**
     * 审核人id
     */
    private Integer operaterId;

    /**
     * 审核意见
     */
    private String checkDesc;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 站点id
     */
    private Integer stationId;
    
    /**
     * 来源
     */
    private Byte source;

    /**
     * 站端数据id
     */
    private Integer extendId;
    
    /**
     * 巡检类型id
     */
    private Integer inspectTypeId;
    
    /**
     * 二次确认任务id
     */
    private Integer twiceConfirmTaskId;
    
    /**
     * 获取巡检点位历史记录id
     *
     * @return point_history_id - 巡检点位历史记录id
     */
    public Integer getPointHistoryId() {
        return pointHistoryId;
    }

    /**
     * 设置巡检点位历史记录id
     *
     * @param pointHistoryId 巡检点位历史记录id
     */
    public void setPointHistoryId(Integer pointHistoryId) {
        this.pointHistoryId = pointHistoryId;
    }

    /**
     * 获取任务id
     *
     * @return task_id - 任务id
     */
    public Integer getTaskId() {
        return taskId;
    }

    /**
     * 设置任务id
     *
     * @param taskId 任务id
     */
    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    /**
     * 获取任务执行记录id
     *
     * @return task_history_id - 任务执行记录id
     */
    public Integer getTaskHistoryId() {
        return taskHistoryId;
    }

    /**
     * 设置任务执行记录id
     *
     * @param taskHistoryId 任务执行记录id
     */
    public void setTaskHistoryId(Integer taskHistoryId) {
        this.taskHistoryId = taskHistoryId;
    }

    /**
     * 获取机器人id
     *
     * @return robot_id - 机器人id
     */
    public Integer getRobotId() {
        return robotId;
    }

    /**
     * 设置机器人id
     *
     * @param robotId 机器人id
     */
    public void setRobotId(Integer robotId) {
        this.robotId = robotId;
    }

    /**
     * 获取对象id
     *
     * @return object_id - 对象id
     */
    public Integer getObjectId() {
        return objectId;
    }

    /**
     * 设置对象id
     *
     * @param objectId 对象id
     */
    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    /**
     * 获取对象名称
     *
     * @return object_name - 对象名称
     */
    public String getObjectName() {
        return objectName;
    }

    /**
     * 设置对象名称
     *
     * @param objectName 对象名称
     */
    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    /**
     * 获取对象类型id
     *
     * @return object_type_id - 对象类型id
     */
    public Integer getObjectTypeId() {
        return objectTypeId;
    }

    /**
     * 设置对象类型id
     *
     * @param objectTypeId 对象类型id
     */
    public void setObjectTypeId(Integer objectTypeId) {
        this.objectTypeId = objectTypeId;
    }

    /**
     * 获取巡检点id
     *
     * @return point_id - 巡检点id
     */
    public Integer getPointId() {
        return pointId;
    }

    /**
     * 设置巡检点id
     *
     * @param pointId 巡检点id
     */
    public void setPointId(Integer pointId) {
        this.pointId = pointId;
    }

    /**
     * 获取部件名称
     *
     * @return point_name - 部件名称
     */
    public String getPointName() {
        return pointName;
    }

    /**
     * 设置部件名称
     *
     * @param pointName 部件名称
     */
    public void setPointName(String pointName) {
        this.pointName = pointName;
    }

    /**
     * 获取告警类型id
     *
     * @return alarm_type_id - 告警类型id
     */
    public Integer getAlarmTypeId() {
        return alarmTypeId;
    }

    /**
     * 设置告警类型id
     *
     * @param alarmTypeId 告警类型id
     */
    public void setAlarmTypeId(Integer alarmTypeId) {
        this.alarmTypeId = alarmTypeId;
    }

    /**
     * 获取识别时间
     *
     * @return recon_time - 识别时间
     */
    public Date getReconTime() {
        return reconTime;
    }

    /**
     * 设置识别时间
     *
     * @param reconTime 识别时间
     */
    public void setReconTime(Date reconTime) {
        this.reconTime = reconTime;
    }

    /**
     * 获取告警等级 0:正常 1:预警 2:一般告警 3:严重告警 4:危机告警
     *
     * @return alarm_level - 告警等级 0:正常 1:预警 2:一般告警 3:严重告警 4:危机告警
     */
    public Byte getAlarmLevel() {
        return alarmLevel;
    }

    /**
     * 设置告警等级 0:正常 1:预警 2:一般告警 3:严重告警 4:危机告警
     *
     * @param alarmLevel 告警等级 0:正常 1:预警 2:一般告警 3:严重告警 4:危机告警
     */
    public void setAlarmLevel(Byte alarmLevel) {
        this.alarmLevel = alarmLevel;
    }

    /**
     * 获取结果状态 0:结果正常 1:结果异常
     *
     * @return result_status - 结果状态 0:结果正常 1:结果异常
     */
    public Byte getResultStatus() {
        return resultStatus;
    }

    /**
     * 设置结果状态 0:结果正常 1:结果异常
     *
     * @param resultStatus 结果状态 0:结果正常 1:结果异常
     */
    public void setResultStatus(Byte resultStatus) {
        this.resultStatus = resultStatus;
    }

    /**
     * 获取异常标识 0:正常 1:异常
     *
     * @return error_flag - 异常标识 0:正常 1:异常
     */
    public Byte getErrorFlag() {
        return errorFlag;
    }

    /**
     * 设置异常标识 0:正常 1:异常
     *
     * @param errorFlag 异常标识 0:正常 1:异常
     */
    public void setErrorFlag(Byte errorFlag) {
        this.errorFlag = errorFlag;
    }

    /**
     * 获取审核状态 0:未审核 1:已审核
     *
     * @return check_status - 审核状态 0:未审核 1:已审核
     */
    public Byte getCheckStatus() {
        return checkStatus;
    }

    /**
     * 设置审核状态 0:未审核 1:已审核
     *
     * @param checkStatus 审核状态 0:未审核 1:已审核
     */
    public void setCheckStatus(Byte checkStatus) {
        this.checkStatus = checkStatus;
    }

    /**
     * 获取审核时间
     *
     * @return check_time - 审核时间
     */
    public Date getCheckTime() {
        return checkTime;
    }

    /**
     * 设置审核时间
     *
     * @param checkTime 审核时间
     */
    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    /**
     * 获取审核人id
     *
     * @return operater_id - 审核人id
     */
    public Integer getOperaterId() {
        return operaterId;
    }

    /**
     * 设置审核人id
     *
     * @param operaterId 审核人id
     */
    public void setOperaterId(Integer operaterId) {
        this.operaterId = operaterId;
    }

    /**
     * 获取审核意见
     *
     * @return check_desc - 审核意见
     */
    public String getCheckDesc() {
        return checkDesc;
    }

    /**
     * 设置审核意见
     *
     * @param checkDesc 审核意见
     */
    public void setCheckDesc(String checkDesc) {
        this.checkDesc = checkDesc;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取站点id
     *
     * @return station_id - 站点id
     */
    public Integer getStationId() {
        return stationId;
    }

    /**
     * 设置站点id
     *
     * @param stationId 站点id
     */
    public void setStationId(Integer stationId) {
        this.stationId = stationId;
    }

    /**
     * 获取站端数据id
     *
     * @return extend_id - 站端数据id
     */
    public Integer getExtendId() {
        return extendId;
    }

    /**
     * 设置站端数据id
     *
     * @param extendId 站端数据id
     */
    public void setExtendId(Integer extendId) {
        this.extendId = extendId;
    }

	public Byte getSource() {
		return source;
	}

	public void setSource(Byte source) {
		this.source = source;
	}

	public Integer getInspectTypeId() {
		return inspectTypeId;
	}

	public void setInspectTypeId(Integer inspectTypeId) {
		this.inspectTypeId = inspectTypeId;
	}

	public Integer getTwiceConfirmTaskId() {
		return twiceConfirmTaskId;
	}

	public void setTwiceConfirmTaskId(Integer twiceConfirmTaskId) {
		this.twiceConfirmTaskId = twiceConfirmTaskId;
	}
}
