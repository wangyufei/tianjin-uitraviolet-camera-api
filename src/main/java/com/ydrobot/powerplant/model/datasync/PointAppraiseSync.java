package com.ydrobot.powerplant.model.datasync;

import com.ydrobot.powerplant.model.PointAppraise;

public class PointAppraiseSync extends PointAppraise{
	
	/**
	 * 变电站id
	 */
	private Integer stationId;
	
	/**
	 * 站端数据id
	 */
	private Integer extendId;

	public Integer getStationId() {
		return stationId;
	}

	public void setStationId(Integer stationId) {
		this.stationId = stationId;
	}

	public Integer getExtendId() {
		return extendId;
	}

	public void setExtendId(Integer extendId) {
		this.extendId = extendId;
	}
	
	
}
