package com.ydrobot.powerplant.model.dto;

import java.util.Date;

import com.ydrobot.powerplant.model.PathPlanning;
import com.ydrobot.powerplant.model.Point;

import io.swagger.annotations.ApiModelProperty;

public class CurrentTaskInfoResult {

    @ApiModelProperty(name = "type", value = "类型 0:无任务 1:有任务正在执行 2:一键返航")
    private Integer type;

    @ApiModelProperty(name = "taskName", value = "巡检任务名称")
    private String taskName;

    @ApiModelProperty(name = "taskId", value = "任务id")
    private Integer taskId;

    @ApiModelProperty(name = "inspectTypeId", value = "巡检类型id")
    private Integer inspectTypeId;

    @ApiModelProperty(name = "taskHistoryId", value = "任务历史id")
    private Integer taskHistoryId;

    @ApiModelProperty(name = "pointTotal", value = "巡检点总数")
    private Integer pointTotal;

    @ApiModelProperty(name = "abnormalPointNum", value = "异常巡检点总数")
    private Integer abnormalPointNum;

    @ApiModelProperty(name = "currentPoint", value = "当前巡检点")
    private Point currentPoint;

    @ApiModelProperty(name = "passPointNum", value = "已巡检点数")
    private Integer passPointNum;

    @ApiModelProperty(name = "taskStartTime", value = "任务开始时间")
    private Date taskStartTime;

    @ApiModelProperty(name = "totalRunTime", value = "预估总运行时间")
    private Integer totalRunTime;

    @ApiModelProperty(name = "cumulativeRunTime", value = "累计运行时间")
    private Integer cumulativeRunTime;

    @ApiModelProperty(name = "restartStartTime", value = "重置开始时间")
    private Date restartStartTime;

    @ApiModelProperty(name = "pathPlanning", value = "当前路径")
    private PathPlanning pathPlanning;

    @ApiModelProperty(name = "taskStatus", value = "任务状态")
    private Byte taskStatus;

    @ApiModelProperty(name = "currentSysTime", value = "当前系统时间")
    private Long currentSysTime = System.currentTimeMillis();

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public Integer getTaskHistoryId() {
        return taskHistoryId;
    }

    public void setTaskHistoryId(Integer taskHistoryId) {
        this.taskHistoryId = taskHistoryId;
    }

    public Integer getPointTotal() {
        return pointTotal;
    }

    public void setPointTotal(Integer pointTotal) {
        this.pointTotal = pointTotal;
    }

    public Integer getAbnormalPointNum() {
        return abnormalPointNum;
    }

    public void setAbnormalPointNum(Integer abnormalPointNum) {
        this.abnormalPointNum = abnormalPointNum;
    }

    public Point getCurrentPoint() {
        return currentPoint;
    }

    public void setCurrentPoint(Point currentPoint) {
        this.currentPoint = currentPoint;
    }

    public Integer getPassPointNum() {
        return passPointNum;
    }

    public void setPassPointNum(Integer passPointNum) {
        this.passPointNum = passPointNum;
    }

    public Date getTaskStartTime() {
        return taskStartTime;
    }

    public void setTaskStartTime(Date taskStartTime) {
        this.taskStartTime = taskStartTime;
    }

    public Integer getTotalRunTime() {
        return totalRunTime;
    }

    public void setTotalRunTime(Integer totalRunTime) {
        this.totalRunTime = totalRunTime;
    }

    public Integer getCumulativeRunTime() {
        return cumulativeRunTime;
    }

    public void setCumulativeRunTime(Integer cumulativeRunTime) {
        this.cumulativeRunTime = cumulativeRunTime;
    }

    public Date getRestartStartTime() {
        return restartStartTime;
    }

    public void setRestartStartTime(Date restartStartTime) {
        this.restartStartTime = restartStartTime;
    }

    public PathPlanning getPathPlanning() {
        return pathPlanning;
    }

    public void setPathPlanning(PathPlanning pathPlanning) {
        this.pathPlanning = pathPlanning;
    }

    public Byte getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(Byte taskStatus) {
        this.taskStatus = taskStatus;
    }

    public Long getCurrentSysTime() {
        return currentSysTime;
    }

    public void setCurrentSysTime(Long currentSysTime) {
        this.currentSysTime = currentSysTime;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getInspectTypeId() {
        return inspectTypeId;
    }

    public void setInspectTypeId(Integer inspectTypeId) {
        this.inspectTypeId = inspectTypeId;
    }

}
