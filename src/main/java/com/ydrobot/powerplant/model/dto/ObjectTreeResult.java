package com.ydrobot.powerplant.model.dto;

public class ObjectTreeResult {

	/**
	 * 对象id
	 */
	Integer objectId;
	
	/**
	 * 对象名称
	 */
	String objectName;
	
	/**
	 * 树id
	 */
	Integer deviceId;
	
	/**
	 * 对象类型id
	 */
	Integer objectTypeId;
	
	/**
	 * 对象类型名称
	 */
	String objectTypeName;

	public Integer getObjectId() {
		return objectId;
	}

	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public Integer getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(Integer deviceId) {
		this.deviceId = deviceId;
	}

	public Integer getObjectTypeId() {
		return objectTypeId;
	}

	public void setObjectTypeId(Integer objectTypeId) {
		this.objectTypeId = objectTypeId;
	}

	public String getObjectTypeName() {
		return objectTypeName;
	}

	public void setObjectTypeName(String objectTypeName) {
		this.objectTypeName = objectTypeName;
	}

}
