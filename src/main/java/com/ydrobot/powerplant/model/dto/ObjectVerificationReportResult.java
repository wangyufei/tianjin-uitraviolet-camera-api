package com.ydrobot.powerplant.model.dto;

import java.util.Date;

public class ObjectVerificationReportResult {
	
	/**
	 * 对象id
	 */
	private Integer objectId;
	
	/**
	 * 对象名称
	 */
	private String objectName;
	
	/**
	 * 巡检原因
	 */
	private String inspectReason;
	
	/**
	 * 巡检时间
	 */
	private Date inspectTime;
	
	/**
	 * 启动模式
	 */
	private String startMode;
	
	/**
	 * 评价状态
	 */
	private String appraiseStatus;

	public Integer getObjectId() {
		return objectId;
	}

	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public String getInspectReason() {
		return inspectReason;
	}

	public void setInspectReason(String inspectReason) {
		this.inspectReason = inspectReason;
	}

	public Date getInspectTime() {
		return inspectTime;
	}

	public void setInspectTime(Date inspectTime) {
		this.inspectTime = inspectTime;
	}

	public String getStartMode() {
		return startMode;
	}

	public void setStartMode(String startMode) {
		this.startMode = startMode;
	}

	public String getAppraiseStatus() {
		return appraiseStatus;
	}

	public void setAppraiseStatus(String appraiseStatus) {
		this.appraiseStatus = appraiseStatus;
	}
}
