package com.ydrobot.powerplant.model.dto;


public class PieResult {
	
	/**
	 * 名字
	 */
	private String name;
	
	/**
	 * 值
	 */
	private Integer value;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}
	
	
}
