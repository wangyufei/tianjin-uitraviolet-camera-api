package com.ydrobot.powerplant.model.dto;

import com.ydrobot.powerplant.model.PointAppraiseHistory;

public class PointAppraiseHistoryResult extends PointAppraiseHistory{
	
	/**
	 * 部件名称
	 */
	private String pointName;

	public String getPointName() {
		return pointName;
	}

	public void setPointName(String pointName) {
		this.pointName = pointName;
	}
	
}
