package com.ydrobot.powerplant.model.dto;

import com.ydrobot.powerplant.model.Point;
import com.ydrobot.powerplant.model.PointCameraRelation;
import com.ydrobot.powerplant.model.Robot;

public class PointCameraResult extends PointCameraRelation{
	
	/**
	 * 点位
	 */
	private Point point;
	
	/**
	 * 机器人
	 */
	private Robot robot;

	public Point getPoint() {
		return point;
	}

	public void setPoint(Point point) {
		this.point = point;
	}

	public Robot getRobot() {
		return robot;
	}

	public void setRobot(Robot robot) {
		this.robot = robot;
	}
	
	
	
}
