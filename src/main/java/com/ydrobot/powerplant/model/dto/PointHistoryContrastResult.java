package com.ydrobot.powerplant.model.dto;

import java.util.List;
import java.util.Map;

public class PointHistoryContrastResult {

	private Integer pointId;
	private String pointName;
	private String pointUnit;
	private List<Map<String, Object>> pointHistory;
	
	public Integer getPointId() {
		return pointId;
	}

	public void setPointId(Integer pointId) {
		this.pointId = pointId;
	}

	public String getPointName() {
		return pointName;
	}

	public void setPointName(String pointName) {
		this.pointName = pointName;
	}

	public String getPointUnit() {
		return pointUnit;
	}

	public void setPointUnit(String pointUnit) {
		this.pointUnit = pointUnit;
	}

	public List<Map<String, Object>> getPointHistory() {
		return pointHistory;
	}

	public void setPointHistory(List<Map<String, Object>> pointHistory) {
		this.pointHistory = pointHistory;
	}

}
