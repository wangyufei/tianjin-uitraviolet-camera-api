package com.ydrobot.powerplant.model.dto;

public class PointPatrolResult {
	
	/**
	 * 部件记录id
	 */
	private Integer pointHistoryId;
	
	/**
	 * 部件名称
	 */
	private String pointName;
	
	/**
	 * 值
	 */
	private Float value;
	
	/**
	 * 状态 0:正常 1:异常
	 */
	private Integer status;
	
	/**
	 * 单位
	 */
	private String unit;

	public Integer getPointHistoryId() {
		return pointHistoryId;
	}

	public void setPointHistoryId(Integer pointHistoryId) {
		this.pointHistoryId = pointHistoryId;
	}

	public String getPointName() {
		return pointName;
	}

	public void setPointName(String pointName) {
		this.pointName = pointName;
	}

	public Float getValue() {
		return value;
	}

	public void setValue(Float value) {
		this.value = value;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}
}
