package com.ydrobot.powerplant.model.dto;

public class PointTreeInfoResult {
    /**
     * 主键id
     */
    private Integer id;

    /**
     * 点位名称
     */
    private String name;

    /**
     * 识别类型id
     */
    private Integer reconTypeId;

    /**
     * 表计类型id
     */
    private Integer meterTypeId;

    /**
     * 外观类型id
     */
    private Integer faceTypeId;

    /**
     * 是否启用
     */
    private Byte isUse;

    /**
     * 报警等级
     */
    private Byte alarmLevel;

    /**
     * 单位
     */
    private String unit;

    /**
     * 父id
     */
    private Integer parentId;

    /**
     *  点位类型
     */
    private Byte type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getReconTypeId() {
        return reconTypeId;
    }

    public void setReconTypeId(Integer reconTypeId) {
        this.reconTypeId = reconTypeId;
    }

    public Integer getMeterTypeId() {
        return meterTypeId;
    }

    public void setMeterTypeId(Integer meterTypeId) {
        this.meterTypeId = meterTypeId;
    }

    public Integer getFaceTypeId() {
        return faceTypeId;
    }

    public void setFaceTypeId(Integer faceTypeId) {
        this.faceTypeId = faceTypeId;
    }

    public Byte getIsUse() {
        return isUse;
    }

    public void setIsUse(Byte isUse) {
        this.isUse = isUse;
    }

    public Byte getAlarmLevel() {
        return alarmLevel;
    }

    public void setAlarmLevel(Byte alarmLevel) {
        this.alarmLevel = alarmLevel;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

}
