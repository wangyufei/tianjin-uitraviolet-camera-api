package com.ydrobot.powerplant.model.dto;

import java.util.List;

public class PointVerificationReportResult {
	
	/**
	 * 任务记录id
	 */
	private Integer taskHistoryId;
	
	/**
	 * 部件id
	 */
	private Integer pointId;
	
	/**
	 * 部件名称
	 */
	private String pointName;
	
	/**
	 * 核查状态 0:正常 1:异常
	 */
	private Integer verificationStatus;
	
	/**
	 * 对象id
	 */
	private Integer objectId;
	
	/**
	 * 点位记录评价报告结果
	 */
	private List<PointHistoryVerificationReportResult> pointHistoryVerificationReportResults;
	
	
	public Integer getTaskHistoryId() {
		return taskHistoryId;
	}

	public void setTaskHistoryId(Integer taskHistoryId) {
		this.taskHistoryId = taskHistoryId;
	}

	public Integer getPointId() {
		return pointId;
	}

	public void setPointId(Integer pointId) {
		this.pointId = pointId;
	}

	public String getPointName() {
		return pointName;
	}

	public void setPointName(String pointName) {
		this.pointName = pointName;
	}

	public Integer getVerificationStatus() {
		return verificationStatus;
	}

	public void setVerificationStatus(Integer verificationStatus) {
		this.verificationStatus = verificationStatus;
	}

	public List<PointHistoryVerificationReportResult> getPointHistoryVerificationReportResults() {
		return pointHistoryVerificationReportResults;
	}

	public void setPointHistoryVerificationReportResults(
			List<PointHistoryVerificationReportResult> pointHistoryVerificationReportResults) {
		this.pointHistoryVerificationReportResults = pointHistoryVerificationReportResults;
	}

	public Integer getObjectId() {
		return objectId;
	}

	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}
	
}
