package com.ydrobot.powerplant.model.dto;

import java.util.List;

import com.ydrobot.powerplant.model.Robot;
import com.ydrobot.powerplant.model.RobotParam;

public class RobotResult extends Robot{
	
	/**
	 * 机器人参数列表
	 */
	public List<RobotParam> robotParams;

	public List<RobotParam> getRobotParams() {
		return robotParams;
	}

	public void setRobotParams(List<RobotParam> robotParams) {
		this.robotParams = robotParams;
	}
	
}
