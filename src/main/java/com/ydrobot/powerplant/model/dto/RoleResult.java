package com.ydrobot.powerplant.model.dto;

import java.util.List;

import com.ydrobot.powerplant.model.Permission;
import com.ydrobot.powerplant.model.Role;

public class RoleResult extends Role{
	
	/**
	 * 权限列表
	 */
	public List<Permission> permissions;

	public List<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}
	
}
