package com.ydrobot.powerplant.model.dto;

import com.ydrobot.powerplant.model.Route;
import com.ydrobot.powerplant.model.RoutePos;

public class RouteResult extends Route {
	
	/**
	 * 开始道路点
	 */
	public RoutePos startRoutePos;

	/**
	 * 结束道路点
	 */
	public RoutePos endRoutePos;

	public RoutePos getStartRoutePos() {
		return startRoutePos;
	}

	public void setStartRoutePos(RoutePos startRoutePos) {
		this.startRoutePos = startRoutePos;
	}

	public RoutePos getEndRoutePos() {
		return endRoutePos;
	}

	public void setEndRoutePos(RoutePos endRoutePos) {
		this.endRoutePos = endRoutePos;
	}
}
