package com.ydrobot.powerplant.model.dto;

import com.ydrobot.powerplant.model.Robot;
import com.ydrobot.powerplant.model.SysAlarmType;
import com.ydrobot.powerplant.model.SysPoint;
import com.ydrobot.powerplant.model.SysPointAlarmHistory;

public class SysPointAlarmHistoryResult extends SysPointAlarmHistory {

	private SysPoint sysPoint;
	private Robot robot;
	private SysAlarmType alarmType;

	public SysPoint getSysPoint() {
		return sysPoint;
	}

	public void setSysPoint(SysPoint sysPoint) {
		this.sysPoint = sysPoint;
	}

	public Robot getRobot() {
		return robot;
	}

	public void setRobot(Robot robot) {
		this.robot = robot;
	}

	public SysAlarmType getAlarmType() {
		return alarmType;
	}

	public void setAlarmType(SysAlarmType alarmType) {
		this.alarmType = alarmType;
	}

	

}
