package com.ydrobot.powerplant.model.dto;

import com.ydrobot.powerplant.model.Task;
import com.ydrobot.powerplant.model.TaskHistory;

public class TaskHistoryResult extends TaskHistory {

	/**
	 * 任务
	 */
	private Task task;

	/**
	 * 点位审核状态
	 */
	private Integer pointCheckStatus;

	/**
	 * 点总数
	 */
	private Integer pointTotalNum;

	/**
	 * 正常点数
	 */
	private Integer normalPointNum;

	/**
	 * 报警点数
	 */
	private Integer alarmPointNum;

	/**
	 * 异常点数
	 */
	private Integer abnormalPointNum;

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public Integer getPointCheckStatus() {
		return pointCheckStatus;
	}

	public void setPointCheckStatus(Integer pointCheckStatus) {
		this.pointCheckStatus = pointCheckStatus;
	}

	public Integer getPointTotalNum() {
		return pointTotalNum;
	}

	public void setPointTotalNum(Integer pointTotalNum) {
		this.pointTotalNum = pointTotalNum;
	}

	public Integer getNormalPointNum() {
		return normalPointNum;
	}

	public void setNormalPointNum(Integer normalPointNum) {
		this.normalPointNum = normalPointNum;
	}

	public Integer getAlarmPointNum() {
		return alarmPointNum;
	}

	public void setAlarmPointNum(Integer alarmPointNum) {
		this.alarmPointNum = alarmPointNum;
	}

	public Integer getAbnormalPointNum() {
		return abnormalPointNum;
	}

	public void setAbnormalPointNum(Integer abnormalPointNum) {
		this.abnormalPointNum = abnormalPointNum;
	}

}
