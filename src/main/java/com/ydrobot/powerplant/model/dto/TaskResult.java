package com.ydrobot.powerplant.model.dto;

import com.ydrobot.powerplant.model.InspectType;
import com.ydrobot.powerplant.model.Task;

public class TaskResult extends Task{

	/**
	 * 巡检类型
	 */
	private InspectType inspectType;

	public InspectType getInspectType() {
		return inspectType;
	}

	public void setInspectType(InspectType inspectType) {
		this.inspectType = inspectType;
	}
	
	
}
