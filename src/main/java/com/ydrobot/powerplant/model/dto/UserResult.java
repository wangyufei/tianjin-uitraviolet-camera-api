package com.ydrobot.powerplant.model.dto;

import java.util.List;

import com.ydrobot.powerplant.model.Organization;
import com.ydrobot.powerplant.model.Role;
import com.ydrobot.powerplant.model.User;

public class UserResult extends User{

	/**
	 * 组织
	 */
	private Organization organization;
	
	/**
	 * 角色
	 */
	private List<Role> roles;

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	
	
}
