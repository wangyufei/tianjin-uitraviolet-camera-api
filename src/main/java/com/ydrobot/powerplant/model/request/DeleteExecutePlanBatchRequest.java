package com.ydrobot.powerplant.model.request;

import java.util.List;

public class DeleteExecutePlanBatchRequest {

    List<DeleteExecutePlanRequest> deleteExecutePlanRequests;

    public List<DeleteExecutePlanRequest> getDeleteExecutePlanRequests() {
        return deleteExecutePlanRequests;
    }

    public void setDeleteExecutePlanRequests(List<DeleteExecutePlanRequest> deleteExecutePlanRequests) {
        this.deleteExecutePlanRequests = deleteExecutePlanRequests;
    }

}
