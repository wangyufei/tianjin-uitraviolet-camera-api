package com.ydrobot.powerplant.model.request;

import io.swagger.annotations.ApiModelProperty;

public class InspectTypeChooseRequest {

    @ApiModelProperty(name = "choose_id", value = "筛选条件id")
    private Integer chooseId;

    @ApiModelProperty(name = "choose_type", value = "筛选类型 0:设备区域、1:设备类型、2:识别类型，3:表计类型，4:设备外观类型")
    private Byte chooseType;

    public Integer getChooseId() {
        return chooseId;
    }

    public void setChooseId(Integer chooseId) {
        this.chooseId = chooseId;
    }

    public Byte getChooseType() {
        return chooseType;
    }

    public void setChooseType(Byte chooseType) {
        this.chooseType = chooseType;
    }

}
