package com.ydrobot.powerplant.model.request;

import io.swagger.annotations.ApiModelProperty;

public class MeterPointInfoRequest {
    @ApiModelProperty(name = "id", value = "主键id，更新数据时需要传递")
    private Integer id;

    @ApiModelProperty(name = "rangeMin", value = "量程下限")
    private Float rangeMin;

    @ApiModelProperty(name = "rangeMax", value = "量程上限")
    private Float rangeMax;

    @ApiModelProperty(name = "angleMin", value = "起始角度")
    private Float angleMin;

    @ApiModelProperty(name = "angleMax", value = "截止角度")
    private Float angleMax;

    @ApiModelProperty(name = "offsetValue", value = "偏移值")
    private Integer offsetValue;

    @ApiModelProperty(name = "direction", value = "方向")
    private Integer direction;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Float getRangeMin() {
        return rangeMin;
    }

    public void setRangeMin(Float rangeMin) {
        this.rangeMin = rangeMin;
    }

    public Float getRangeMax() {
        return rangeMax;
    }

    public void setRangeMax(Float rangeMax) {
        this.rangeMax = rangeMax;
    }

    public Float getAngleMin() {
        return angleMin;
    }

    public void setAngleMin(Float angleMin) {
        this.angleMin = angleMin;
    }

    public Float getAngleMax() {
        return angleMax;
    }

    public void setAngleMax(Float angleMax) {
        this.angleMax = angleMax;
    }

    public Integer getOffsetValue() {
        return offsetValue;
    }

    public void setOffsetValue(Integer offsetValue) {
        this.offsetValue = offsetValue;
    }

    public Integer getDirection() {
        return direction;
    }

    public void setDirection(Integer direction) {
        this.direction = direction;
    }

}
