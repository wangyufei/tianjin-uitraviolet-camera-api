package com.ydrobot.powerplant.model.request;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import io.swagger.annotations.ApiModelProperty;

public class OperationLogRequest {

    @ApiModelProperty(name = "type", value = "日志类型 0:系统 1:机器人 2:客户端")
    @Range(min = 0, max = 2, message = "日志类型不正确")
    private Byte type;

    @ApiModelProperty(name = "content", value = "日志内容")
    @NotEmpty(message = "日志内容不能为空")
    private String content;

    @ApiModelProperty(name = "deviceId", value = "变电站id")
    private Integer deviceId;

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

}
