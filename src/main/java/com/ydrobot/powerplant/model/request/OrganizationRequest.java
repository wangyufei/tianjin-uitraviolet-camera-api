package com.ydrobot.powerplant.model.request;

import org.hibernate.validator.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;

public class OrganizationRequest {

    @ApiModelProperty(name = "sn", value = "编号")
    private String sn;

    @ApiModelProperty(name = "name", value = "组织名称")
    @NotEmpty(message = "组织名称不能为空")
    private String name;

    @ApiModelProperty(name = "parentId", value = "父级id")
    private Integer parentId;

    @ApiModelProperty(name = "cnName", value = "中文名称")
    private String cnName;

    @ApiModelProperty(name = "enName", value = "英文名称")
    private String enName;

    @ApiModelProperty(name = "description", value = "组织描述")
    private String description;

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getCnName() {
        return cnName;
    }

    public void setCnName(String cnName) {
        this.cnName = cnName;
    }

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
