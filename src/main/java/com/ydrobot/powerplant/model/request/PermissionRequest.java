package com.ydrobot.powerplant.model.request;

import io.swagger.annotations.ApiModelProperty;

public class PermissionRequest {

    @ApiModelProperty(name = "name", value = "权限名称")
    private String name;

    @ApiModelProperty(name = "displayName", value = "显示名称")
    private String displayName;

    @ApiModelProperty(name = "routeAddress", value = "路由地址")
    private String routeAddress;

    @ApiModelProperty(name = "parentId", value = "父级id")
    private Integer parentId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getRouteAddress() {
        return routeAddress;
    }

    public void setRouteAddress(String routeAddress) {
        this.routeAddress = routeAddress;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

}
