package com.ydrobot.powerplant.model.request;

import org.hibernate.validator.constraints.Range;

import io.swagger.annotations.ApiModelProperty;

public class PointAlarmHistoryRequest {

    @ApiModelProperty(name = "alarmTypeId", value = "告警类型id")
    private Integer alarmTypeId;

    @ApiModelProperty(name = "alarmLevel", value = "0:正常 1:预警 2:一般告警 3:严重告警 4:危机告警")
    @Range(min = 0, max = 4, message = "报警类型不正确")
    private Byte alarmLevel;

    @ApiModelProperty(name = "resultStatus", value = "结果状态 0:正常 1:异常")
    @Range(min = 0, max = 1, message = "结果状态不正确")
    private Byte resultStatus;

    @ApiModelProperty(name = "modifyValue", value = "人工判断结果值")
    private Float modifyValue;

    @ApiModelProperty(name = "reconStatus", value = "识别状态 0:正常 1:错误")
    @Range(min = 0, max = 1, message = "识别状态不正确")
    private Byte reconStatus;

    @ApiModelProperty(name = "checkDesc", value = "审核意见")
    private String checkDesc;
    
    @ApiModelProperty(name = "appraiseStatus", value = "评价状态 0:正常 1:存在缺陷 2:设备出现异常趋势 3:需人工审核")
    @Range(min = 0, max = 3, message = "评价状态不正确")
    private Integer appraiseStatus;

    public Integer getAlarmTypeId() {
        return alarmTypeId;
    }

    public void setAlarmTypeId(Integer alarmTypeId) {
        this.alarmTypeId = alarmTypeId;
    }

    public Byte getAlarmLevel() {
        return alarmLevel;
    }

    public void setAlarmLevel(Byte alarmLevel) {
        this.alarmLevel = alarmLevel;
    }

    public Byte getResultStatus() {
        return resultStatus;
    }

    public void setResultStatus(Byte resultStatus) {
        this.resultStatus = resultStatus;
    }

    public Float getModifyValue() {
        return modifyValue;
    }

    public void setModifyValue(Float modifyValue) {
        this.modifyValue = modifyValue;
    }

    public Byte getReconStatus() {
        return reconStatus;
    }

    public void setReconStatus(Byte reconStatus) {
        this.reconStatus = reconStatus;
    }

    public String getCheckDesc() {
        return checkDesc;
    }

    public void setCheckDesc(String checkDesc) {
        this.checkDesc = checkDesc;
    }

	public Integer getAppraiseStatus() {
		return appraiseStatus;
	}

	public void setAppraiseStatus(Integer appraiseStatus) {
		this.appraiseStatus = appraiseStatus;
	}
}
