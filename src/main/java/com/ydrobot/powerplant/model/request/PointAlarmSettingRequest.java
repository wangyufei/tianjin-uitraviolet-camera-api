package com.ydrobot.powerplant.model.request;

import io.swagger.annotations.ApiModelProperty;

public class PointAlarmSettingRequest {

    @ApiModelProperty(name = "alarmTypeId", value = "告警类型id")
    private Integer alarmTypeId;

    @ApiModelProperty(name = "alarmName", value = "告警名称")
    private String alarmName;

    @ApiModelProperty(name = "alarmLevel", value = "告警等级")
    private Byte alarmLevel;

    @ApiModelProperty(name = "limitValue", value = "限制")
    private Float limitValue;

    @ApiModelProperty(name = "valueType", value = "值类型")
    private Byte valueType;

    @ApiModelProperty(name = "upOrDown", value = "0:下限 1:上限")
    private Byte upOrDown;

    @ApiModelProperty(name = "isEffective", value = "是否有效 0:无效 1:有效")
    private Byte isEffective;

    @ApiModelProperty(name = "isOpenEnvironmentTemp", value = "是否启用环境温度 0:不启用 1:启用")
    private Byte isOpenEnvironmentTemp;

    @ApiModelProperty(name = "referenceTemp", value = "参考温度")
    private Float referenceTemp;

    public Integer getAlarmTypeId() {
        return alarmTypeId;
    }

    public void setAlarmTypeId(Integer alarmTypeId) {
        this.alarmTypeId = alarmTypeId;
    }

    public String getAlarmName() {
        return alarmName;
    }

    public void setAlarmName(String alarmName) {
        this.alarmName = alarmName;
    }

    public Byte getAlarmLevel() {
        return alarmLevel;
    }

    public void setAlarmLevel(Byte alarmLevel) {
        this.alarmLevel = alarmLevel;
    }

    public Float getLimitValue() {
        return limitValue;
    }

    public void setLimitValue(Float limitValue) {
        this.limitValue = limitValue;
    }

    public Byte getValueType() {
        return valueType;
    }

    public void setValueType(Byte valueType) {
        this.valueType = valueType;
    }

    public Byte getUpOrDown() {
        return upOrDown;
    }

    public void setUpOrDown(Byte upOrDown) {
        this.upOrDown = upOrDown;
    }

    public Byte getIsEffective() {
        return isEffective;
    }

    public void setIsEffective(Byte isEffective) {
        this.isEffective = isEffective;
    }

    public Byte getIsOpenEnvironmentTemp() {
        return isOpenEnvironmentTemp;
    }

    public void setIsOpenEnvironmentTemp(Byte isOpenEnvironmentTemp) {
        this.isOpenEnvironmentTemp = isOpenEnvironmentTemp;
    }

    public Float getReferenceTemp() {
        return referenceTemp;
    }

    public void setReferenceTemp(Float referenceTemp) {
        this.referenceTemp = referenceTemp;
    }

}
