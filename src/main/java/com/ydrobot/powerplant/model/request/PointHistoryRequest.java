package com.ydrobot.powerplant.model.request;

import org.hibernate.validator.constraints.Range;

import io.swagger.annotations.ApiModelProperty;

public class PointHistoryRequest {

    @ApiModelProperty(name = "reconStatus", value = "识别状态 0:正常 1:错误")
    private byte reconStatus;

    @ApiModelProperty(name = "modifyValue", value = "人工判断结果值")
    private Float modifyValue;

    @ApiModelProperty(name = "appraiseStatus", value = "评价状态 0:正常 1:存在缺陷 2:设备出现异常趋势 3:需人工审核")
    @Range(min = 0, max = 3, message = "评价状态不正确")
    private Integer appraiseStatus;

    public byte getReconStatus() {
        return reconStatus;
    }

    public void setReconStatus(byte reconStatus) {
        this.reconStatus = reconStatus;
    }

    public Float getModifyValue() {
        return modifyValue;
    }

    public void setModifyValue(Float modifyValue) {
        this.modifyValue = modifyValue;
    }

	public Integer getAppraiseStatus() {
		return appraiseStatus;
	}

	public void setAppraiseStatus(Integer appraiseStatus) {
		this.appraiseStatus = appraiseStatus;
	}
}
