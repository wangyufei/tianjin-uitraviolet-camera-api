package com.ydrobot.powerplant.model.request;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class PointRelationSettingBatchRequest {

    @ApiModelProperty(name = "pointRelationSettingRequests", value = "点位关系集合")
    private List<PointRelationSettingRequest> pointRelationSettingRequests;

    public List<PointRelationSettingRequest> getPointRelationSettingRequests() {
        return pointRelationSettingRequests;
    }

    public void setPointRelationSettingRequests(List<PointRelationSettingRequest> pointRelationSettingRequests) {
        this.pointRelationSettingRequests = pointRelationSettingRequests;
    }

}
