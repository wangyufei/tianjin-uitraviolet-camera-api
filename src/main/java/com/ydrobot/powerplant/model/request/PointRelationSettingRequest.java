package com.ydrobot.powerplant.model.request;

import io.swagger.annotations.ApiModelProperty;

public class PointRelationSettingRequest {

    @ApiModelProperty(name = "pointId", value = "点位id")
    private Integer pointId;

    @ApiModelProperty(name = "type", value = "类型 0:三相温差 1:三相对比")
    private Byte type;

    @ApiModelProperty(name = "relationPointIds", value = "关联点位id,多个用逗号分隔")
    private String relationPointIds;

    @ApiModelProperty(name = "dependPoint", value = "依赖点")
    private String dependPoint;

    public Integer getPointId() {
        return pointId;
    }

    public void setPointId(Integer pointId) {
        this.pointId = pointId;
    }

    public String getRelationPointIds() {
        return relationPointIds;
    }

    public void setRelationPointIds(String relationPointIds) {
        this.relationPointIds = relationPointIds;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public String getDependPoint() {
        return dependPoint;
    }

    public void setDependPoint(String dependPoint) {
        this.dependPoint = dependPoint;
    }

}
