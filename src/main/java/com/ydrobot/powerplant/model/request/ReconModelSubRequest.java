package com.ydrobot.powerplant.model.request;

import io.swagger.annotations.ApiModelProperty;

public class ReconModelSubRequest {

    @ApiModelProperty(name = "reconModelId", value = "识别模型id")
    private Integer reconModelId;

    @ApiModelProperty(name = "name", value = "名称KEY")
    private String name;

    @ApiModelProperty(name = "display_name", value = "显示名称")
    private String displayName;

    @ApiModelProperty(name = "description", value = "描述")
    private String description;

    public Integer getReconModelId() {
        return reconModelId;
    }

    public void setReconModelId(Integer reconModelId) {
        this.reconModelId = reconModelId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
