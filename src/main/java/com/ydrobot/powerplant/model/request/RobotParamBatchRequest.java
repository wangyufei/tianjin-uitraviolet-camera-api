package com.ydrobot.powerplant.model.request;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class RobotParamBatchRequest {

    @ApiModelProperty(name = "robotId", value = "机器人id")
    private Integer robotId;

    @ApiModelProperty(name = "type", value = "参数类型 0:机器人通讯中断及告警设置 1:云台控制初始化设置 2:机器人控制初始化设置")
    private Byte type;

    @ApiModelProperty(name = "robotParamRequests", value = "机器人id")
    private List<RobotParamRequest> robotParamRequests;

    public Integer getRobotId() {
        return robotId;
    }

    public void setRobotId(Integer robotId) {
        this.robotId = robotId;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public List<RobotParamRequest> getRobotParamRequests() {
        return robotParamRequests;
    }

    public void setRobotParamRequests(List<RobotParamRequest> robotParamRequests) {
        this.robotParamRequests = robotParamRequests;
    }

}
