package com.ydrobot.powerplant.model.request;

import io.swagger.annotations.ApiModelProperty;

public class RobotParamRequest {
    @ApiModelProperty(name = "name", value = "参数名称")
    private String name;

    @ApiModelProperty(name = "display_name", value = "参数显示名称")
    private String displayName;

    @ApiModelProperty(name = "value", value = "参数值")
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
