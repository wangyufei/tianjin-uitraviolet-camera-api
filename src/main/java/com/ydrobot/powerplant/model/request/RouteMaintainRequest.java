package com.ydrobot.powerplant.model.request;

import io.swagger.annotations.ApiModelProperty;

public class RouteMaintainRequest {

	@ApiModelProperty(name = "routeId", value = "道路id")
	private Integer routeId;
	@ApiModelProperty(name = "standard", value = "道路状态")
	private String standard;
	@ApiModelProperty(name = "start", value = "x,y,z")
	private String start;
	@ApiModelProperty(name = "start", value = "x,y,z")
	private String end;
	@ApiModelProperty(name = "lineWt", value = "路宽")
	private Float lineWt;

	public Integer getRouteId() {
		return routeId;
	}

	public void setRouteId(Integer routeId) {
		this.routeId = routeId;
	}

	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public Float getLineWt() {
		return lineWt;
	}

	public void setLineWt(Float lineWt) {
		this.lineWt = lineWt;
	}

}
