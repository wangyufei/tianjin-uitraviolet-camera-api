package com.ydrobot.powerplant.model.request;

import io.swagger.annotations.ApiModelProperty;

public class SysPointAlarmHistoryRequest {

    @ApiModelProperty(name = "alarmTypeId", value = "告警类型id")
    private Integer alarmTypeId;

    @ApiModelProperty(name = "alarmLevel", value = "0:正常 1:预警 2:一般告警 3:严重告警 4:危机告警")
    private Byte alarmLevel;

    @ApiModelProperty(name = "resultStatus", value = "结果状态 0:正常 1:异常")
    private Byte resultStatus;

    @ApiModelProperty(name = "modifyValue", value = "人工判断结果值")
    private Float modifyValue;

    @ApiModelProperty(name = "errorFlag", value = "异常标识 0:正常 1:异常")
    private Byte errorFlag;

    @ApiModelProperty(name = "reconStatus", value = "识别状态 0:正常 1:异常")
    private Byte reconStatus;

    @ApiModelProperty(name = "checkDesc", value = "审核意见")
    private String checkDesc;

    public Integer getAlarmTypeId() {
        return alarmTypeId;
    }

    public void setAlarmTypeId(Integer alarmTypeId) {
        this.alarmTypeId = alarmTypeId;
    }

    public Byte getAlarmLevel() {
        return alarmLevel;
    }

    public void setAlarmLevel(Byte alarmLevel) {
        this.alarmLevel = alarmLevel;
    }

    public Byte getResultStatus() {
        return resultStatus;
    }

    public void setResultStatus(Byte resultStatus) {
        this.resultStatus = resultStatus;
    }

    public Float getModifyValue() {
        return modifyValue;
    }

    public void setModifyValue(Float modifyValue) {
        this.modifyValue = modifyValue;
    }

    public Byte getErrorFlag() {
        return errorFlag;
    }

    public void setErrorFlag(Byte errorFlag) {
        this.errorFlag = errorFlag;
    }

    public String getCheckDesc() {
        return checkDesc;
    }

    public void setCheckDesc(String checkDesc) {
        this.checkDesc = checkDesc;
    }

    public Byte getReconStatus() {
        return reconStatus;
    }

    public void setReconStatus(Byte reconStatus) {
        this.reconStatus = reconStatus;
    }

}
