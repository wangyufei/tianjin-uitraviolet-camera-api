package com.ydrobot.powerplant.model.request;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class SystemSettingBatchRequest {

    @ApiModelProperty(name = "SystemSettingRequest", value = "系统设置请求集合")
    private List<SystemSettingRequest> systemSettingRequests;

    public List<SystemSettingRequest> getSystemSettingRequests() {
        return systemSettingRequests;
    }

    public void setSystemSettingRequests(List<SystemSettingRequest> systemSettingRequests) {
        this.systemSettingRequests = systemSettingRequests;
    }

}
