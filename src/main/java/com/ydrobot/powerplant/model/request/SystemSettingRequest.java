package com.ydrobot.powerplant.model.request;

import io.swagger.annotations.ApiModelProperty;

public class SystemSettingRequest {

    @ApiModelProperty(name = "name", value = "key")
    private String name;

    @ApiModelProperty(name = "displayName", value = "显示名称")
    private String displayName;

    @ApiModelProperty(name = "value", value = "值")
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
