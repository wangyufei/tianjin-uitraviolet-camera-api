package com.ydrobot.powerplant.model.request;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

public class TaskCycleRequest {

    @ApiModelProperty(name = "robotId", value = "机器人id")
    private Integer robotId;

    @ApiModelProperty(name = "taskId", value = "任务id")
    private Integer taskId;

    @ApiModelProperty(name = "executeType", value = "执行类型 0:周期执行 1:间隔执行")
    private Byte executeType;

    @ApiModelProperty(name = "month", value = "月，多个以逗号分隔")
    private String month;

    @ApiModelProperty(name = "week", value = "周，多个以逗号分隔")
    private String week;

    @ApiModelProperty(name = "executeTime", value = "周期执行时间")
    private String executeTime;

    @ApiModelProperty(name = "startTime", value = "开始时间")
    private Date startTime;

    @ApiModelProperty(name = "endTime", value = "结束时间")
    private Date endTime;

    @ApiModelProperty(name = "intervalTime", value = "间隔时间")
    private Integer intervalTime;

    @ApiModelProperty(name = "intervalUnit", value = "间隔单位 0:分钟 1:小时 2:天")
    private Byte intervalUnit;

    @ApiModelProperty(name = "name", value = "任务周期名称")
    private String name;

    public Integer getRobotId() {
        return robotId;
    }

    public void setRobotId(Integer robotId) {
        this.robotId = robotId;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public Byte getExecuteType() {
        return executeType;
    }

    public void setExecuteType(Byte executeType) {
        this.executeType = executeType;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getWeek() {
        return week;
    }

    public void setWeek(String week) {
        this.week = week;
    }

    public String getExecuteTime() {
        return executeTime;
    }

    public void setExecuteTime(String executeTime) {
        this.executeTime = executeTime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getIntervalTime() {
        return intervalTime;
    }

    public void setIntervalTime(Integer intervalTime) {
        this.intervalTime = intervalTime;
    }

    public Byte getIntervalUnit() {
        return intervalUnit;
    }

    public void setIntervalUnit(Byte intervalUnit) {
        this.intervalUnit = intervalUnit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
