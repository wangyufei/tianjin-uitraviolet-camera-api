package com.ydrobot.powerplant.model.request;

import io.swagger.annotations.ApiModelProperty;

public class TaskRequest {

    @ApiModelProperty(name = "robotId", value = "机器人id")
    private Integer robotId;

    @ApiModelProperty(name = "name", value = "任务名称")
    private String name;

    @ApiModelProperty(name = "inspectTypeId", value = "巡检类型id")
    private Integer inspectTypeId;

    @ApiModelProperty(name = "description", value = "任务描述")
    private String description;

    @ApiModelProperty(name = "isCustom", value = "是否自定义 0:正常 1:自定义")
    private Byte isCustom;

    @ApiModelProperty(name = "points", value = "点id集合，多个用逗号分隔")
    private String points;

    public Integer getRobotId() {
        return robotId;
    }

    public void setRobotId(Integer robotId) {
        this.robotId = robotId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getInspectTypeId() {
        return inspectTypeId;
    }

    public void setInspectTypeId(Integer inspectTypeId) {
        this.inspectTypeId = inspectTypeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Byte getIsCustom() {
        return isCustom;
    }

    public void setIsCustom(Byte isCustom) {
        this.isCustom = isCustom;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

}
