package com.ydrobot.powerplant.model.request;

import io.swagger.annotations.ApiModelProperty;

public class UpdatePointAppraiseStatusRequest {

    @ApiModelProperty(name = "status", value = "处理状态 0:正在处理 1:计划处理 2:处理完成")
    
    private Integer status;

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

    
}
