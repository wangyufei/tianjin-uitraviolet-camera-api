package com.ydrobot.powerplant.model.request;

import io.swagger.annotations.ApiModelProperty;

public class UpdateSysPointAlarmHistoryBatchRequest {

    @ApiModelProperty(name = "ids", value = "id集合，多个用逗号分隔")
    private String ids;

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

}
