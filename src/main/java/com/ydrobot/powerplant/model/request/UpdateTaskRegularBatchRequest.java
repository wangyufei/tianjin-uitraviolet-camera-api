package com.ydrobot.powerplant.model.request;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class UpdateTaskRegularBatchRequest {

    @ApiModelProperty(name = "robotId", value = "机器人id")
    private Integer robotId;

    @ApiModelProperty(name = "taskId", value = "任务id")
    private Integer taskId;

    private List<TaskRegularRequest> taskRegularRequests;

    public Integer getRobotId() {
        return robotId;
    }

    public void setRobotId(Integer robotId) {
        this.robotId = robotId;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public List<TaskRegularRequest> getTaskRegularRequests() {
        return taskRegularRequests;
    }

    public void setTaskRegularRequests(List<TaskRegularRequest> taskRegularRequests) {
        this.taskRegularRequests = taskRegularRequests;
    }

}
