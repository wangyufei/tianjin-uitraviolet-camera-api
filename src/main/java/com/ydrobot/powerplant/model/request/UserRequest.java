package com.ydrobot.powerplant.model.request;

import java.util.Date;

import org.hibernate.validator.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;

public class UserRequest {

    @ApiModelProperty(name = "sn", value = "工号")
    private String sn;

    @ApiModelProperty(name = "name", value = "名字")
    private String name;

    @ApiModelProperty(name = "account", value = "账号", required = true)
    @NotEmpty(message = "账号不能为空")
    private String account;

    @ApiModelProperty(name = "password", value = "密码", required = true)
    @NotEmpty(message = "密码不能为空")
    private String password;

    @ApiModelProperty(name = "mobile", value = "电话")
    private String mobile;

    @ApiModelProperty(name = "email", value = "邮箱")
    private String email;

    @ApiModelProperty(name = "organizationId", value = "所属组织id")
    private Integer organizationId;

    @ApiModelProperty(name = "isOpenSms", value = "是否打开短信推送: 0:关闭 1:开启")
    private Integer isOpenSms;

    @ApiModelProperty(name = "validityTime", value = "有效期")
    private Date validityTime;

    @ApiModelProperty(name = "roleIds", value = "角色id集合，多个逗号分隔")
    private String roleIds;

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Integer organizationId) {
        this.organizationId = organizationId;
    }

    public Integer getIsOpenSms() {
        return isOpenSms;
    }

    public void setIsOpenSms(Integer isOpenSms) {
        this.isOpenSms = isOpenSms;
    }

    public Date getValidityTime() {
        return validityTime;
    }

    public void setValidityTime(Date validityTime) {
        this.validityTime = validityTime;
    }

    public String getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(String roleIds) {
        this.roleIds = roleIds;
    }

}
