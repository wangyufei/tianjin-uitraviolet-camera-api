package com.ydrobot.powerplant.model.status;

public enum AlarmLevelStatus {

    /**
     * 正常
     */
    NORMAL(0),

    /**
     * 预警
     */
    WARNING(1),

    /**
     * 一般缺陷
     */
    GENERAL(2),

    /**
     * 严重缺陷
     */
    SERIOUS(3),

    /**
     * 危机缺陷
     */
    CRISIS(4),;

    private int value;

    public int getValue() {
        return value;
    }

    AlarmLevelStatus(int value) {
        this.value = value;
    }
}
