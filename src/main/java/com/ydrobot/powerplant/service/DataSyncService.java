package com.ydrobot.powerplant.service;

import com.ydrobot.powerplant.model.ObjectAlarmHistory;
import com.ydrobot.powerplant.model.PointAlarmHistory;
import com.ydrobot.powerplant.model.PointAppraise;
import com.ydrobot.powerplant.model.PointAppraiseHistory;
import com.ydrobot.powerplant.model.PointHistory;
import com.ydrobot.powerplant.model.SysPointAlarmHistory;
import com.ydrobot.powerplant.model.Task;
import com.ydrobot.powerplant.model.TaskHistory;
import com.ydrobot.powerplant.model.datasync.AlarmHistorySync;

/**
 * Created by Wyf on 2019/04/12.
 */
public interface DataSyncService {

	/**
	 * 点位报警历史记录同步
	 * @param pointAlarmHistorySync
	 */
	public String pointAlarmHistorySync(PointAlarmHistory pointAlarmHistory);
	
	/**
	 * 系统点位报警历史记录同步
	 * @param pointAlarmHistorySync
	 */
	public String sysPointAlarmHistorySync(SysPointAlarmHistory sysPointAlarmHistory);
	
	/**
	 * 任务执行记录同步
	 * @param taskHistory
	 */
	public String taskHistorySync(TaskHistory taskHistory);
	
	/**
	 * 任务同步
	 * @param task
	 */
	public String TaskSync(Task task);
	
	/**
	 * 设备报警记录同步
	 * @param objectAlarmHistory
	 */
	public String objectAlarmHistorySync(ObjectAlarmHistory objectAlarmHistory);
	
	/**
	 * 二次确认告警记录同步
	 * @param alarmHistorySync
	 */
	public void twiceConfirmAlarmHistorySync(AlarmHistorySync alarmHistorySync);

	/**
	 * 设备评价状态同步
	 * @param pointAppraise
	 */
	public String pointAppraiseSync(PointAppraise pointAppraise);
	
	/**
	 * 点位评价状态历史记录同步
	 * @param pointAppraiseHistory
	 */
	public String pointAppraiseHistorySync(PointAppraiseHistory pointAppraiseHistory);
	
	/**
	 * 部件巡检记录同步
	 * @param pointHistory
	 */
	public String pointHistorySync(PointHistory pointHistory);
	
	/**
	 * 部件同步
	 */
	public void pointSync();
	
	/**
	 * 对象同步
	 */
	public void objectSync();
}
