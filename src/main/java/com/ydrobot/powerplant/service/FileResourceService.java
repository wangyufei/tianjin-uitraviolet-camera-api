package com.ydrobot.powerplant.service;
import com.ydrobot.powerplant.model.FileResource;
import com.ydrobot.powerplant.model.condition.FileResourceQueryParam;

import java.util.List;

import com.ydrobot.powerplant.core.service.Service;


/**
 * Created by Wyf on 2020/05/13.
 */
public interface FileResourceService extends Service<FileResource> {
	
	/**
	 * 获取文件资源列表
	 * @param queryParam
	 * @param page
	 * @param size
	 * @return
	 */
	public List<FileResource> listFileResource(FileResourceQueryParam queryParam,Integer page,Integer size);
}
