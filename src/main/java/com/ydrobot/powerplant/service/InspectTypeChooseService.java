package com.ydrobot.powerplant.service;

import java.util.List;

import com.ydrobot.powerplant.core.service.Service;
import com.ydrobot.powerplant.model.InspectTypeChoose;
import com.ydrobot.powerplant.model.request.InspectTypeChooseBatchRequest;

/**
 * Created by Wyf on 2019/04/12.
 */
public interface InspectTypeChooseService extends Service<InspectTypeChoose> {

    /**
     * 获取某个巡检类型的默认项
     * @param inspectId
     * @return
     */
    public List<InspectTypeChoose> listInspectTypeChoose(Integer inspectId);

    /**
     * 批量更新
     * @param inspectTypeChooseBatchRequest
     */
    public void batchUpdate(InspectTypeChooseBatchRequest inspectTypeChooseBatchRequest);
}
