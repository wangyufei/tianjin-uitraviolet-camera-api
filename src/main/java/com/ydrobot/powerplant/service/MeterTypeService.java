package com.ydrobot.powerplant.service;
import com.ydrobot.powerplant.model.MeterType;
import com.ydrobot.powerplant.core.service.Service;


/**
 * Created by Wyf on 2019/04/12.
 */
public interface MeterTypeService extends Service<MeterType> {

}
