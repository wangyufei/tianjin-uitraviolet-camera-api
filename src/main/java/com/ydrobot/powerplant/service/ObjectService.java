package com.ydrobot.powerplant.service;

import java.util.List;

import com.ydrobot.powerplant.core.service.Service;
import com.ydrobot.powerplant.model.PointHistory;
import com.ydrobot.powerplant.model.TObject;
import com.ydrobot.powerplant.model.dto.ObjectPatrolInfoResult;
import com.ydrobot.powerplant.model.dto.ObjectResult;
import com.ydrobot.powerplant.model.dto.PieResult;

/**
 * Created by Wyf on 2019/04/12.
 */
public interface ObjectService extends Service<TObject> {

	/**
	 * 通过设备id获取对象列表
	 * 
	 * @param deviceId
	 * @return
	 */
	public List<TObject> listObjectByDeviceId(Integer deviceId);
	
	/**
	 * 获取对象详情
	 * @param objectId
	 * @return
	 */
	public ObjectResult getDetail(Integer objectId);
	
	/**
	 * 获取对象最新的巡检信息
	 * @param objectId
	 * @return
	 */
	public ObjectPatrolInfoResult getObjectPatrolInfo(Integer objectId);
	
	/**
	 * 获取异常评价对象列表
	 * @return
	 */
	public List<TObject> listAbnormalAppraiseObject();
	
	/**
	 * 获取最近一次任务的报警对象列表
	 * @return
	 */
	public List<TObject> listAlarmObject();
	
	/**
	 * 通过对象id获取报警位置
	 * @param objectId
	 * @return
	 */
	public List<PointHistory> listObjectAlarmPosition(Integer objectId);
	
	/**
	 * 通过对象id获取运行状态
	 * @param objectId
	 * @return
	 */
	public List<PieResult> countObjectRunStatus(Integer objectId);
}
