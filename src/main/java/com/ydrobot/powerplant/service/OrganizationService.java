package com.ydrobot.powerplant.service;

import java.util.List;

import com.ydrobot.powerplant.core.service.Service;
import com.ydrobot.powerplant.model.Organization;
import com.ydrobot.powerplant.model.request.OrganizationRequest;

/**
 * Created by Wyf on 2019/04/12.
 */
public interface OrganizationService extends Service<Organization> {

    /**
     * 获取组织结构树
     * @return
     */
    public List<Organization> listOrganizationTree();

    /**
     * 保存组织结构
     * @param organizationRequest
     */
    public void saveOrganization(OrganizationRequest organizationRequest);

    /**
     * 更新组织结构
     * @param organization
     * @param organizationRequest
     */
    public void updateOrganization(Organization organization, OrganizationRequest organizationRequest);

    /**
     * 删除组织
     * @param id
     */
    public void deleteOrganization(Integer id);
    
    /**
     * 通过组织id获取下级id集合
     * @param id
     * @return
     */
    public List<Integer> listSubIdsById(Integer id);
}
