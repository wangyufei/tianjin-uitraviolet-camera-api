package com.ydrobot.powerplant.service;
import com.ydrobot.powerplant.model.PathPlanning;
import com.ydrobot.powerplant.core.service.Service;


/**
 * Created by Wyf on 2019/05/17.
 */
public interface PathPlanningService extends Service<PathPlanning> {

}
