package com.ydrobot.powerplant.service;

import java.util.List;

import com.ydrobot.powerplant.core.service.Service;
import com.ydrobot.powerplant.model.Permission;
import com.ydrobot.powerplant.model.condition.PermissionQueryParam;
import com.ydrobot.powerplant.model.request.PermissionRequest;

/**
 * Created by Wyf on 2019/04/12.
 */
public interface PermissionService extends Service<Permission> {
    
    /**
     * 获取权限列表
     * @param permissionCondition
     * @param page
     * @param size
     * @return
     */
    public List<Permission> listPermission(PermissionQueryParam queryParam,Integer page,Integer size);

    /**
     * 获取权限树
     * @return
     */
    public List<Permission> listPermissionTree();

    /**
     * 添加权限
     * @param permissionRequest
     */
    public void savePermission(PermissionRequest permissionRequest);

    /**
     * 更新权限
     * @param permission
     * @param permissionRequest
     */
    public void updatePermission(Permission permission, PermissionRequest permissionRequest);
}
