package com.ydrobot.powerplant.service;
import com.ydrobot.powerplant.model.PmsDevice;
import com.ydrobot.powerplant.core.service.Service;


/**
 * Created by Wyf on 2020/07/06.
 */
public interface PmsDeviceService extends Service<PmsDevice> {

}
