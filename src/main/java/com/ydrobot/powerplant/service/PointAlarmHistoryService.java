package com.ydrobot.powerplant.service;

import java.util.List;
import com.ydrobot.powerplant.core.service.Service;
import com.ydrobot.powerplant.model.PointAlarmHistory;
import com.ydrobot.powerplant.model.Task;
import com.ydrobot.powerplant.model.condition.PointAlarmHistoryQueryParam;
import com.ydrobot.powerplant.model.dto.PointAlarmHistoryResult;
import com.ydrobot.powerplant.model.request.PointAlarmHistoryBatchRequest;
import com.ydrobot.powerplant.model.request.PointAlarmHistoryRequest;

/**
 * Created by Wyf on 2019/04/12.
 */
public interface PointAlarmHistoryService extends Service<PointAlarmHistory> {
    
    /**
     * 获取点位报警历史列表
     * @param pointAlarmHistoryCondition
     * @return
     */
    public List<PointAlarmHistoryResult> listPointAlarmHistory(PointAlarmHistoryQueryParam queryParam);

    /**
     * 更新点位报警记录
     * @param pointAlarmHistory
     * @param pointAlarmHistoryRequest
     */
    public void updatePointAlarmHistory(PointAlarmHistory pointAlarmHistory,
            PointAlarmHistoryRequest pointAlarmHistoryRequest);

    /**
     * 批量确认
     * @param pointAlarmHistoryBatchRequest
     */
    public void batchConfirm(PointAlarmHistoryBatchRequest pointAlarmHistoryBatchRequest);
    
    /**
     * 通过任务历史记录id获取点位报警记录列表
     * @param taskHistoryId
     * @return
     */
    public List<PointAlarmHistory> listPointAlarmHistoryByTaskHistoryId(Integer taskHistoryId);
    
    /**
     * 告警记录二次确认
     * @param pointAlarmHistoryId
     * @return
     */
    public Task alarmTwiceConfirm(Integer pointAlarmHistoryId);
}
