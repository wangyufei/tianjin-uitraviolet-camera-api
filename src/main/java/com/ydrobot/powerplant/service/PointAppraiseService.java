package com.ydrobot.powerplant.service;
import com.ydrobot.powerplant.model.PointAppraise;
import com.ydrobot.powerplant.model.request.UpdatePointAppraiseStatusRequest;

import com.ydrobot.powerplant.core.service.Service;


/**
 * Created by Wyf on 2020/06/05.
 */
public interface PointAppraiseService extends Service<PointAppraise> {

	/**
	 * 更新部件评价状态
	 * @param pointAppraise
	 * @param request
	 */
	public void updatePointAppraiseStatus(PointAppraise pointAppraise,UpdatePointAppraiseStatusRequest request);
}
