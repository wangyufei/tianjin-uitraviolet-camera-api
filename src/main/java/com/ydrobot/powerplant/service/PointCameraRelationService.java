package com.ydrobot.powerplant.service;
import com.ydrobot.powerplant.model.PointCameraRelation;
import com.ydrobot.powerplant.model.condition.PointCameraQueryParam;
import com.ydrobot.powerplant.model.dto.PointCameraResult;

import java.util.List;

import com.ydrobot.powerplant.core.service.Service;


/**
 * Created by Wyf on 2020/11/15.
 */
public interface PointCameraRelationService extends Service<PointCameraRelation> {
	
	/**
	 * 获取点位摄像头关系列表
	 * @param queryParam
	 * @param page
	 * @param size
	 * @return
	 */
	public List<PointCameraResult> listPointCamera(PointCameraQueryParam queryParam, Integer page,Integer size);
}
