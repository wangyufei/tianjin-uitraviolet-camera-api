package com.ydrobot.powerplant.service;

import java.util.List;
import java.util.Map;

import com.ydrobot.powerplant.core.service.Service;
import com.ydrobot.powerplant.model.PointHistory;
import com.ydrobot.powerplant.model.Task;
import com.ydrobot.powerplant.model.condition.PointHistoryQueryParam;
import com.ydrobot.powerplant.model.condition.PointHistoryReportQueryParam;
import com.ydrobot.powerplant.model.condition.VerificationReportQueryParam;
import com.ydrobot.powerplant.model.dto.ObjectVerificationReportResult;
import com.ydrobot.powerplant.model.dto.PointHistoryContrastResult;
import com.ydrobot.powerplant.model.dto.PointHistoryCurveResult;
import com.ydrobot.powerplant.model.dto.PointHistoryReportResult;
import com.ydrobot.powerplant.model.dto.PointHistoryResult;
import com.ydrobot.powerplant.model.dto.PointHistoryVerificationReportResult;
import com.ydrobot.powerplant.model.dto.PointVerificationReportResult;
import com.ydrobot.powerplant.model.request.PointHistoryBatchRequest;
import com.ydrobot.powerplant.model.request.PointHistoryRequest;

/**
 * Created by Wyf on 2019/04/12.
 */
public interface PointHistoryService extends Service<PointHistory> {
    
    /**
     * 获取点位历史列表
     * @param pointHistoryCondition
     * @return
     */
    public List<PointHistoryResult> listPointHistory(PointHistoryQueryParam queryParam);

    /**
     * 获取点位历史对比数据
     * @param pointHistoryCondition
     * @return
     */
    public List<PointHistoryContrastResult> listPointHistoryContrast(PointHistoryQueryParam queryParam);

    /**
     * 审核点位记录
     * @param pointHistory
     * @param updatePointHistoryRequest
     */
    public void updatePointHistory(PointHistory pointHistory, PointHistoryRequest pointHistoryRequest);

    /**
     * 更新点位状态
     * @param pointHistoryId
     */
    public void updatePointStatus(PointHistory pointHistory);

    /**
     * 获取关系点历史
     * @param pointId
     * @param taskHistoryId
     * @return
     */
    public List<PointHistory> listRelationPointHistory(Integer pointId, Integer taskHistoryId);

    /**
     * 扩展点位历史记录信息
     * @param pointHistories
     * @return
     */
    public List<Map<String, Object>> extendRelationPoint(List<PointHistory> pointHistories);

    /**
     * 更新点位历史审核状态
     * @param pointHistoryRequest
     * @param pointHistoryId
     */
    public void updatePointHistoryCheckStatus(PointHistoryRequest pointHistoryRequest, Integer pointHistoryId);

    /**
     * 批量审核巡检结果
     * @param body
     */
    public void batchConfirm(PointHistoryBatchRequest body);
    
    /**
     * 通过任务巡检历史id获取点位历史列表
     * @param taskHistoryId
     * @return
     */
    public List<PointHistory> listPointHistoryByTaskHistoryId(Integer taskHistoryId);
    
    /**
     * 通过任务巡检历史id获取正常点位历史列表
     * @param taskHistoryId
     * @return
     */
    public List<PointHistory> listNormalPointHistoryByTaskHistoryId(Integer taskHistoryId);
    
    /**
     * 通过任务巡检历史id获取点位总数
     * @param taskHistoryId
     * @return
     */
    public int countPointTotalNumByTaskHistoryId(Integer taskHistoryId);
    
    /**
     * 获取巡检报表
     * @param queryParam
     * @return
     */
    public List<PointHistoryReportResult> listPointHistoryReport(PointHistoryReportQueryParam queryParam);
    
    /**
     * 组件核查报告列表
     * @param queryParam
     * @return
     */
    public List<PointVerificationReportResult> listPointVerificationReport(VerificationReportQueryParam queryParam);
    
    /**
     * 设备核查报告信息
     * @param queryParam
     * @return
     */
    public ObjectVerificationReportResult getObjectVerificationReport(VerificationReportQueryParam queryParam);

    /**
     * 获取部件一周历史曲线
     * @param pointId
     */
    public List<PointHistoryCurveResult> countPointHistoryCurve(Integer pointId);
    
    /**
     * 点位记录二次确认
     * @param pointHistoryId
     * @return
     */
    public Task twiceConfirm(Integer pointHistoryId);
    
    /**
     * 获取部件评价的原因
     * @param pointHistoryId
     * @return
     */
    public List<PointHistoryVerificationReportResult> getPointAppraiseReason(Integer pointHistoryId);
}
