package com.ydrobot.powerplant.service;

import java.util.List;
import java.util.Map;

import com.ydrobot.powerplant.core.service.Service;
import com.ydrobot.powerplant.model.PointRelationSetting;
import com.ydrobot.powerplant.model.condition.PointRelationSettingQueryParam;
import com.ydrobot.powerplant.model.condition.PointRelationSettingListQueryParam;
import com.ydrobot.powerplant.model.request.PointRelationSettingBatchRequest;

/**
 * Created by Wyf on 2019/04/12.
 */
public interface PointRelationSettingService extends Service<PointRelationSetting> {

    /**
     * 获取点位关系列表
     * @param pointRelationSettingListCondition
     * @param page
     * @param size
     * @return
     */
    public List<PointRelationSetting> listPointRelationSetting(PointRelationSettingListQueryParam queryParam);
    
    /**
     * 更新点位关系设置
     * @param pointRelationSettingRequest
     */
    public void updatePointRelationSetting(PointRelationSettingBatchRequest pointRelationSettingBatchRequest);

    /**
     * 获取关联点
     * @param pointRelationSettingCondition
     * @return
     */
    public List<Map<String, Object>> listRelationPoint(PointRelationSettingQueryParam queryParam);
}
