package com.ydrobot.powerplant.service;

import java.util.List;

import com.ydrobot.powerplant.core.service.Service;
import com.ydrobot.powerplant.model.Device;
import com.ydrobot.powerplant.model.Point;
import com.ydrobot.powerplant.model.PointAppraise;
import com.ydrobot.powerplant.model.WatchPoint;
import com.ydrobot.powerplant.model.condition.PointQueryParam;
import com.ydrobot.powerplant.model.condition.PointTreeQueryParam;
import com.ydrobot.powerplant.model.dto.AbnormalAppraisePointResult;
import com.ydrobot.powerplant.model.dto.PointResult;
import com.ydrobot.powerplant.model.request.PointRequest;

/**
 * Created by Wyf on 2019/04/12.
 */
public interface PointService extends Service<Point> {

	/**
	 * 获取点位列表
	 */
	public List<Point> listPoint(PointQueryParam queryParam,Integer page,Integer size);
	
    /**
     * 获取设备树
     * @return
     */
    public List<Device> listPointTree(Long random,PointTreeQueryParam queryParam);

    /**
     * 获取异常设备树
     * @param random
     * @return
     */
    public List<Device> listAbnormalPointTree(Long random,PointTreeQueryParam queryParam);

    /**
     * 保存点位信息
     * @param createPointRequest
     */
    public Integer savePoint(PointRequest pointRequest);

    /**
     * 更新点位信息
     * @param updatePointRequest
     */
    public void updatePoint(Point point, PointRequest pointRequest);
    
    /**
     * 获取点位详情
     * @param pointId
     * @return
     */
    public PointResult getDetail(Integer pointId);
    
    /**
     * 通过点位id获取观察点列表
     * @param pointId
     * @return
     */
    public List<WatchPoint> listWatchPointByPointId(Integer pointId);
    
    /**
     * 获取异常点位列表
     * @return
     */
    public List<AbnormalAppraisePointResult> listAbnormalAppraisePoint();
    
    /**
     * 更新点位评价信息
     * @param pointAppraise
     */
    public void updatePointAppraise(PointAppraise pointAppraise);
}
