package com.ydrobot.powerplant.service;

import java.util.List;
import com.ydrobot.powerplant.core.service.Service;
import com.ydrobot.powerplant.model.ReconModel;
import com.ydrobot.powerplant.model.dto.ReconModelResult;

/**
 * Created by Wyf on 2019/05/27.
 */
public interface ReconModelService extends Service<ReconModel> {

	/**
	 * 获取识别模型列表
	 * 
	 * @param page
	 * @param size
	 * @return
	 */
	public List<ReconModelResult> listReconModel(Integer page, Integer size);
}
