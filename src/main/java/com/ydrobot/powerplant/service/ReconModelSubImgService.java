package com.ydrobot.powerplant.service;

import java.util.List;

import com.ydrobot.powerplant.core.service.Service;
import com.ydrobot.powerplant.model.ReconModelSubImg;
import com.ydrobot.powerplant.model.condition.ReconModelSubImgQueryParam;
import com.ydrobot.powerplant.model.request.ReconModelSubImgRequest;

/**
 * Created by Wyf on 2019/05/27.
 */
public interface ReconModelSubImgService extends Service<ReconModelSubImg> {

    /**
     * 获取识别模型子类图标查询条件
     * @param queryParam
     * @param page
     * @param size
     * @return
     */
    public List<ReconModelSubImg> listReconModelSubImg(ReconModelSubImgQueryParam queryParam,Integer page,Integer size);

    /**
     * 保存识别模型子类图片
     * @param reconModelSubImgRequest
     */
    public void saveReconModelSubImg(ReconModelSubImgRequest reconModelSubImgRequest);

    /**
     * 更细识别模型子类图片信息
     * @param reconModelSubImg
     * @param reconModelSubImgRequest
     */
    public void updateReconModelSubImg(ReconModelSubImg reconModelSubImg,
            ReconModelSubImgRequest reconModelSubImgRequest);
}
