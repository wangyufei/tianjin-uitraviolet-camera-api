package com.ydrobot.powerplant.service;

import java.util.List;

import com.ydrobot.powerplant.core.service.Service;
import com.ydrobot.powerplant.model.ReconModelSub;
import com.ydrobot.powerplant.model.condition.ReconModelSubQueryParam;
import com.ydrobot.powerplant.model.request.ReconModelSubRequest;

/**
 * Created by Wyf on 2019/05/27.
 */
public interface ReconModelSubService extends Service<ReconModelSub> {

    /**
     * 获取识别模型子类列表
     * @param reconModelSubCondition
     * @param page
     * @param size
     * @return
     */
    public List<ReconModelSub> listReconModelSub(ReconModelSubQueryParam queryParam,Integer page,Integer size);
    
    /**
     * 创建识别模型子类
     * @param reconModelSubRequest
     */
    public void saveReconModelSub(ReconModelSubRequest reconModelSubRequest);

    /**
     * 更新识别模型子类
     * @param reconModelSub
     * @param reconModelSubRequest
     */
    public void updateReconModelSub(ReconModelSub reconModelSub, ReconModelSubRequest reconModelSubRequest);
}
