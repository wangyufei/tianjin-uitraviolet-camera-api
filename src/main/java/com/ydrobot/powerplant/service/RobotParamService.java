package com.ydrobot.powerplant.service;

import java.util.List;

import com.ydrobot.powerplant.core.service.Service;
import com.ydrobot.powerplant.model.RobotParam;
import com.ydrobot.powerplant.model.condition.RobotParamQueryParam;
import com.ydrobot.powerplant.model.request.RobotParamBatchRequest;

/**
 * Created by Wyf on 2019/04/12.
 */
public interface RobotParamService extends Service<RobotParam> {
    
    /**
     * 获取机器人参数列表
     * @param robotParamCondition
     * @param page
     * @param size
     * @return
     */
    public List<RobotParam> listRobotParam(RobotParamQueryParam queryParam,Integer page,Integer size);

    /**
     * 批量更新机器人参数
     * @param robotParamBatchRequest
     */
    public void batchUpdate(RobotParamBatchRequest robotParamBatchRequest);
}
