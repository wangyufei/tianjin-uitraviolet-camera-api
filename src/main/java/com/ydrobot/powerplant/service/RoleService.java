package com.ydrobot.powerplant.service;

import java.util.List;

import com.ydrobot.powerplant.core.service.Service;
import com.ydrobot.powerplant.model.Role;
import com.ydrobot.powerplant.model.dto.RoleResult;
import com.ydrobot.powerplant.model.request.RoleRequest;
import com.ydrobot.powerplant.model.request.UpdateRolePermissionRequest;

/**
 * Created by Wyf on 2019/04/12.
 */
public interface RoleService extends Service<Role> {

	/**
	 * 更新角色权限
	 * 
	 * @param role
	 * @param updateRolePermissionRequest
	 */
	public void updateRolePermission(Role role, UpdateRolePermissionRequest updateRolePermissionRequest);

	/**
	 * 获取角色列表
	 * 
	 * @param page
	 * @param size
	 * @return
	 */
	public List<RoleResult> listRole(Integer page, Integer size);

	/**
	 * 保存角色信息
	 * 
	 * @param roleRequest
	 */
	public void saveRole(RoleRequest roleRequest);

	/**
	 * 更新角色信息
	 * 
	 * @param role
	 * @param roleRequest
	 */
	public void updateRole(Role role, RoleRequest roleRequest);
}
