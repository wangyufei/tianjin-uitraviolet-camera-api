package com.ydrobot.powerplant.service;
import com.ydrobot.powerplant.model.RoutePos;

import java.util.List;

import com.ydrobot.powerplant.core.service.Service;


/**
 * Created by Wyf on 2019/04/12.
 */
public interface RoutePosService extends Service<RoutePos> {
	
	/**
	 * 添加点
	 * @param point
	 * @return
	 */
	public Integer saveRoutePos(List<String> point);
	
	/**
	 * 删除道路点
	 * @param id
	 */
	public void deleteRoutePos(Integer id);
	
	/**
	 * 通过点的xyz获取道路点信息
	 * @param point
	 * @return
	 */
	public RoutePos getRoutePosByPoint(String[] point);
}
