package com.ydrobot.powerplant.service;
import com.ydrobot.powerplant.model.StopPoint;
import com.ydrobot.powerplant.model.dto.RouteWithPosResult;

import java.util.List;

import com.ydrobot.powerplant.core.service.Service;


/**
 * Created by Wyf on 2019/04/12.
 */
public interface StopPointService extends Service<StopPoint> {
	
	/**
	 * 更新停靠点道路
	 * @param routeWithPosVos
	 */
	public void updateStopPoint(List<RouteWithPosResult> routeWithPosResults);
	
	/**
	 * 更新停靠点道路id
	 * @param sourceRouteId
	 * @param targetRouteId
	 */
	public void updateStopPoint(Integer sourceRouteId,Integer targetRouteId);
}
