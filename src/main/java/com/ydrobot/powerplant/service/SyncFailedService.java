package com.ydrobot.powerplant.service;
import com.ydrobot.powerplant.model.SyncFailed;
import com.ydrobot.powerplant.core.service.Service;


/**
 * Created by Wyf on 2020/07/15.
 */
public interface SyncFailedService extends Service<SyncFailed> {
	
	/**
	 * 保存同步失败的数据
	 * @param tableName
	 * @param dataId
	 */
	public void saveSyncFailed(String tableName,Integer dataId);
}
