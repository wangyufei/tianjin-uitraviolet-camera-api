package com.ydrobot.powerplant.service;

import java.util.List;

import com.ydrobot.powerplant.core.service.Service;
import com.ydrobot.powerplant.model.SysPointAlarmHistory;
import com.ydrobot.powerplant.model.condition.SysPointAlarmHistoryQueryParam;
import com.ydrobot.powerplant.model.dto.SysPointAlarmHistoryResult;
import com.ydrobot.powerplant.model.request.SysPointAlarmHistoryBatchCheckRequest;

/**
 * Created by Wyf on 2019/04/12.
 */
public interface SysPointAlarmHistoryService extends Service<SysPointAlarmHistory> {
    
    /**
     * 获取系统点位报警历史列表
     * @param sysPointAlarmHistoryCondition
     * @param page
     * @param size
     * @return
     */
    public List<SysPointAlarmHistoryResult> listSysPointAlarmHistory(SysPointAlarmHistoryQueryParam queryParam,Integer page,Integer size);
    
    /**
     * 批量审核
     * @param request
     */
    public void batchCheck(SysPointAlarmHistoryBatchCheckRequest request);
    
    /**
     * 获取报警数
     * @return
     */
    public Integer getAlarmNum();
}
