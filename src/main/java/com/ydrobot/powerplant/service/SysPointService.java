package com.ydrobot.powerplant.service;
import com.ydrobot.powerplant.model.SysPoint;
import com.ydrobot.powerplant.core.service.Service;


/**
 * Created by Wyf on 2019/04/12.
 */
public interface SysPointService extends Service<SysPoint> {

	/**
	 * 保存系统点位
	 * @param sysPoint
	 */
	public void saveSysPoint(SysPoint sysPoint);
}
