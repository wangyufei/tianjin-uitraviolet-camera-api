package com.ydrobot.powerplant.service;

import com.ydrobot.powerplant.core.service.Service;
import com.ydrobot.powerplant.model.SystemSetting;
import com.ydrobot.powerplant.model.request.SystemSettingBatchRequest;

/**
 * Created by Wyf on 2019/06/19.
 */
public interface SystemSettingService extends Service<SystemSetting> {

    /**
     * 批量更新
     * @param systemSettingBatchRequest
     */
    public void batchUpdate(SystemSettingBatchRequest systemSettingBatchRequest);
}
