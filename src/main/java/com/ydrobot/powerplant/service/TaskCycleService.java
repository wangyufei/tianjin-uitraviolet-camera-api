package com.ydrobot.powerplant.service;

import java.util.List;

import com.ydrobot.powerplant.core.service.Service;
import com.ydrobot.powerplant.model.TaskCycle;
import com.ydrobot.powerplant.model.condition.TaskCycleQueryParam;
import com.ydrobot.powerplant.model.request.TaskCycleRequest;

/**
 * Created by Wyf on 2019/04/12.
 */
public interface TaskCycleService extends Service<TaskCycle> {
    
    /**
     * 获取任务周期列表
     * @param taskCycleCondition
     * @param page
     * @param size
     * @return
     */
    public List<TaskCycle> listTaskCycle(TaskCycleQueryParam queryParam,Integer page,Integer size);

    /**
     * 添加任务周期
     * @param updateTaskCycleRequest
     */
    public void createTaskCycle(TaskCycleRequest taskCycleRequest);

    /**
     * 更新任务周期
     * @param taskCycle
     * @param updateTaskCycleRequest
     */
    public void updateTaskCycle(TaskCycle taskCycle, TaskCycleRequest taskCycleRequest);
}
