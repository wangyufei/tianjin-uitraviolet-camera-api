package com.ydrobot.powerplant.service;

import com.ydrobot.powerplant.core.service.Service;
import com.ydrobot.powerplant.model.TaskPoint;

/**
 * Created by Wyf on 2019/04/12.
 */
public interface TaskPointService extends Service<TaskPoint> {

    /**
     * 通过任务id删除关联点信息
     * @param taskId
     */
    public void deleteByTaskId(Integer taskId);
}
