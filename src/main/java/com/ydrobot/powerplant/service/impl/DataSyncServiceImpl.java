package com.ydrobot.powerplant.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.ydrobot.powerplant.core.constant.TableNameConsts;
import com.ydrobot.powerplant.core.utils.ProjectHttpUtils;
import com.ydrobot.powerplant.dao.ObjectMapper;
import com.ydrobot.powerplant.dao.PointAlarmHistoryMapper;
import com.ydrobot.powerplant.dao.PointHistoryMapper;
import com.ydrobot.powerplant.dao.PointMapper;
import com.ydrobot.powerplant.dao.TaskHistoryMapper;
import com.ydrobot.powerplant.dao.TaskMapper;
import com.ydrobot.powerplant.model.ObjectAlarmHistory;
import com.ydrobot.powerplant.model.Point;
import com.ydrobot.powerplant.model.PointAlarmHistory;
import com.ydrobot.powerplant.model.PointAppraise;
import com.ydrobot.powerplant.model.PointAppraiseHistory;
import com.ydrobot.powerplant.model.PointHistory;
import com.ydrobot.powerplant.model.SysPointAlarmHistory;
import com.ydrobot.powerplant.model.TObject;
import com.ydrobot.powerplant.model.Task;
import com.ydrobot.powerplant.model.TaskHistory;
import com.ydrobot.powerplant.model.datasync.AlarmHistorySync;
import com.ydrobot.powerplant.model.datasync.ObjectSync;
import com.ydrobot.powerplant.model.datasync.PointAppraiseHistorySync;
import com.ydrobot.powerplant.model.datasync.PointAppraiseSync;
import com.ydrobot.powerplant.model.datasync.PointHistorySync;
import com.ydrobot.powerplant.model.datasync.PointSync;
import com.ydrobot.powerplant.model.datasync.SysPointAlarmHistorySync;
import com.ydrobot.powerplant.model.datasync.TaskHistorySync;
import com.ydrobot.powerplant.model.datasync.TaskSync;
import com.ydrobot.powerplant.service.DataSyncService;
import com.ydrobot.powerplant.service.SyncFailedService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class DataSyncServiceImpl implements DataSyncService {

	private static final Logger log = LoggerFactory.getLogger(DataSyncServiceImpl.class);

	@Value("${cloud.url}")
	private String cloudUrl;
	@Value("${station.id}")
	private String stationId;
	@Resource
	private ProjectHttpUtils projectHttpUtils;
	@Resource
	private PointMapper pointMapper;
	@Resource
	private TaskHistoryMapper taskHistoryMapper;
	@Resource
	private TaskMapper taskMapper;
	@Resource
	private PointHistoryMapper pointHistoryMapper;
	@Resource
	private PointAlarmHistoryMapper pointAlarmHistoryMapper;
	@Resource
	private ObjectMapper objectMapper;
	@Resource
	private SyncFailedService syncFailedService;

	@Async
	@Override
	public String pointAlarmHistorySync(PointAlarmHistory pointAlarmHistory) {
		// 同步状态 0:失败 1:成功
		String syncStatus = "0";

		try {
			AlarmHistorySync alarmHistorySync = new AlarmHistorySync();
			BeanUtils.copyProperties(pointAlarmHistory, alarmHistorySync);
			alarmHistorySync.setStationId(Integer.valueOf(stationId));
			alarmHistorySync.setExtendId(pointAlarmHistory.getId());
			alarmHistorySync.setSource(new Byte("0")); // source 0:本地 1:第三方

			Point point = pointMapper.selectByPrimaryKey(pointAlarmHistory.getPointId());

			if (point != null) {
				alarmHistorySync.setObjectId(point.getObjectId());
			}

			TaskHistory taskHistory = taskHistoryMapper.selectByPrimaryKey(pointAlarmHistory.getTaskHistoryId());
			if (taskHistory != null) {
				Task task = taskMapper.selectByPrimaryKey(taskHistory.getTaskId());
				if (task != null) {
					alarmHistorySync.setInspectTypeId(task.getInspectTypeId());
					alarmHistorySync.setTaskId(task.getId());
				}
			}

			String param = JSONObject.toJSONString(alarmHistorySync);
			String result = projectHttpUtils.post(cloudUrl + "/data-syncs/alarm-historys", param);
			JSONObject jsonObject = JSONObject.parseObject(result);
			if (jsonObject.getInteger("code") == 1) {
				syncStatus = "1";
				log.info("点位报警记录同步：" + result);
			}
		} catch (Exception e) {
			syncFailedService.saveSyncFailed(TableNameConsts.POINT_ALARM_HISTORY, pointAlarmHistory.getId());
			log.error("点位报警记录同步：" + e.getMessage());
		}

		return syncStatus;
	}

	@Async
	@Override
	public String taskHistorySync(TaskHistory taskHistory) {
		// 同步状态 0:失败 1:成功
		String syncStatus = "0";

		try {
			TaskHistorySync taskHistorySync = new TaskHistorySync();
			BeanUtils.copyProperties(taskHistory, taskHistorySync);
			taskHistorySync.setId(null);
			taskHistorySync.setStationId(Integer.valueOf(stationId));
			taskHistorySync.setExtendId(taskHistory.getId());
			String param = JSONObject.toJSONString(taskHistorySync);
			String result = projectHttpUtils.post(cloudUrl + "/data-syncs/task-historys", param);
			JSONObject jsonObject = JSONObject.parseObject(result);
			if (jsonObject.getInteger("code") == 1) {
				syncStatus = "1";
				log.info("任务执行记录同步：" + result);
			}

			// 当任务执行完成时再同步一下点位记录和点位报警记录 0:已执行
			if (taskHistory != null && taskHistory.getTaskStatus() != null && taskHistory.getTaskStatus() == 0) {
				PointHistory pointHistoryRecord = new PointHistory();
				pointHistoryRecord.setTaskHistoryId(taskHistory.getId());
				List<PointHistory> pointHistories = pointHistoryMapper.select(pointHistoryRecord);
				for (PointHistory pointHistory : pointHistories) {
					pointHistorySync(pointHistory);
				}

				PointAlarmHistory pointAlarmHistoryRecord = new PointAlarmHistory();
				pointAlarmHistoryRecord.setTaskHistoryId(taskHistory.getId());
				List<PointAlarmHistory> pointAlarmHistories = pointAlarmHistoryMapper.select(pointAlarmHistoryRecord);
				for (PointAlarmHistory pointAlarmHistory : pointAlarmHistories) {
					pointAlarmHistorySync(pointAlarmHistory);
				}
			}
		} catch (Exception e) {
			syncFailedService.saveSyncFailed(TableNameConsts.TASK_HISTORY, taskHistory.getId());
			log.error("任务执行记录同步" + e.getMessage());
		}
		
		return syncStatus;
	}
	

	@Override
	public String sysPointAlarmHistorySync(SysPointAlarmHistory sysPointAlarmHistory) {
		// 同步状态 0:失败 1:成功
		String syncStatus = "0";
		try {
			SysPointAlarmHistorySync sysPointAlarmHistorySync = new SysPointAlarmHistorySync();
			BeanUtils.copyProperties(sysPointAlarmHistory, sysPointAlarmHistorySync);
			sysPointAlarmHistorySync.setId(null);
			sysPointAlarmHistorySync.setStationId(Integer.valueOf(stationId));
			sysPointAlarmHistorySync.setExtendId(sysPointAlarmHistory.getId());
			String param = JSONObject.toJSONString(sysPointAlarmHistorySync);
			String result = projectHttpUtils.post(cloudUrl + "/data-syncs/sys-point-alarm-historys", param);
			JSONObject jsonObject = JSONObject.parseObject(result);
			if (jsonObject.getInteger("code") == 1) {
				syncStatus = "1";
				log.info("系统点位告警记录同步：" + result);
			}
		} catch (Exception e) {
			syncFailedService.saveSyncFailed(TableNameConsts.SYS_POINT_ALARM_HISTORY, sysPointAlarmHistory.getId());
			log.error("系统点位告警记录:" + e.getMessage());
		}
		
		return syncStatus;
	}

	@Async
	@Override
	public String TaskSync(Task task) {
		// 同步状态 0:失败 1:成功
		String syncStatus = "0";
		try {
			TaskSync taskSync = new TaskSync();
			BeanUtils.copyProperties(task, taskSync);
			taskSync.setId(null);
			taskSync.setStationId(Integer.valueOf(stationId));
			taskSync.setExtendId(task.getId());
			String param = JSONObject.toJSONString(taskSync);
			String result = projectHttpUtils.post(cloudUrl + "/data-syncs/tasks", param);
			JSONObject jsonObject = JSONObject.parseObject(result);
			if (jsonObject.getInteger("code") == 1) {
				syncStatus = "1";
				log.info("任务同步：" + result);
			}
		} catch (Exception e) {
			syncFailedService.saveSyncFailed(TableNameConsts.TASK, task.getId());
			log.error("任务同步:" + e.getMessage());
		}
		
		return syncStatus;
	}

	@Async
	@Override
	public String objectAlarmHistorySync(ObjectAlarmHistory objectAlarmHistory) {
		// 同步状态 0:失败 1:成功
		String syncStatus = "0";
		try {
			AlarmHistorySync alarmHistorySync = new AlarmHistorySync();
			BeanUtils.copyProperties(objectAlarmHistory, alarmHistorySync);
			alarmHistorySync.setStationId(Integer.valueOf(stationId));
			alarmHistorySync.setExtendId(objectAlarmHistory.getId());
			alarmHistorySync.setSource(new Byte("1"));
			alarmHistorySync.setInspectTypeId(6000);
			String param = JSONObject.toJSONString(alarmHistorySync);
			String result = projectHttpUtils.post(cloudUrl + "/data-syncs/alarm-historys", param);
			JSONObject jsonObject = JSONObject.parseObject(result);
			if (jsonObject.getInteger("code") == 1) {
				syncStatus = "1";
				log.info("第三方对象报警记录同步：" + result);
			}
			
		} catch (Exception e) {
			syncFailedService.saveSyncFailed(TableNameConsts.OBJECT_ALARM_HISTORY, objectAlarmHistory.getId());
			log.error("第三方对象报警记录同步：" + e.getMessage());
		}
		
		return syncStatus;
	}

	@Async
	@Override
	public void twiceConfirmAlarmHistorySync(AlarmHistorySync alarmHistorySync) {
		try {
			alarmHistorySync.setStationId(Integer.valueOf(stationId));
			TaskHistory taskHistory = taskHistoryMapper.selectByPrimaryKey(alarmHistorySync.getTaskHistoryId());
			if (taskHistory != null) {
				Task task = taskMapper.selectByPrimaryKey(taskHistory.getTaskId());
				if (task != null) {
					alarmHistorySync.setTaskId(task.getId());
				}
			}
			String param = JSONObject.toJSONString(alarmHistorySync);
			String result = projectHttpUtils.post(cloudUrl + "/data-syncs/alarm-historys", param);
			log.info("二次确认告警记录同步：" + result);
		} catch (Exception e) {
			log.error("二次确认告警记录同步：" + e.getMessage());
		}

	}

	@Async
	@Override
	public String pointAppraiseSync(PointAppraise pointAppraise) {
		// 同步状态 0:失败 1:成功
		String syncStatus = "0";

		try {
			PointAppraiseSync pointAppraiseSync = new PointAppraiseSync();
			BeanUtils.copyProperties(pointAppraise, pointAppraiseSync);
			pointAppraiseSync.setId(null);
			pointAppraiseSync.setStationId(Integer.valueOf(stationId));
			pointAppraiseSync.setExtendId(pointAppraise.getId());
			String param = JSONObject.toJSONString(pointAppraiseSync);
			String result = projectHttpUtils.post(cloudUrl + "/data-syncs/point-appraise", param);
			JSONObject jsonObject = JSONObject.parseObject(result);
			if (jsonObject.getInteger("code") == 1) {
				syncStatus = "1";
				log.error("点位评价状态同步成功：" + result);
			}
		} catch (Exception e) {
			syncFailedService.saveSyncFailed(TableNameConsts.POINT_APPRAISE, pointAppraise.getId());
			log.error("点位评价状态同步：" + e.getMessage());
		}

		return syncStatus;
	}

	@Async
	@Override
	public String pointAppraiseHistorySync(PointAppraiseHistory pointAppraiseHistory) {
		// 同步状态 0:失败 1:成功
		String syncStatus = "0";
		try {
			PointAppraiseHistorySync pointAppraiseHistorySync = new PointAppraiseHistorySync();
			BeanUtils.copyProperties(pointAppraiseHistory, pointAppraiseHistorySync);
			pointAppraiseHistorySync.setId(null);
			pointAppraiseHistorySync.setStationId(Integer.valueOf(stationId));
			pointAppraiseHistorySync.setExtendId(pointAppraiseHistory.getId());
			String param = JSONObject.toJSONString(pointAppraiseHistorySync);
			String result = projectHttpUtils.post(cloudUrl + "/data-syncs/point-appraise-historys", param);
			JSONObject jsonObject = JSONObject.parseObject(result);
			if (jsonObject.getInteger("code") == 1) {
				syncStatus = "1";
				log.info("点位评价历史记录同步：" + result);
			}
		} catch (Exception e) {
			syncFailedService.saveSyncFailed(TableNameConsts.POINT_APPRAISE_HISTORY, pointAppraiseHistory.getId());
			log.error("点位评价历史记录同步：" + e.getMessage());
		}
		
		return syncStatus;
	}

	@Async
	@Override
	public String pointHistorySync(PointHistory pointHistory) {
		// 同步状态 0:失败 1:成功
		String syncStatus = "0";
		try {
			PointHistorySync pointHistorySync = new PointHistorySync();
			BeanUtils.copyProperties(pointHistory, pointHistorySync);
			pointHistorySync.setId(null);
			pointHistorySync.setStationId(Integer.valueOf(stationId));
			pointHistorySync.setExtendId(pointHistory.getId());
			String param = JSONObject.toJSONString(pointHistorySync);
			String result = projectHttpUtils.post(cloudUrl + "/data-syncs/point-historys", param);
			JSONObject jsonObject = JSONObject.parseObject(result);
			if (jsonObject.getInteger("code") == 1) {
				syncStatus = "1";
				log.info("点位历史记录同步：" + result);
			}
		} catch (Exception e) {
			syncFailedService.saveSyncFailed(TableNameConsts.POINT_HISTORY, pointHistory.getId());
			log.error("点位历史记录同步：" + e.getMessage());
		}
		return syncStatus;
	}

	@Async
	@Override
	public void pointSync() {
		try {
			List<Point> points = pointMapper.selectAll();
			List<PointSync> pointSyncs = new ArrayList<PointSync>();
			for (Point point : points) {
				PointSync pointSync = new PointSync();
				BeanUtils.copyProperties(point, pointSync);
				pointSync.setId(null);
				pointSync.setExtendId(point.getId());
				pointSync.setStationId(Integer.valueOf(stationId));
				pointSyncs.add(pointSync);
			}

			String param = JSONObject.toJSONString(pointSyncs);
			String result = projectHttpUtils.post(cloudUrl + "/data-syncs/points", param);
			log.info("点位同步：" + result);
		} catch (Exception e) {
			log.error("点位同步：" + e.getMessage());
		}

	}

	@Async
	@Override
	public void objectSync() {
		try {
			List<TObject> objects = objectMapper.selectAll();
			List<ObjectSync> objectSyncs = new ArrayList<ObjectSync>();
			for (TObject object : objects) {
				ObjectSync objectSync = new ObjectSync();
				BeanUtils.copyProperties(object, objectSync);
				objectSync.setId(null);
				objectSync.setExtendId(object.getId());
				objectSync.setStationId(Integer.valueOf(stationId));
				objectSyncs.add(objectSync);
			}

			String param = JSONObject.toJSONString(objectSyncs);
			String result = projectHttpUtils.post(cloudUrl + "/data-syncs/objects", param);
			log.info("对象同步：" + result);
		} catch (Exception e) {
			log.error("对象同步：" + e.getMessage());
		}

	}
}
