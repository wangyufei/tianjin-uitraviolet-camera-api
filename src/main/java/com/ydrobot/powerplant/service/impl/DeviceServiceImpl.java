package com.ydrobot.powerplant.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ydrobot.powerplant.core.constant.ProjectConsts;
import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.dao.DeviceMapper;
import com.ydrobot.powerplant.dao.PointMapper;
import com.ydrobot.powerplant.model.Device;
import com.ydrobot.powerplant.model.TObject;
import com.ydrobot.powerplant.model.condition.DeviceIntervalQueryParam;
import com.ydrobot.powerplant.service.DeviceService;
import com.ydrobot.powerplant.service.ObjectService;

import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class DeviceServiceImpl extends AbstractService<Device> implements DeviceService {
	@Resource
	private DeviceMapper deviceMapper;
	@Resource
	private PointMapper pointMapper;
	@Resource
	private ObjectService objectService;

	@Override
	public List<Device> listArea() {
		Device device = new Device();
		device.setModelLevel(ProjectConsts.DEVICE_AREA_MODEL_LEVEL);
		return deviceMapper.select(device);
	}

	@Override
	public List<Device> listInterval(DeviceIntervalQueryParam queryParam) {
		Condition condition = new Condition(Device.class);
		Criteria criteria = condition.createCriteria();

		if (queryParam.getDeviceAreaId() != null) {
			criteria.andEqualTo("parentId", queryParam.getDeviceAreaId());
		}

		if (queryParam.getIntervalName() != null) {
			criteria.andLike("name", "%" + queryParam.getIntervalName() + "%");
		}

		criteria.andEqualTo("modelLevel", ProjectConsts.DEVICE_INTERVAL_MODEL_LEVEL);
		List<Device> devices = findByCondition(condition);

		for (Device device : devices) {
			Integer alarmlevel = pointMapper.getMaxAlarmLevelByDeviceIntervalId(device.getId());
			device.setAlarmLevel(alarmlevel);
		}

		return devices;
	}

	@Override
	public List<Device> listDevicesByIntervalId(Integer intervalId) {
		List<TObject> tObjects = objectService.listObjectByDeviceId(intervalId);

		List<Device> devices = new ArrayList<>();
		for (TObject tObject : tObjects) {
			Device device = new Device();
			device.setId(tObject.getId());
			device.setModelLevel(ProjectConsts.DEVICE_MODEL_LEVEL);
			device.setName(tObject.getName());
			device.setRealId(tObject.getId());
			device.setParentId(intervalId);
			device.setAlarmLevel(pointMapper.getMaxAlarmLevelByObjectId(tObject.getId()));
			devices.add(device);
		}

		return devices;
	}

	@Override
	public String getDeviceAreaNameByDeviceId(Integer id) {
		String deviceAreaName = "";
		Device device = deviceMapper.selectByPrimaryKey(id);
		if (device != null) {
			if (device.getModelLevel().intValue() == ProjectConsts.DEVICE_AREA_MODEL_LEVEL) {
				deviceAreaName = device.getName();
			} else {
				return getDeviceAreaNameByDeviceId(device.getParentId());
			}
		}
		return deviceAreaName;
	}

	@Override
	public String getDeviceIntervalNameByDeviceId(Integer id) {
		String deviceIntervalName = "";
		Device device = deviceMapper.selectByPrimaryKey(id);
		if (device != null) {
			if (device.getModelLevel().intValue() == ProjectConsts.DEVICE_INTERVAL_MODEL_LEVEL) {
				deviceIntervalName = device.getName();
			} else {
				return getDeviceIntervalNameByDeviceId(device.getParentId());
			}
		}

		return deviceIntervalName;
	}

	@Override
	public List<Device> getDeviceTree() {
		List<Device> devices = deviceMapper.getStationTree();
		List<Device> devicesTree = listToTree(devices);
		return devicesTree;
	}
	
	/**
	 * 列表转树
	 * 
	 * @param devices
	 * @return
	 */
	private List<Device> listToTree(List<Device> devices) {
		List<Device> deviceTree = new ArrayList<Device>();
		for (Device device : devices) {
			// 层级为变电站，则是一级数据
			if (device.getModelLevel().intValue() == ProjectConsts.DEVICE_STAND_MODEL_LEVEL) {
				deviceTree.add(device);
			}

			// 循环出子级数据，添加到一级数据
			for (Device subDevice : devices) {
				if (subDevice.getParentId().intValue() == device.getId().intValue()) {
					if (device.getChildren() == null) {
						device.setChildren(new ArrayList<>());
					}
					device.getChildren().add(subDevice);
				}
			}
		}

		return deviceTree;
	}
}
