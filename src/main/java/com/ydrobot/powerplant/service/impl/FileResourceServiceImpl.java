package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.FileResourceMapper;
import com.ydrobot.powerplant.model.FileResource;
import com.ydrobot.powerplant.model.condition.FileResourceQueryParam;
import com.ydrobot.powerplant.service.FileResourceService;

import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;

import com.github.pagehelper.PageHelper;
import com.ydrobot.powerplant.core.service.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.annotation.Resource;


/**
 * Created by Wyf on 2020/05/13.
 */
@Service
@Transactional
public class FileResourceServiceImpl extends AbstractService<FileResource> implements FileResourceService {
    @Resource
    private FileResourceMapper fileResourceMapper;

	@Override
	public List<FileResource> listFileResource(FileResourceQueryParam queryParam, Integer page, Integer size) {
		PageHelper.startPage(page, size);
		Condition condition = new Condition(FileResource.class);
		Criteria criteria = condition.createCriteria();

		if (queryParam.getType() != null) {
			criteria.andEqualTo("type", queryParam.getType());
		}
		
		condition.setOrderByClause("create_time desc");
		return findByCondition(condition);
	}

    
}
