package com.ydrobot.powerplant.service.impl;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.ydrobot.powerplant.core.constant.ProjectConsts;
import com.ydrobot.powerplant.core.exception.ServiceException;
import com.ydrobot.powerplant.core.result.ResultCode;
import com.ydrobot.powerplant.core.utils.ProjectMD5Utils;
import com.ydrobot.powerplant.core.utils.ProjectSqlUtils;
import com.ydrobot.powerplant.service.FileService;
import com.ydrobot.powerplant.service.TaskService;

@Service
public class FileServiceImpl implements FileService {
	
	private static final Logger log = LoggerFactory.getLogger(FileServiceImpl.class);

	@Autowired
	private ProjectSqlUtils projectSqlUtils;
	@Autowired
	private TaskService taskService;
	
    @Value("${file.path}")
    private String path;

    @Override
    public String uploadFile(MultipartFile file, String extName) throws IOException {
        String fileId = ProjectMD5Utils.getMD5String(String.valueOf(System.currentTimeMillis())) + extName;
        OutputStream fs = new FileOutputStream(path + "/" + ProjectConsts.PREFIX_FILE + "/" + fileId);
        IOUtils.write(file.getBytes(), fs);
        IOUtils.closeQuietly(fs);
        return fileId;
    }

	@Override
	public void importSql(MultipartFile file){
		String sqlFilePath = path + "/" + ProjectConsts.PREFIX_FILE + "/"+file.getOriginalFilename();

		try {
			OutputStream fs;
			fs = new FileOutputStream(sqlFilePath);
	        IOUtils.write(file.getBytes(), fs);
	        IOUtils.closeQuietly(fs);
	        projectSqlUtils.importSql(sqlFilePath);
	        //重置任务更新状态
	        taskService.updateAllTaskUpdateStatus();
		} catch (Exception e) {
			log.info(e.getMessage());
			throw new ServiceException(ResultCode.SQL_IMPORT_FAIL);
		}
		
	}
}
