package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.FullInspectStopPointMapper;
import com.ydrobot.powerplant.model.FullInspectStopPoint;
import com.ydrobot.powerplant.service.FullInspectStopPointService;
import com.ydrobot.powerplant.core.service.AbstractService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * Created by Wyf on 2019/04/12.
 */
@Service
public class FullInspectStopPointServiceImpl extends AbstractService<FullInspectStopPoint> implements FullInspectStopPointService {
    @Resource
    private FullInspectStopPointMapper fullInspectStopPointMapper;

}
