package com.ydrobot.powerplant.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.dao.InspectTypeChooseMapper;
import com.ydrobot.powerplant.model.InspectTypeChoose;
import com.ydrobot.powerplant.model.request.InspectTypeChooseBatchRequest;
import com.ydrobot.powerplant.model.request.InspectTypeChooseRequest;
import com.ydrobot.powerplant.service.InspectTypeChooseService;

/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class InspectTypeChooseServiceImpl extends AbstractService<InspectTypeChoose>
        implements InspectTypeChooseService {
    @Resource
    private InspectTypeChooseMapper inspectTypeChooseMapper;

    @Override
    public List<InspectTypeChoose> listInspectTypeChoose(Integer inspectId) {
        InspectTypeChoose inspectTypeChoose = new InspectTypeChoose();
        inspectTypeChoose.setInspectTypeId(inspectId);
        return inspectTypeChooseMapper.select(inspectTypeChoose);
    }

    @Override
    public void batchUpdate(InspectTypeChooseBatchRequest inspectTypeChooseBatchRequest) {
        InspectTypeChoose record = new InspectTypeChoose();
        record.setInspectTypeId(inspectTypeChooseBatchRequest.getInspectTypeId());
        inspectTypeChooseMapper.delete(record);

        List<InspectTypeChooseRequest> inspectTypeChooseRequests = inspectTypeChooseBatchRequest
                .getInspectTypeChooseRequests();

        for (InspectTypeChooseRequest inspectTypeChooseRequest : inspectTypeChooseRequests) {
            InspectTypeChoose inspectTypeChoose = new InspectTypeChoose();
            inspectTypeChoose.setInspectTypeId(inspectTypeChooseBatchRequest.getInspectTypeId());
            inspectTypeChoose.setChooseId(inspectTypeChooseRequest.getChooseId());
            inspectTypeChoose.setChooseType(inspectTypeChooseRequest.getChooseType());
            inspectTypeChooseMapper.insert(inspectTypeChoose);
        }
    }

}
