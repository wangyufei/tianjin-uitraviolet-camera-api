package com.ydrobot.powerplant.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.dao.InspectTypeMapper;
import com.ydrobot.powerplant.model.InspectType;
import com.ydrobot.powerplant.service.InspectTypeService;

/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class InspectTypeServiceImpl extends AbstractService<InspectType> implements InspectTypeService {
    @Resource
    private InspectTypeMapper inspectTypeMapper;

    @Override
    public List<InspectType> listInspectTypeTree() {
        List<InspectType> inspectTypes = inspectTypeMapper.selectAll();
        inspectTypes = listToTree(inspectTypes);
        return inspectTypes;
    }

    /**
     * 列表转树
     * @param devices
     * @return
     */
    private List<InspectType> listToTree(List<InspectType> inspectTypes) {
        List<InspectType> inspectTypeTree = new ArrayList<InspectType>();

        for (InspectType inspectType : inspectTypes) {
            if (inspectType.getParentId() != null) {

                if (inspectType.getParentId().intValue() == 0) {
                    inspectTypeTree.add(inspectType);
                }

                //循环出子级数据，添加到一级数据
                for (InspectType subInspectType : inspectTypes) {
                    if (subInspectType.getParentId() == inspectType.getId()) {
                        if (inspectType.getChildren() == null) {
                            inspectType.setChildren(new ArrayList<>());
                        }
                        inspectType.getChildren().add(subInspectType);
                    }
                }
            }
        }

        return inspectTypeTree;
    }
}
