package com.ydrobot.powerplant.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.dao.ObjectMapper;
import com.ydrobot.powerplant.dao.PointAlarmHistoryMapper;
import com.ydrobot.powerplant.dao.PointAppraiseHistoryMapper;
import com.ydrobot.powerplant.dao.PointAppraiseMapper;
import com.ydrobot.powerplant.dao.PointHistoryMapper;
import com.ydrobot.powerplant.dao.TaskHistoryMapper;
import com.ydrobot.powerplant.model.PointAppraiseHistory;
import com.ydrobot.powerplant.model.PointHistory;
import com.ydrobot.powerplant.model.TObject;
import com.ydrobot.powerplant.model.TaskHistory;
import com.ydrobot.powerplant.model.dto.ObjectPatrolInfoResult;
import com.ydrobot.powerplant.model.dto.ObjectResult;
import com.ydrobot.powerplant.model.dto.PieResult;
import com.ydrobot.powerplant.model.dto.PointAppraiseResult;
import com.ydrobot.powerplant.service.ObjectService;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;

/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class ObjectServiceImpl extends AbstractService<TObject> implements ObjectService {

	@Value("${commissioning.date}")
	private String commissioningDate;

	@Resource
	private ObjectMapper objectMapper;
	@Resource
	private TaskHistoryMapper taskHistoryMapper;
	@Resource
	private PointHistoryMapper pointHistoryMapper;
	@Resource
	private PointAlarmHistoryMapper pointAlarmHistoryMapper;
	@Resource
	private PointAppraiseMapper pointAppraiseMapper;
	@Resource
	private PointAppraiseHistoryMapper pointAppraiseHistoryMapper;

	@Override
	public List<TObject> listObjectByDeviceId(Integer deviceId) {
		TObject record = new TObject();
		record.setDeviceId(deviceId);
		return objectMapper.select(record);
	}

	@Override
	public ObjectResult getDetail(Integer objectId) {
		return objectMapper.getDetail(objectId);
	}

	@Override
	public ObjectPatrolInfoResult getObjectPatrolInfo(Integer objectId) {
		TObject object = findById(objectId);
		ObjectPatrolInfoResult objectPatrolInfoResult = new ObjectPatrolInfoResult();

		if (object != null) {
			objectPatrolInfoResult.setObjectId(objectId);
			objectPatrolInfoResult.setObjectName(object.getName());

			TaskHistory taskHistory = taskHistoryMapper.getLastTaskHistoryByObjectId(objectId);

			if (taskHistory != null) {

				// 获取某个任务记录的某个对象报警数
				Integer alarmNum = pointAlarmHistoryMapper.getAlarmNumByTaskHistoryIdAndObjectId(taskHistory.getId(),
						objectId);
				Integer objectAppraiseStatus = 0; // 0:正常 1:异常

				// 获取某个任务记录的某个对象的最后巡检时间
				Date patrolTime = pointHistoryMapper.getPatrolTimeByTaskHistory(taskHistory.getId(), objectId);
				objectPatrolInfoResult.setPatrolTime(patrolTime);
				objectPatrolInfoResult.setAlarmNum(alarmNum);

				List<PointAppraiseResult> pointAppraiseResults = pointAppraiseMapper
						.getPointAppraiseByObjectId(objectId);
				if (pointAppraiseResults.size() > 0) {
					objectAppraiseStatus = 1;
				}
				objectPatrolInfoResult.setObjectAppraiseStatus(objectAppraiseStatus);
			}
		}

		return objectPatrolInfoResult;
	}

	@Override
	public List<TObject> listAbnormalAppraiseObject() {
		return objectMapper.getAbnormalAppraiseObject();
	}

	@Override
	public List<TObject> listAlarmObject() {
		return objectMapper.getAlarmObject();
	}

	@Override
	public List<PointHistory> listObjectAlarmPosition(Integer objectId) {
		List<PointHistory> pointHistories = new ArrayList<PointHistory>();
		TaskHistory taskHistory = taskHistoryMapper.getLastTaskHistoryByObjectId(objectId);
		if (taskHistory != null) {
			pointHistories = pointHistoryMapper.getPointHistoryByObjectIdAndTaskHistoryId(taskHistory.getId(),
					objectId);
		}
		return pointHistories;
	}

	@Override
	public List<PieResult> countObjectRunStatus(Integer objectId) {
		List<PieResult> pieResults = new ArrayList<>();
		TObject object = findById(objectId);

		// 获取投运至今的总天数
		Date commissioningStartDate = DateUtil.parse(commissioningDate);
		Integer commissioningAllDate = (int) DateUtil.between(commissioningStartDate, new Date(), DateUnit.DAY);

		Integer normalRunDay = commissioningAllDate;
		Integer abnormalRunDay = 0;

		if (object != null) {
			Integer lastAbnormalDate = 0;
			// 获取最后异常的评价时间到现在的天数
			PointAppraiseHistory pointAppraiseHistory = pointAppraiseHistoryMapper
					.getObjectLastPointAppraiseHistory(object.getId());
			if (pointAppraiseHistory != null && pointAppraiseHistory.getAppraiseTime() != null) {
				lastAbnormalDate = (int) DateUtil.between(pointAppraiseHistory.getAppraiseTime(), new Date(),
						DateUnit.DAY);
			}

			// 计算异常运行天数
			if (object.getAbnormalRunTime() != null) {
				abnormalRunDay = object.getAbnormalRunTime() + lastAbnormalDate;
			} else {
				abnormalRunDay = 0;
			}
			
			normalRunDay = commissioningAllDate - abnormalRunDay;

			if (abnormalRunDay > commissioningAllDate) {
				normalRunDay = 0;
				abnormalRunDay = commissioningAllDate;
			}
		}

		PieResult pieResult1 = new PieResult();
		pieResult1.setName("正常运行时间");
		pieResult1.setValue(normalRunDay);
		pieResults.add(pieResult1);

		PieResult pieResult2 = new PieResult();
		pieResult2.setName("异常运行时间");
		pieResult2.setValue(abnormalRunDay);
		pieResults.add(pieResult2);

		return pieResults;
	}

}
