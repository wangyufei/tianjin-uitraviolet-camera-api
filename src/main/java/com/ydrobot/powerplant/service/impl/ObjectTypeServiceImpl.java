package com.ydrobot.powerplant.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.model.ObjectType;
import com.ydrobot.powerplant.service.ObjectTypeService;

/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class ObjectTypeServiceImpl extends AbstractService<ObjectType> implements ObjectTypeService {

	@Override
	public String getObjectTypeNameById(Integer id) {
		String objectTypeName = "";
		ObjectType objectType = findById(id);

		if (objectType != null) {
			objectTypeName = objectType.getName();
		}

		return objectTypeName;
	}

}
