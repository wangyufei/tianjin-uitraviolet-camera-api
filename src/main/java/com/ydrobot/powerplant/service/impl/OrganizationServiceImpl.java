package com.ydrobot.powerplant.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ydrobot.powerplant.core.exception.ServiceException;
import com.ydrobot.powerplant.core.result.ResultCode;
import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.dao.OrganizationMapper;
import com.ydrobot.powerplant.model.Organization;
import com.ydrobot.powerplant.model.User;
import com.ydrobot.powerplant.model.request.OrganizationRequest;
import com.ydrobot.powerplant.service.OrganizationService;
import com.ydrobot.powerplant.service.UserService;

/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class OrganizationServiceImpl extends AbstractService<Organization> implements OrganizationService {
    @Resource
    private OrganizationMapper organizationMapper;
    @Resource
    private UserService userService;

    @Override
    public List<Organization> listOrganizationTree() {
        List<Organization> organizations = organizationMapper.selectAll();
        organizations = listToTree(0,organizations);
        return organizations;
    }

    @Override
    public void saveOrganization(OrganizationRequest organizationRequest) {
        Organization organization = new Organization();
        BeanUtils.copyProperties(organizationRequest, organization);
        save(organization);
    }

    @Override
    public void updateOrganization(Organization organization, OrganizationRequest organizationRequest) {
        BeanUtils.copyProperties(organizationRequest, organization);
        update(organization);
    }

    @Override
    public void deleteOrganization(Integer id) {
    		List<User> users = userService.listUserByOrganizationId(id);
        if (!users.isEmpty()) {
            throw new ServiceException(ResultCode.ORG_EXIST_USER);
        }
        deleteById(id);
    }

    @Override
	public List<Integer> listSubIdsById(Integer id) {
    		List<Organization> organizations = organizationMapper.selectAll();
        organizations = listToTree(id, organizations);
        List<Integer> organizationIds = new ArrayList<>();
        getOrganizationIds(organizationIds, organizations);
		return organizationIds;
	}

	/**
     * 列表转树
     * @param devices
     * @return
     */
    private List<Organization> listToTree(Integer id,List<Organization> organizations) {
        List<Organization> organizationTree = new ArrayList<Organization>();

        for (Organization organization : organizations) {
            if (organization.getParentId() != null) {
                
            		if (id == 0) {
            			if (organization.getParentId().intValue() == 0) {
                        organizationTree.add(organization);
                    }
				} else {
					if (organization.getId() == id) {
	                    organizationTree.add(organization);
	                }
				}
                
                //循环出子级数据，添加到一级数据
                for (Organization subOrganization : organizations) {
                    if (subOrganization.getParentId() == organization.getId()) {
                        if (organization.getChildren() == null) {
                            organization.setChildren(new ArrayList<>());
                        }
                        organization.getChildren().add(subOrganization);
                    }
                }
            }
        }

        return organizationTree;
    }
    
    /**
     * 获取某个组织下id集合
     * @param organizationIds
     * @param tree
     */
    private void getOrganizationIds(List<Integer> organizationIds, List<Organization> tree) {
        if (!tree.isEmpty()) {
            for (Organization organization : tree) {
                organizationIds.add(organization.getId());
                if (organization.getChildren() != null) {
                    getOrganizationIds(organizationIds, organization.getChildren());
                }
            }
        }
    }
}
