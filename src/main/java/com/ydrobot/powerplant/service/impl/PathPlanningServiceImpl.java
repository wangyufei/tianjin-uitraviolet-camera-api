package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.PathPlanningMapper;
import com.ydrobot.powerplant.model.PathPlanning;
import com.ydrobot.powerplant.service.PathPlanningService;
import com.ydrobot.powerplant.core.service.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by Wyf on 2019/05/17.
 */
@Service
@Transactional
public class PathPlanningServiceImpl extends AbstractService<PathPlanning> implements PathPlanningService {
    @Resource
    private PathPlanningMapper pathPlanningMapper;

}
