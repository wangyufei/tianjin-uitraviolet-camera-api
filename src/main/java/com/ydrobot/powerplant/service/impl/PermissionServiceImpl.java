package com.ydrobot.powerplant.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.dao.PermissionMapper;
import com.ydrobot.powerplant.model.Permission;
import com.ydrobot.powerplant.model.condition.PermissionQueryParam;
import com.ydrobot.powerplant.model.request.PermissionRequest;
import com.ydrobot.powerplant.service.PermissionService;

/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class PermissionServiceImpl extends AbstractService<Permission> implements PermissionService {
    @Resource
    private PermissionMapper permissionMapper;

    @Override
    public void savePermission(PermissionRequest permissionRequest) {
        Permission permission = new Permission();
        BeanUtils.copyProperties(permissionRequest, permission);
        save(permission);
    }

    @Override
    public void updatePermission(Permission permission, PermissionRequest permissionRequest) {
        BeanUtils.copyProperties(permissionRequest, permission);
        update(permission);
    }

    @Override
	public List<Permission> listPermission(PermissionQueryParam queryParam, Integer page, Integer size) {
    		PageHelper.startPage(page, size);
        return permissionMapper.getList(queryParam);
	}

	@Override
    public List<Permission> listPermissionTree() {
        List<Permission> permissions = permissionMapper.selectAll();
        permissions = listToTree(permissions);
        return permissions;
    }
	
    /**
     * 列表转树
     * @param devices
     * @return
     */
    private List<Permission> listToTree(List<Permission> permissions) {
        List<Permission> permissionTree = new ArrayList<Permission>();

        for (Permission permission : permissions) {
            if (permission.getParentId() != null) {
                if (permission.getParentId().intValue() == 0) {
                    permissionTree.add(permission);
                }

                //循环出子级数据，添加到一级数据
                for (Permission subPermission : permissions) {
                    if (subPermission.getParentId() == permission.getId()) {
                        if (permission.getChildren() == null) {
                            permission.setChildren(new ArrayList<>());
                        }
                        permission.getChildren().add(subPermission);
                    }
                }
            }
        }

        return permissionTree;
    }

}
