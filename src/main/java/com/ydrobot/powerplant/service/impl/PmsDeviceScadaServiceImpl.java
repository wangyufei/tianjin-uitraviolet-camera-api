package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.PmsDeviceScadaMapper;
import com.ydrobot.powerplant.model.PmsDeviceScada;
import com.ydrobot.powerplant.service.PmsDeviceScadaService;
import com.ydrobot.powerplant.core.service.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by Wyf on 2020/01/10.
 */
@Service
@Transactional
public class PmsDeviceScadaServiceImpl extends AbstractService<PmsDeviceScada> implements PmsDeviceScadaService {
    @Resource
    private PmsDeviceScadaMapper pmsDeviceScadaMapper;

}
