package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.PmsDeviceMapper;
import com.ydrobot.powerplant.model.PmsDevice;
import com.ydrobot.powerplant.service.PmsDeviceService;
import com.ydrobot.powerplant.core.service.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by Wyf on 2020/07/06.
 */
@Service
@Transactional
public class PmsDeviceServiceImpl extends AbstractService<PmsDevice> implements PmsDeviceService {
    @Resource
    private PmsDeviceMapper pmsDeviceMapper;

}
