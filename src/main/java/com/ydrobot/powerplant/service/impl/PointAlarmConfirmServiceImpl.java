package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.PointAlarmConfirmMapper;
import com.ydrobot.powerplant.model.PointAlarmConfirm;
import com.ydrobot.powerplant.service.PointAlarmConfirmService;
import com.ydrobot.powerplant.core.service.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by Wyf on 2020/06/13.
 */
@Service
@Transactional
public class PointAlarmConfirmServiceImpl extends AbstractService<PointAlarmConfirm> implements PointAlarmConfirmService {
    @Resource
    private PointAlarmConfirmMapper pointAlarmConfirmMapper;

}
