package com.ydrobot.powerplant.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.ydrobot.powerplant.core.constant.ProjectConsts;
import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.dao.PointAlarmSettingMapper;
import com.ydrobot.powerplant.dao.PointRelationSettingMapper;
import com.ydrobot.powerplant.model.PointAlarmSetting;
import com.ydrobot.powerplant.model.PointRelationSetting;
import com.ydrobot.powerplant.model.condition.PointAlarmSettingQueryParam;
import com.ydrobot.powerplant.model.dto.PointAlarmSettingResult;
import com.ydrobot.powerplant.model.request.PointAlarmSettingRequest;
import com.ydrobot.powerplant.model.request.UpdatePointAlarmSettingRequest;
import com.ydrobot.powerplant.service.PointAlarmSettingService;

/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class PointAlarmSettingServiceImpl extends AbstractService<PointAlarmSetting>
		implements PointAlarmSettingService {
	@Resource
	private PointAlarmSettingMapper pointAlarmSettingMapper;
	@Resource
	private PointRelationSettingMapper pointRelationSettingMapper;

	@Override
	public void batchUpdate(UpdatePointAlarmSettingRequest updatePointAlarmSettingRequest) {
		List<PointAlarmSettingRequest> pointAlarmSettingRequests = updatePointAlarmSettingRequest
				.getPointAlarmSettingRequests();

		PointAlarmSetting record = new PointAlarmSetting();
		record.setPointId(updatePointAlarmSettingRequest.getPointId());
		pointAlarmSettingMapper.delete(record);

		for (PointAlarmSettingRequest pointAlarmSettingRequest : pointAlarmSettingRequests) {
			PointAlarmSetting pointAlarmSetting = new PointAlarmSetting();
			BeanUtils.copyProperties(pointAlarmSettingRequest, pointAlarmSetting);
			pointAlarmSetting.setPointId(updatePointAlarmSettingRequest.getPointId());
			save(pointAlarmSetting);
		}
		
		// 同步关联点三相对比设置
		syncReferencePointSetting(updatePointAlarmSettingRequest.getPointId(), ProjectConsts.THREE_PHASE_CONTRAST);
		// 同步三相温差设置
		syncReferencePointSetting(updatePointAlarmSettingRequest.getPointId(), ProjectConsts.THREE_PHASE_TEMP_DIF);
	}

	@Override
	public List<PointAlarmSettingResult> listPointAlarmSetting(PointAlarmSettingQueryParam queryParam, Integer page,
			Integer size) {
		PageHelper.startPage(page, size);
		return pointAlarmSettingMapper.getList(queryParam);
	}

	@Override
	public void syncReferencePointSetting(Integer pointId, Byte type) {
		
		if (type.intValue() == 0 || type.intValue() == 1) {
			Integer alarmType = 0;
			
			// 三相温差
			if (type.intValue() == 0) {
				alarmType = 4;
			}

			// 三相对比
			if (type.intValue() == 1) {
				alarmType = 3;
			}

			List<PointAlarmSetting> pointAlarmSettings = pointAlarmSettingMapper.getTriphaseByPointId(pointId,
					alarmType);

			PointRelationSetting record = new PointRelationSetting();
			record.setPointId(pointId);
			record.setType(type);
			List<PointRelationSetting> pointRelationSettings = pointRelationSettingMapper.select(record);
			List<Integer> pointRelationIds = getReferencePointIds(pointRelationSettings, pointId);

			for (Integer pointRelationId : pointRelationIds) {
				pointAlarmSettingMapper.deleteTriphaseByPointId(pointRelationId, alarmType);

				if (!pointAlarmSettings.isEmpty()) {
					for (PointAlarmSetting pointAlarmSetting : pointAlarmSettings) {
						pointAlarmSetting.setId(null);
						pointAlarmSetting.setPointId(pointRelationId);
					}

					pointAlarmSettingMapper.insertList(pointAlarmSettings);
				}
			}
		}
	}

	/**
	 * 获取关联点集合
	 * 
	 * @param pointRelationSettings
	 * @param pointId
	 * @return
	 */
	private List<Integer> getReferencePointIds(List<PointRelationSetting> pointRelationSettings, Integer pointId) {
		List<Integer> referencePointIds = new ArrayList<>();
		for (PointRelationSetting pointRelationSetting : pointRelationSettings) {
			if (StringUtils.isNotBlank(pointRelationSetting.getDependPoint())) {
				try {
					JSONObject dependPoint = JSONObject.parseObject(pointRelationSetting.getDependPoint());
					@SuppressWarnings("unchecked")
					List<JSONObject> jsonObjects = (List<JSONObject>) dependPoint.get("pointRelationSettingRequests");

					for (JSONObject jsonObject : jsonObjects) {
						if (pointId != jsonObject.getInteger("pointId")
								&& !referencePointIds.contains(jsonObject.getInteger("pointId"))) {
							referencePointIds.add(jsonObject.getInteger("pointId"));
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return referencePointIds;
	}

}
