package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.PointCameraRelationMapper;
import com.ydrobot.powerplant.model.PointCameraRelation;
import com.ydrobot.powerplant.model.condition.PointCameraQueryParam;
import com.ydrobot.powerplant.model.dto.PointCameraResult;
import com.ydrobot.powerplant.service.PointCameraRelationService;
import com.github.pagehelper.PageHelper;
import com.ydrobot.powerplant.core.service.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.annotation.Resource;


/**
 * Created by Wyf on 2020/11/15.
 */
@Service
@Transactional
public class PointCameraRelationServiceImpl extends AbstractService<PointCameraRelation> implements PointCameraRelationService {
    @Resource
    private PointCameraRelationMapper pointCameraRelationMapper;

	@Override
	public List<PointCameraResult> listPointCamera(PointCameraQueryParam queryParam, Integer page, Integer size) {
		PageHelper.startPage(page, size);
		return pointCameraRelationMapper.getList(queryParam);
	}

}
