package com.ydrobot.powerplant.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.core.utils.ProjectCommonUtils;
import com.ydrobot.powerplant.dao.PointRelationSettingMapper;
import com.ydrobot.powerplant.model.PointRelationSetting;
import com.ydrobot.powerplant.model.condition.PointRelationSettingQueryParam;
import com.ydrobot.powerplant.model.condition.PointRelationSettingListQueryParam;
import com.ydrobot.powerplant.model.request.PointRelationSettingBatchRequest;
import com.ydrobot.powerplant.model.request.PointRelationSettingRequest;
import com.ydrobot.powerplant.service.PointAlarmSettingService;
import com.ydrobot.powerplant.service.PointRelationSettingService;

import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class PointRelationSettingServiceImpl extends AbstractService<PointRelationSetting>
        implements PointRelationSettingService {
    @Resource
    private PointRelationSettingMapper pointRelationSettingMapper;
    @Resource
    private PointAlarmSettingService pointAlarmSettingService;

    @Override
	public List<PointRelationSetting> listPointRelationSetting(PointRelationSettingListQueryParam queryParam) {
        PageHelper.startPage(queryParam.getPage(), queryParam.getSize());
    		Condition condition = new Condition(PointRelationSetting.class);
        Criteria criteria = condition.createCriteria();

        if (queryParam.getPointIds() != null) {
            List<Integer> pointIds = ProjectCommonUtils.getSplitValInt(queryParam.getPointIds(),
                    ",");
            criteria.andIn("pointId", pointIds);
        }

        if (queryParam.getType() != null) {
            criteria.andEqualTo("type", queryParam.getType());
        }
        
        return findByCondition(condition);
	}

	@Override
    public void updatePointRelationSetting(PointRelationSettingBatchRequest pointRelationSettingBatchRequest) {

        List<PointRelationSettingRequest> pointRelationSettingRequests = pointRelationSettingBatchRequest
                .getPointRelationSettingRequests();

        for (PointRelationSettingRequest pointRelationSettingRequest : pointRelationSettingRequests) {
            deletePointRelation(pointRelationSettingRequest.getPointId(), pointRelationSettingRequest.getType());

            if (StringUtils.isNotBlank(pointRelationSettingRequest.getRelationPointIds())) {
                List<Integer> relationPointIds = ProjectCommonUtils
                        .getSplitValInt(pointRelationSettingRequest.getRelationPointIds(), ",");
                for (Integer relationPointId : relationPointIds) {
                    PointRelationSetting pointRelationSetting = new PointRelationSetting();
                    pointRelationSetting.setPointId(pointRelationSettingRequest.getPointId());
                    pointRelationSetting.setReferencePointId(relationPointId);
                    pointRelationSetting.setType(pointRelationSettingRequest.getType());
                    pointRelationSetting.setDependPoint(pointRelationSettingRequest.getDependPoint());
                    save(pointRelationSetting);
                }
            }

            pointAlarmSettingService.syncReferencePointSetting(pointRelationSettingRequest.getPointId(),
                    pointRelationSettingRequest.getType());
        }
    }

    @Override
    public List<Map<String, Object>> listRelationPoint(PointRelationSettingQueryParam queryParam) {
        PointRelationSetting record = new PointRelationSetting();
        record.setPointId(queryParam.getPointId());
        record.setType(queryParam.getType());
        List<PointRelationSetting> pointRelationSettings = pointRelationSettingMapper.select(record);

        String dependPoint = "";

        if (!pointRelationSettings.isEmpty()) {
            PointRelationSetting pointRelationSetting = pointRelationSettings.get(0);
            dependPoint = pointRelationSetting.getDependPoint();
        }

        List<Integer> pointIds = new ArrayList<>();

        if (!pointRelationSettings.isEmpty()) {
            pointIds.add(queryParam.getPointId());
            for (PointRelationSetting pointRelationSetting : pointRelationSettings) {
                pointIds.add(pointRelationSetting.getReferencePointId());
            }
        }
        return getOtherRelation(pointIds, queryParam.getType(), dependPoint);
    }

    private List<Map<String, Object>> getOtherRelation(List<Integer> pointIds, Byte type, String dependPoint) {
        List<Map<String, Object>> maps = new ArrayList<>();
        for (Integer pointId : pointIds) {
            PointRelationSetting record = new PointRelationSetting();
            record.setPointId(pointId);
            record.setType(type);
            List<PointRelationSetting> pointRelationSettings = pointRelationSettingMapper.select(record);

            if (!pointRelationSettings.isEmpty()) {
                Map<String, Object> map = new HashMap<>();
                map.put("pointId", pointId);
                map.put("type", type);

                String relationIds = "";
                for (PointRelationSetting pointRelationSetting : pointRelationSettings) {
                    relationIds += pointRelationSetting.getReferencePointId() + ",";
                }

                map.put("relationIds", ProjectCommonUtils.removeExtraComma(relationIds));
                map.put("dependPoint", dependPoint);
                maps.add(map);
            }

        }

        return maps;
    }

    /**
     * 删除点关系
     * @param pointId
     * @param type
     */
    private void deletePointRelation(Integer pointId, Byte type) {
        PointRelationSetting record = new PointRelationSetting();
        record.setPointId(pointId);
        record.setType(type);
        List<PointRelationSetting> pointRelationSettings = pointRelationSettingMapper
                .select(record);

        for (PointRelationSetting pointRelationSetting : pointRelationSettings) {
            PointRelationSetting relationSettingCondition = new PointRelationSetting();
            relationSettingCondition.setPointId(pointRelationSetting.getReferencePointId());
            relationSettingCondition.setType(type);
            pointRelationSettingMapper.delete(relationSettingCondition);
        }

        pointRelationSettingMapper.delete(record);
    }
}
