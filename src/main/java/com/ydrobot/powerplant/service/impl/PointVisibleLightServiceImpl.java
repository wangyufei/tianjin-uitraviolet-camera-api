package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.PointVisibleLightMapper;
import com.ydrobot.powerplant.model.PointVisibleLight;
import com.ydrobot.powerplant.service.PointVisibleLightService;
import com.ydrobot.powerplant.core.service.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class PointVisibleLightServiceImpl extends AbstractService<PointVisibleLight> implements PointVisibleLightService {
    @Resource
    private PointVisibleLightMapper pointVisibleLightMapper;

}
