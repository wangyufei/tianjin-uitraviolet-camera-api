package com.ydrobot.powerplant.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.dao.ReconModelSubImgMapper;
import com.ydrobot.powerplant.model.ReconModelSubImg;
import com.ydrobot.powerplant.model.condition.ReconModelSubImgQueryParam;
import com.ydrobot.powerplant.model.request.ReconModelSubImgRequest;
import com.ydrobot.powerplant.service.ReconModelSubImgService;

import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * Created by Wyf on 2019/05/27.
 */
@Service
@Transactional
public class ReconModelSubImgServiceImpl extends AbstractService<ReconModelSubImg> implements ReconModelSubImgService {
    @Resource
    private ReconModelSubImgMapper reconModelSubImgMapper;

    @Override
    public List<ReconModelSubImg> listReconModelSubImg(ReconModelSubImgQueryParam queryParam,Integer page,Integer size) {
    		PageHelper.startPage(page, size);
        Condition condition = new Condition(ReconModelSubImg.class);
        Criteria criteria = condition.createCriteria();

        if (queryParam.getReconModelSubId() != null) {
            criteria.andEqualTo("reconModelSubId", queryParam.getReconModelSubId());
        }

        return findByCondition(condition);
    }

    @Override
    public void saveReconModelSubImg(ReconModelSubImgRequest reconModelSubImgRequest) {
        ReconModelSubImg reconModelSubImg = new ReconModelSubImg();
        BeanUtils.copyProperties(reconModelSubImgRequest, reconModelSubImg);
        save(reconModelSubImg);
    }

    @Override
    public void updateReconModelSubImg(ReconModelSubImg reconModelSubImg,
            ReconModelSubImgRequest reconModelSubImgRequest) {
        BeanUtils.copyProperties(reconModelSubImgRequest, reconModelSubImg);
        update(reconModelSubImg);
    }
}
