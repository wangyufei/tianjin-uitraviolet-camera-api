package com.ydrobot.powerplant.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.dao.ReconModelSubMapper;
import com.ydrobot.powerplant.model.ReconModelSub;
import com.ydrobot.powerplant.model.condition.ReconModelSubQueryParam;
import com.ydrobot.powerplant.model.request.ReconModelSubRequest;
import com.ydrobot.powerplant.service.ReconModelSubService;

import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * Created by Wyf on 2019/05/27.
 */
@Service
@Transactional
public class ReconModelSubServiceImpl extends AbstractService<ReconModelSub> implements ReconModelSubService {
	@Resource
	private ReconModelSubMapper reconModelSubMapper;

	@Override
	public List<ReconModelSub> listReconModelSub(ReconModelSubQueryParam queryParam, Integer page, Integer size) {
		PageHelper.startPage(page, size);
		
		Condition condition = new Condition(ReconModelSub.class);
		Criteria criteria = condition.createCriteria();

		if (queryParam.getReconModelId() != null) {
			criteria.andEqualTo("reconModelId", queryParam.getReconModelId());
		}

		return findByCondition(condition);
	}

	@Override
	public void saveReconModelSub(ReconModelSubRequest reconModelSubRequest) {
		ReconModelSub reconModelSub = new ReconModelSub();
		BeanUtils.copyProperties(reconModelSubRequest, reconModelSub);
		save(reconModelSub);
	}

	@Override
	public void updateReconModelSub(ReconModelSub reconModelSub, ReconModelSubRequest reconModelSubRequest) {
		BeanUtils.copyProperties(reconModelSubRequest, reconModelSub);
		update(reconModelSub);
	}

}
