package com.ydrobot.powerplant.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.dao.RobotParamMapper;
import com.ydrobot.powerplant.model.RobotParam;
import com.ydrobot.powerplant.model.condition.RobotParamQueryParam;
import com.ydrobot.powerplant.model.request.RobotParamBatchRequest;
import com.ydrobot.powerplant.model.request.RobotParamRequest;
import com.ydrobot.powerplant.service.RobotParamService;

import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class RobotParamServiceImpl extends AbstractService<RobotParam> implements RobotParamService {
	@Resource
	private RobotParamMapper robotParamMapper;

	@Override
	public List<RobotParam> listRobotParam(RobotParamQueryParam queryParam, Integer page, Integer size) {
		PageHelper.startPage(page, size);
		Condition condition = new Condition(RobotParam.class);
		Criteria criteria = condition.createCriteria();

		if (queryParam.getRobotId() != null) {
			criteria.andEqualTo("robotId", queryParam.getRobotId());
		}

		if (queryParam.getType() != null) {
			criteria.andEqualTo("type", queryParam.getType());
		}

		if (queryParam.getName() != null) {
			criteria.andEqualTo("name", queryParam.getName());
		}

		return findByCondition(condition);
	}

	@Override
	public void batchUpdate(RobotParamBatchRequest robotParamBatchRequest) {
		RobotParam robotParamCondition = new RobotParam();
		robotParamCondition.setRobotId(robotParamBatchRequest.getRobotId());
		robotParamCondition.setType(robotParamBatchRequest.getType());
		robotParamMapper.delete(robotParamCondition);

		List<RobotParamRequest> robotParamRequests = robotParamBatchRequest.getRobotParamRequests();
		for (RobotParamRequest robotParamRequest : robotParamRequests) {
			RobotParam robotParam = new RobotParam();
			robotParam.setType(robotParamBatchRequest.getType());
			robotParam.setName(robotParamRequest.getName());
			robotParam.setDisplayName(robotParamRequest.getDisplayName());
			robotParam.setRobotId(robotParamBatchRequest.getRobotId());
			robotParam.setValue(robotParamRequest.getValue());
			save(robotParam);
		}
	}

}
