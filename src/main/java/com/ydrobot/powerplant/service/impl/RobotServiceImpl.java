package com.ydrobot.powerplant.service.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.ydrobot.powerplant.common.ThreadCache;
import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.dao.ObjectMapper;
import com.ydrobot.powerplant.dao.PathPlanningMapper;
import com.ydrobot.powerplant.dao.PointAlarmHistoryMapper;
import com.ydrobot.powerplant.dao.PointHistoryMapper;
import com.ydrobot.powerplant.dao.PointMapper;
import com.ydrobot.powerplant.dao.RobotMapper;
import com.ydrobot.powerplant.dao.TaskHistoryMapper;
import com.ydrobot.powerplant.dao.TaskMapper;
import com.ydrobot.powerplant.dao.TaskPointMapper;
import com.ydrobot.powerplant.model.PathPlanning;
import com.ydrobot.powerplant.model.Point;
import com.ydrobot.powerplant.model.PointHistory;
import com.ydrobot.powerplant.model.Robot;
import com.ydrobot.powerplant.model.Task;
import com.ydrobot.powerplant.model.TaskPoint;
import com.ydrobot.powerplant.model.dto.CurrentTaskInfoResult;
import com.ydrobot.powerplant.model.dto.RobotResult;
import com.ydrobot.powerplant.model.request.ControlAuthRequest;
import com.ydrobot.powerplant.service.RobotService;
import com.ydrobot.powerplant.service.TaskService;
import com.ydrobot.powerplant.websocket.WebSocketServer;

/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class RobotServiceImpl extends AbstractService<Robot> implements RobotService {
	@Resource
	private RobotMapper robotMapper;
	@Resource
	private TaskHistoryMapper taskHistoryMapper;
	@Resource
	private TaskPointMapper taskPointMapper;
	@Resource
	private PointAlarmHistoryMapper pointAlarmHistoryMapper;
	@Resource
	private PointHistoryMapper pointHistoryMapper;
	@Resource
	private TaskMapper taskMapper;
	@Resource
	private PointMapper pointMapper;
	@Resource
	private TaskService taskService;
	@Resource
	private PathPlanningMapper pathPlanningMapper;
	@Resource
	private ObjectMapper objectMapper;

	@Override
	public CurrentTaskInfoResult getRobotCurrentTask(Integer robotId) {
		CurrentTaskInfoResult currentTaskInfoResult = taskHistoryMapper.getRobotCurrentExecutTask(robotId);

		// 判断机器人当前是否有任务在执行 type->0:没有正在执行的任务，1:有正在执行的任务
		if (currentTaskInfoResult == null) {
			currentTaskInfoResult = new CurrentTaskInfoResult();
			currentTaskInfoResult.setType(0);
		} else {
			Task task = taskMapper.selectByPrimaryKey(currentTaskInfoResult.getTaskId());
			currentTaskInfoResult.setPointTotal(getPointTotalNum(currentTaskInfoResult.getTaskId()));
			currentTaskInfoResult.setAbnormalPointNum(
					pointAlarmHistoryMapper.getAbnormalPointNum(currentTaskInfoResult.getTaskHistoryId()));
			currentTaskInfoResult.setPassPointNum(pointHistoryMapper.getPassPointNum(currentTaskInfoResult.getTaskHistoryId()));
			currentTaskInfoResult.setCurrentPoint(getCurrentPoint(currentTaskInfoResult.getTaskHistoryId()));
			currentTaskInfoResult
					.setPathPlanning(taskService.getTaskCurrentPath(currentTaskInfoResult.getTaskHistoryId()));
			if (task != null) {
				currentTaskInfoResult.setTaskName(task.getName());
				currentTaskInfoResult.setInspectTypeId(task.getInspectTypeId());
			}
			currentTaskInfoResult.setType(1);
		}

		return currentTaskInfoResult;
	}

	@Override
	public List<RobotResult> listRobot(Integer page, Integer size) {
		PageHelper.startPage(page, size);
		return robotMapper.getList();
	}

	@Override
	public Map<String, Object> getControlAuth(Integer id) {
		Map<String, Object> map = new HashMap<>();
		Integer userId = redisService.get("current-control-auth-" + id, Integer.class);
		if (userId == null || userId == ThreadCache.getUser().getId()) {
			map.put("status", 1);
			redisService.set("current-control-auth-" + id, ThreadCache.getUser().getId());
			redisService.set("current-control-device-" + ThreadCache.getUser().getId(), id);
		} else {

			redisService.set("request-control-auth-" + id, ThreadCache.getUser().getId());
			redisService.set("request-control-device-" + ThreadCache.getUser().getId(), id);
			map.put("status", 0);

			JSONObject jsonObject = new JSONObject();
			jsonObject.put("cmd", "requestAuth");
			try {
				WebSocketServer.sendInfo(JSON.toJSONString(jsonObject), String.valueOf(userId));
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		return map;
	}

	@Override
	public void handleControlAuth(Integer id, ControlAuthRequest controlAuthRequest) {
		Integer reqUserId = redisService.get("request-control-auth-" + id, Integer.class);

		String handleStatus = controlAuthRequest.getHandleStatus();

		if (reqUserId != null) {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("code", handleStatus);
			jsonObject.put("cmd", "responseAuth");
			try {
				WebSocketServer.sendInfo(JSON.toJSONString(jsonObject), String.valueOf(reqUserId));
				redisService.delete("request-control-auth-" + id);

				if ("1".equals(handleStatus)) {
					redisService.set("current-control-auth-" + id, reqUserId);
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public Map<String, Object> checkControlAuth(Integer id) {
		Map<String, Object> map = new HashMap<>();
		map.put("status", 0);
		Integer reqUserId = redisService.get("current-control-auth-" + id, Integer.class);
		if (reqUserId != null && ThreadCache.getUser().getId() == reqUserId) {
			map.put("status", 1);
		}

		return map;
	}

	@Override
	public PathPlanning getTurnBack(Integer id) {
		PathPlanning pathPlanning = pathPlanningMapper.getBackPathByRobotId(id);
		return pathPlanning;
	}
	
	/**
	 * 获取巡检点总数
	 * 
	 * @param taskHistory
	 * @return
	 */
	private int getPointTotalNum(Integer taskId) {
		int pointTotalNum = 0;
		TaskPoint taskPointCondition = new TaskPoint();
		taskPointCondition.setTaskId(taskId);
		pointTotalNum = taskPointMapper.selectCount(taskPointCondition);
		return pointTotalNum;
	}

	/**
	 * 获取当前正在巡检的点位
	 * 
	 * @param taskHistoryId
	 * @return
	 */
	private Point getCurrentPoint(Integer taskHistoryId) {
		Point point = new Point();
		PointHistory pointHistory = pointHistoryMapper.getPointHistoryByTaskHistoryId(taskHistoryId);
		if (pointHistory != null) {
			point = pointMapper.selectByPrimaryKey(pointHistory.getPointId());
		}
		return point;
	}
}
