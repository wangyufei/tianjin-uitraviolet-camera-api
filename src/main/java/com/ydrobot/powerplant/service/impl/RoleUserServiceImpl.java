package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.RoleUserMapper;
import com.ydrobot.powerplant.model.RoleUser;
import com.ydrobot.powerplant.service.RoleUserService;
import com.ydrobot.powerplant.core.service.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class RoleUserServiceImpl extends AbstractService<RoleUser> implements RoleUserService {
    @Resource
    private RoleUserMapper roleUserMapper;

}
