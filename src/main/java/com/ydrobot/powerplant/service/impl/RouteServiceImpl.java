package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.model.Route;
import com.ydrobot.powerplant.model.RoutePos;
import com.ydrobot.powerplant.model.dto.RouteResult;
import com.ydrobot.powerplant.model.dto.RouteWithPosResult;
import com.ydrobot.powerplant.model.request.RouteMaintainRequest;
import com.ydrobot.powerplant.service.RoutePosService;
import com.ydrobot.powerplant.service.RouteService;
import com.ydrobot.powerplant.service.StopPointService;
import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.dao.RouteMapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class RouteServiceImpl extends AbstractService<Route> implements RouteService {

	@Autowired
	private RouteMapper routeMapper;
	@Autowired
	private RoutePosService routePosService;
	@Autowired
	private StopPointService stopPointService;

	@Override
	public List<RouteResult> listRouteWithRoutePos() {
		return routeMapper.getList();
	}

	@Override
	public void updateRoute(Integer id, Route route) {
		route.setId(id);
		update(route);
	}

	@Override
	public void saveRouteMaintain(List<RouteMaintainRequest> routeMaintainRequests) {
		List<RouteWithPosResult> routeWithPosDTOs = new ArrayList<>();

		for (RouteMaintainRequest routeMaintainRequest : routeMaintainRequests) {
			String[] start = getPointArray(routeMaintainRequest.getStart());
			String[] end = getPointArray(routeMaintainRequest.getEnd());
			Integer routeId = routeMaintainRequest.getRouteId();

			RoutePos startPoint = routePosService.getRoutePosByPoint(start);
			RoutePos endPoint = routePosService.getRoutePosByPoint(end);

			// 通过routeId判断该道路是更新还是新增
			if (routeId > 0) {
				Route route = findById(routeMaintainRequest.getRouteId());
				route.setStart(startPoint.getId());
				route.setEnd(endPoint.getId());
				route.setStandard(routeMaintainRequest.getStandard());
				route.setLineWt(routeMaintainRequest.getLineWt());
				update(route);
			} else {
				// 创建道路
				Route route = new Route();
				route.setStart(startPoint.getId());
				route.setEnd(endPoint.getId());
				route.setStandard(routeMaintainRequest.getStandard());
				route.setLineWt(routeMaintainRequest.getLineWt());
				save(route);
				routeId = route.getId();
			}

			RouteWithPosResult routeWithPosDTO = new RouteWithPosResult();
			routeWithPosDTO.setRouteId(routeId);
			routeWithPosDTO.setStartX(Float.valueOf(start[0]));
			routeWithPosDTO.setStartY(Float.valueOf(start[1]));
			routeWithPosDTO.setEndX(Float.valueOf(end[0]));
			routeWithPosDTO.setEndY(Float.valueOf(end[1]));
			routeWithPosDTOs.add(routeWithPosDTO);
		}

		// 更新停靠点道路
		stopPointService.updateStopPoint(routeWithPosDTOs);
	}

	private String[] getPointArray(String pointStr) {
		String[] point = pointStr.split(",");

		for (int i = 0; i < point.length; i++) {
			if (point[i].indexOf(".") != -1) {
				// 获取小数点的位置
				int num = 0;
				num = point[i].indexOf(".");

				// 获取小数点后面的数字 是否有两位 不足两位补足两位
				String pointAfter = point[i].substring(0, num);
				String pointBehind = point[i].substring(num + 1);
				if (pointBehind.length() > 5) {
					pointBehind = pointBehind.substring(0, 4);
				}
				point[i] = pointAfter + "." + pointBehind;
			}
		}

		return point;
	}
}
