package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.StopPointMapper;
import com.ydrobot.powerplant.model.StopPoint;
import com.ydrobot.powerplant.model.dto.RouteWithPosResult;
import com.ydrobot.powerplant.service.StopPointService;
import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.core.utils.ProjectCommonUtils;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.annotation.Resource;

/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class StopPointServiceImpl extends AbstractService<StopPoint> implements StopPointService {
	@Resource
	private StopPointMapper stopPointMapper;

	@Override
	public void updateStopPoint(List<RouteWithPosResult> routeWithPosResults) {
		String routeIds = "";

		for (RouteWithPosResult routeWithPosResult : routeWithPosResults) {
			routeIds += routeWithPosResult.getRouteId() + ",";
		}

		routeIds = ProjectCommonUtils.removeExtraComma(routeIds);
		List<StopPoint> stopPoints = stopPointMapper.getStopPointByRouteId(routeIds);

		for (StopPoint stopPoint : stopPoints) {
			Integer routeId = getStopPointRouteId(stopPoint, routeWithPosResults);
			stopPoint.setRouteId(routeId);
			update(stopPoint);
		}
	}

	@Override
	public void updateStopPoint(Integer sourceRouteId, Integer targetRouteId) {
		StopPoint record = new StopPoint();
		record.setRouteId(sourceRouteId);
		List<StopPoint> stopPoints = stopPointMapper.select(record);

		for (StopPoint stopPoint : stopPoints) {
			stopPoint.setRouteId(targetRouteId);
			update(stopPoint);
		}
	}

	/**
	 * 获取停靠点所在道路
	 * 
	 * @param stopPoint
	 * @param routeWithPosVos
	 * @return
	 */
	private Integer getStopPointRouteId(StopPoint stopPoint, List<RouteWithPosResult> routeWithPosResults) {
		Integer routeId = stopPoint.getRouteId();
		for (RouteWithPosResult routeWithPosResult : routeWithPosResults) {
			// 判断该停靠点是否在线段内
			boolean flag = ProjectCommonUtils.pointToLine(routeWithPosResult.getStartX(),
					routeWithPosResult.getStartY(), routeWithPosResult.getEndX(), routeWithPosResult.getEndY(),
					stopPoint.getX(), stopPoint.getY());
			if (flag) {
				routeId = routeWithPosResult.getRouteId();
				break;
			}
		}

		return routeId;
	}

}
