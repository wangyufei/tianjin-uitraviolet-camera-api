package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.SysAlarmTypeMapper;
import com.ydrobot.powerplant.model.SysAlarmType;
import com.ydrobot.powerplant.service.SysAlarmTypeService;
import com.ydrobot.powerplant.core.service.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by Wyf on 2020/04/30.
 */
@Service
@Transactional
public class SysAlarmTypeServiceImpl extends AbstractService<SysAlarmType> implements SysAlarmTypeService {
    @Resource
    private SysAlarmTypeMapper sysAlarmTypeMapper;

}
