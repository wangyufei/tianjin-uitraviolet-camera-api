package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.SysPointAlarmSettingMapper;
import com.ydrobot.powerplant.model.SysPointAlarmSetting;
import com.ydrobot.powerplant.model.condition.SysPointAlarmSettingQueryParam;
import com.ydrobot.powerplant.service.SysPointAlarmSettingService;

import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;

import com.github.pagehelper.PageHelper;
import com.ydrobot.powerplant.core.service.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.annotation.Resource;

/**
 * Created by Wyf on 2020/04/27.
 */
@Service
@Transactional
public class SysPointAlarmSettingServiceImpl extends AbstractService<SysPointAlarmSetting>
		implements SysPointAlarmSettingService {
	@Resource
	private SysPointAlarmSettingMapper sysPointAlarmSettingMapper;

	@Override
	public List<SysPointAlarmSetting> listSysPointAlarmSetting(SysPointAlarmSettingQueryParam queryParam, Integer page,
			Integer size) {
		PageHelper.startPage(page, size);
		Condition condition = new Condition(SysPointAlarmSetting.class);
		Criteria criteria = condition.createCriteria();

		if (queryParam.getSysPointId() != null) {
			criteria.andEqualTo("sysPointId", queryParam.getSysPointId());
		}

		return findByCondition(condition);
	}
}
