package com.ydrobot.powerplant.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.core.utils.ProjectCommonUtils;
import com.ydrobot.powerplant.dao.SystemSettingMapper;
import com.ydrobot.powerplant.model.SystemSetting;
import com.ydrobot.powerplant.model.request.SystemSettingBatchRequest;
import com.ydrobot.powerplant.model.request.SystemSettingRequest;
import com.ydrobot.powerplant.service.SystemSettingService;

/**
 * Created by Wyf on 2019/06/19.
 */
@Service
@Transactional
public class SystemSettingServiceImpl extends AbstractService<SystemSetting> implements SystemSettingService {
    @Resource
    private SystemSettingMapper systemSettingMapper;

    @Override
    public void batchUpdate(SystemSettingBatchRequest body) {
        String ids = "";

        List<SystemSetting> systemSettings = findAll();
        for (SystemSetting systemSetting : systemSettings) {
            ids += systemSetting.getId() + ",";
        }

        deleteByIds(ProjectCommonUtils.removeExtraComma(ids));

        List<SystemSettingRequest> systemSettingRequests = body.getSystemSettingRequests();
        for (SystemSettingRequest systemSettingRequest : systemSettingRequests) {
            SystemSetting systemSetting = new SystemSetting();
            systemSetting.setName(systemSettingRequest.getName());
            systemSetting.setDisplayName(systemSettingRequest.getDisplayName());
            systemSetting.setValue(systemSettingRequest.getValue());
            save(systemSetting);
        }
    }

}
