package com.ydrobot.powerplant.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.dao.TaskCycleMapper;
import com.ydrobot.powerplant.model.TaskCycle;
import com.ydrobot.powerplant.model.condition.TaskCycleQueryParam;
import com.ydrobot.powerplant.model.request.TaskCycleRequest;
import com.ydrobot.powerplant.service.TaskCycleService;
import com.ydrobot.powerplant.service.TaskService;

import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class TaskCycleServiceImpl extends AbstractService<TaskCycle> implements TaskCycleService {
    @Resource
    private TaskCycleMapper taskCycleMapper;
    @Resource
    private TaskService taskService;
    
    @Override
	public List<TaskCycle> listTaskCycle(TaskCycleQueryParam queryParam, Integer page, Integer size) {
        PageHelper.startPage(page, size);
    		Condition condition = new Condition(TaskCycle.class);
        Criteria criteria = condition.createCriteria();

        if (queryParam.getTaskId() != null) {
            criteria.andEqualTo("taskId", queryParam.getTaskId());
        }

        condition.setOrderByClause("create_time desc");
        return findByCondition(condition);
	}

	@Override
    public void createTaskCycle(TaskCycleRequest taskCycleRequest) {
        TaskCycle taskCycle = new TaskCycle();
        BeanUtils.copyProperties(taskCycleRequest, taskCycle);
        taskCycle.setTaskLevel(taskService.getTaskLevel(taskCycleRequest.getTaskId()));
        save(taskCycle);
    }

    @Override
    public void updateTaskCycle(TaskCycle taskCycle, TaskCycleRequest taskCycleRequest) {
        BeanUtils.copyProperties(taskCycleRequest, taskCycle);
        taskCycle.setTaskLevel(taskService.getTaskLevel(taskCycleRequest.getTaskId()));
        update(taskCycle);
    }

}
