package com.ydrobot.powerplant.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.ydrobot.powerplant.core.constant.CacheConsts;
import com.ydrobot.powerplant.core.constant.ProjectConsts;
import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.core.utils.ProjectCommonUtils;
import com.ydrobot.powerplant.core.utils.ProjectMD5Utils;
import com.ydrobot.powerplant.dao.PermissionRoleMapper;
import com.ydrobot.powerplant.dao.RoleMapper;
import com.ydrobot.powerplant.dao.RoleUserMapper;
import com.ydrobot.powerplant.dao.UserMapper;
import com.ydrobot.powerplant.model.Permission;
import com.ydrobot.powerplant.model.Role;
import com.ydrobot.powerplant.model.RoleUser;
import com.ydrobot.powerplant.model.User;
import com.ydrobot.powerplant.model.condition.UserQueryParam;
import com.ydrobot.powerplant.model.dto.UserResult;
import com.ydrobot.powerplant.model.request.LoginRequest;
import com.ydrobot.powerplant.model.request.UserRequest;
import com.ydrobot.powerplant.service.UserService;
import com.ydrobot.powerplant.service.OrganizationService;

import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class UserServiceImpl extends AbstractService<User> implements UserService {
	@Resource
	private UserMapper userMapper;
	@Resource
	private RoleUserMapper roleUserMapper;
	@Resource
	private RoleMapper roleMapper;
	@Resource
	private PermissionRoleMapper permissionRoleMapper;
	@Resource
	private OrganizationService organizationService;

	@Override
	public List<UserResult> listUser(UserQueryParam queryParam, Integer page, Integer size) {
		List<Integer> organizationIdList = new ArrayList<>();
		if (queryParam.getOrganizationId() != null) {
			organizationIdList = organizationService.listSubIdsById(queryParam.getOrganizationId());
		}
		PageHelper.startPage(page, size);
		return userMapper.getList(queryParam,organizationIdList);
	}

	@Override
	public List<User> listUserByOrganizationId(Integer organizationId) {
		List<User> users = new ArrayList<>();
		Condition condition = new Condition(User.class);
		Criteria criteria = condition.createCriteria();

		List<Integer> organizationIds = organizationService.listSubIdsById(organizationId);
		if (organizationIds.isEmpty()) {
			return users;
		}
		criteria.andIn("organizationId", organizationIds);

		return findByCondition(condition);
	}

	@Override
	public User getUserByAccountAndPasswod(LoginRequest loginRequest) {
		User user = new User();
		user.setAccount(loginRequest.getAccount());
		user.setPassword(ProjectMD5Utils.getMD5String(loginRequest.getPassword()));
		return userMapper.selectOne(user);
	}

	@Override
	public User getUserByAccount(String account) {
		User user = findBy("account", account);
		return user;
	}

	@Override
	public String getToken(User user) {
		String key = ProjectConsts.SECRET + user.getId() + System.currentTimeMillis();
		String token = ProjectMD5Utils.getMD5String(key);
		setRedisUser(user, token);
		return token;
	}

	@Override
	public String getExistToken(User user) {
		String token = redisService.get(CacheConsts.TOKEN_MAP + user.getId());
		if (token != null) {
			setRedisUser(user, token);
		}
		return token;
	}

	@Override
	public void setRedisUser(User user, String token) {
		redisService.set(CacheConsts.USER_TOKEN_PREFIX + token, user, CacheConsts.TOKEN_TIMEOUT);
		redisService.set(CacheConsts.TOKEN_MAP + token, user.getId(), CacheConsts.TOKEN_TIMEOUT);
	}

	@Override
	public void logout(String token) {
		User user = redisService.get(CacheConsts.USER_TOKEN_PREFIX + token, User.class);
		if (user != null) {
			redisService.delete(CacheConsts.USER_TOKEN_PREFIX + token);
			redisService.delete(CacheConsts.TOKEN_MAP + user.getId());
		}
	}

	@Override
	public User getRedisUserByToken(String token) {
		return redisService.get(CacheConsts.USER_TOKEN_PREFIX + token, User.class);
	}

	@Override
	public void saveUser(UserRequest userRequest) {
		User user = new User();
		userRequest.setPassword(ProjectMD5Utils.getMD5String(userRequest.getPassword()));
		BeanUtils.copyProperties(userRequest, user);
		save(user);

		List<Integer> roleIds = ProjectCommonUtils.getSplitValInt(userRequest.getRoleIds(), ",");
		updateUserRole(user.getId(), roleIds);
	}

	@Override
	public void updateUser(User user, UserRequest userRequest) {
		if (userRequest.getPassword() != null && !user.getPassword().equals(userRequest.getPassword())) {
			userRequest.setPassword(ProjectMD5Utils.getMD5String(userRequest.getPassword()));
		}
		BeanUtils.copyProperties(userRequest, user);
		update(user);

		List<Integer> roleIds = ProjectCommonUtils.getSplitValInt(userRequest.getRoleIds(), ",");
		updateUserRole(user.getId(), roleIds);
	}

	/**
	 * 更新用户角色
	 * 
	 * @param userId
	 * @param roleIds
	 */
	private void updateUserRole(Integer userId, List<Integer> roleIds) {
		RoleUser roleUserCondition = new RoleUser();
		roleUserCondition.setUserId(userId);
		roleUserMapper.delete(roleUserCondition);

		for (Integer roleId : roleIds) {
			Role role = roleMapper.selectByPrimaryKey(roleId);
			if (role != null) {
				RoleUser roleUser = new RoleUser();
				roleUser.setRoleId(roleId);
				roleUser.setUserId(userId);
				roleUserMapper.insert(roleUser);
			}
		}
	}

	@Override
	public List<Permission> listUserPermission(User user) {
		return userMapper.getUserPermissions(user.getId());
	}
}
