package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.WatchPointMapper;
import com.ydrobot.powerplant.model.WatchPoint;
import com.ydrobot.powerplant.service.WatchPointService;
import com.ydrobot.powerplant.core.service.AbstractService;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class WatchPointServiceImpl extends AbstractService<WatchPoint> implements WatchPointService {
    @Resource
    private WatchPointMapper watchPointMapper;

	@Override
	public void updateWatchPoint(Integer id, WatchPoint body) {
		WatchPoint watchPoint = new WatchPoint();
		BeanUtils.copyProperties(body, watchPoint);
		watchPoint.setId(id);
		update(watchPoint);
	}
}
