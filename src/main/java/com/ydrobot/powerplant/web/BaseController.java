package com.ydrobot.powerplant.web;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.ydrobot.powerplant.core.constant.ProjectConsts;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.utils.ProjectMD5Utils;

import cn.hutool.http.HttpRequest;

public class BaseController {
	
	

	/**
	 * 解析结果
	 * 
	 * @param jsonObject
	 * @return
	 */
	protected Result getRemoteResult(String res) {
		JSONObject jsonObject = JSONObject.parseObject(res);
		Result result = new Result();
		result.setCode(jsonObject.getIntValue("code"));
		result.setMessage(jsonObject.getString("message"));
		result.setData(jsonObject.get("data"));
		return result;
	}

	/**
	 * 获取Header
	 * 
	 * @return
	 */
	private Map<String, String> getHeader() {
		long currentTime = System.currentTimeMillis();
		String sign = ProjectMD5Utils.getMD5String(ProjectConsts.SECRET + "&&" + currentTime);
		Map<String, String> header = new HashMap<String, String>();
		header.put("sign", sign);
		header.put("timestamp", String.valueOf(currentTime));
		return header;
	}

	/**
	 * 发送get请求
	 * 
	 * @param urlString
	 *            网址
	 * @return 返回内容，如果只检查状态码，正常只返回 ""，不正常返回 null
	 */
	public String get(String urlString) {
		return HttpRequest.get(urlString).addHeaders(getHeader()).execute().body();
	}

	/**
	 * 发送get请求
	 * 
	 * @param urlString
	 *            网址
	 * @param paramMap
	 *            post表单数据
	 * @return 返回数据
	 */
	public String get(String urlString, Map<String, Object> paramMap) {
		return HttpRequest.get(urlString).addHeaders(getHeader()).form(paramMap).execute().body();
	}

	/**
	 * post请求
	 * 
	 * @param urlString
	 * @param body
	 * @return
	 */
	public String post(String urlString, String body) {
		return HttpRequest.post(urlString).addHeaders(getHeader()).body(body).execute().body();
	}
	
	/**
	 * put请求
	 * 
	 * @param urlString
	 * @param body
	 * @return
	 */
	public String put(String urlString, String body) {
		return HttpRequest.put(urlString).addHeaders(getHeader()).body(body).execute().body();
	}
	
	/**
	 * delete请求
	 * 
	 * @param urlString
	 * @return
	 */
	public String delete(String urlString) {
		return HttpRequest.delete(urlString).addHeaders(getHeader()).execute().body();
	}
}
