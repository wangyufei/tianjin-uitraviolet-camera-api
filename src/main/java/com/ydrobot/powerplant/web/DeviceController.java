package com.ydrobot.powerplant.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.Device;
import com.ydrobot.powerplant.model.condition.DeviceIntervalQueryParam;
import com.ydrobot.powerplant.service.DeviceService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
* Created by Wyf on 2019/04/12.
*/
@Api(tags = "设备模块")
@RestController
@RequestMapping("/ui/devices")
public class DeviceController {
    @Resource
    private DeviceService deviceService;

    @ApiOperation("获取设备区域列表")
    @GetMapping("/areas")
    public Result listArea() {
        List<Device> devices = deviceService.listArea();
        return ResultGenerator.genSuccessResult(devices);
    }

    @ApiOperation("获取某个区域的间隔列表")
    @GetMapping("/intervals")
    public Result listInterval(DeviceIntervalQueryParam queryParam) {
        List<Device> devices = deviceService.listInterval(queryParam);
        return ResultGenerator.genSuccessResult(devices);
    }

    @ApiOperation("获取某个间隔的设备列表")
    @GetMapping("/interval-devices")
    public Result listDevicesByIntervalId(@RequestParam(required = true) Integer intervalId) {
        List<Device> devices = deviceService.listDevicesByIntervalId(intervalId);
        return ResultGenerator.genSuccessResult(devices);
    }
    
    @ApiOperation("获取设备树")
    @GetMapping("/tree")
    public Result getDeviceTree() {
    		List<Device> devices = deviceService.getDeviceTree();
    		return ResultGenerator.genSuccessResult(devices);
    }
}
