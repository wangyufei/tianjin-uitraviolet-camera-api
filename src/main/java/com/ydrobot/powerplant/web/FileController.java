package com.ydrobot.powerplant.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ydrobot.powerplant.core.constant.ProjectConsts;
import com.ydrobot.powerplant.core.exception.ServiceException;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultCode;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.service.FileService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "文件上传模块")
@RestController
@RequestMapping("/ui/files")
public class FileController {

    @Value("${file.max}")
    private String fileMax;

    @Value("${file.exts}")
    private String fileExts;

    @Value("${web.domain}")
    private String domain;

    public Set<String> exts = new HashSet<String>();

    @Resource
    private FileService fileService;

    @ApiOperation("文件上传")
    @PostMapping()
    public Result uploadFile(@RequestParam("file") MultipartFile file) throws IOException, ServiceException {

        long max = Long.parseLong(fileMax) * 1024 * 1024;

        String dar[] = fileExts.split(",");
        for (String ext : dar) {
            exts.add(ext);
        }

        String ext = getExtName(file.getOriginalFilename());
        if (ext == null || !exts.contains(ext)) {
            throw new ServiceException(ResultCode.FILE_EXTS);
        }
        long realFileSize = file.getSize();
        if (max < realFileSize) {
            throw new ServiceException(ResultCode.FILE_MAX);
        }
        String fileId = fileService.uploadFile(file, ext);
        Map<String, String> map = new HashMap<String, String>();
        map.put("localUrl", domain + ProjectConsts.PREFIX_FILE + "/" + fileId);
        return ResultGenerator.genSuccessResult(map);
    }
    
    @ApiOperation("文件上传")
    @PostMapping("import-sql")
    public Result importSql(@RequestParam("file") MultipartFile file) {
    		
    		String ext = getExtName(file.getOriginalFilename());
        if (ext == null || !ext.contains("sql")) {
            throw new ServiceException(ResultCode.FILE_EXTS);
        }
    	
    		fileService.importSql(file);
        return ResultGenerator.genSuccessResult();
    }

    private String getExtName(String fileName) {
        int pos = fileName.lastIndexOf(".");
        String ext = null;
        if (pos > 0) {
            ext = fileName.substring(pos);
        }
        if (ext == null) {
            return null;
        }
        return ext.toLowerCase();
    }
}
