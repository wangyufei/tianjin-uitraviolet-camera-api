package com.ydrobot.powerplant.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.FullInspectStopPoint;
import com.ydrobot.powerplant.service.FullInspectStopPointService;

import io.swagger.annotations.Api;

/**
* Created by Wyf on 2019/04/12.
*/
@Api(tags = "全面巡检停靠点模块")
@RestController
@RequestMapping("/ui/full-inspect-stop-points")
public class FullInspectStopPointController {
    @Resource
    private FullInspectStopPointService fullInspectStopPointService;

    @GetMapping
    public Result list(@RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer size) {
        PageHelper.startPage(page, size);
        List<FullInspectStopPoint> fullInspectStopPoints = fullInspectStopPointService.findAll();
        PageInfo<FullInspectStopPoint> pageInfo = new PageInfo<>(fullInspectStopPoints);
        return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
    }
}
