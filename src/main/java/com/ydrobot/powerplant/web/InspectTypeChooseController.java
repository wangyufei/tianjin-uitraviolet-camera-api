package com.ydrobot.powerplant.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.InspectTypeChoose;
import com.ydrobot.powerplant.model.request.InspectTypeChooseBatchRequest;
import com.ydrobot.powerplant.service.InspectTypeChooseService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

/**
* Created by Wyf on 2019/04/12.
*/
@Api(tags = "巡检类型默认配置模块")
@RestController
@RequestMapping("/ui/inspect-type-chooses")
public class InspectTypeChooseController {
    @Resource
    private InspectTypeChooseService inspectTypeChooseService;

    @ApiOperation("获取巡检点默认选项")
    @ApiImplicitParam(name = "inspectId", value = "巡检类型id", dataType = "int", paramType = "query", required = true)
    @GetMapping("/default-checks")
    public Result listInspectTypeChoose(@RequestParam(required = true) Integer inspectId) {
        List<InspectTypeChoose> inspectTypeChooses = inspectTypeChooseService.listInspectTypeChoose(inspectId);
        return ResultGenerator.genSuccessResult(inspectTypeChooses);
    }

    @ApiOperation("批量更新")
    @PutMapping("/batch")
    public Result batchUpdate(@RequestBody InspectTypeChooseBatchRequest inspectTypeChooseBatchRequest) {
        inspectTypeChooseService.batchUpdate(inspectTypeChooseBatchRequest);
        return ResultGenerator.genSuccessResult();
    }
}
