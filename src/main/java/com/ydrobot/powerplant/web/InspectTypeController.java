package com.ydrobot.powerplant.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.InspectType;
import com.ydrobot.powerplant.service.InspectTypeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
* Created by Wyf on 2019/04/12.
*/
@Api(tags = "巡检类型模块")
@RestController
@RequestMapping("/ui/inspect-types")
public class InspectTypeController {
    @Resource
    private InspectTypeService inspectTypeService;

    /**
     * 获取巡检类型列表
     * @param page
     * @param size
     * @return
     */
    @GetMapping
    public Result list(@RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer size) {
        PageHelper.startPage(page, size);
        List<InspectType> inspectTypes = inspectTypeService.findAll();
        PageInfo<InspectType> pageInfo = new PageInfo<>(inspectTypes);
        return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
    }

    @ApiOperation("获取巡检类型树")
    @GetMapping("/tree")
    public Result listInspectTypeTree() {
        List<InspectType> inspectTypes = inspectTypeService.listInspectTypeTree();
        return ResultGenerator.genSuccessResult(inspectTypes);
    }
}
