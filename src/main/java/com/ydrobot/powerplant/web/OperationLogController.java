package com.ydrobot.powerplant.web;

import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.condition.OperationLogQueryParam;
import com.ydrobot.powerplant.model.dto.OperationLogResult;
import com.ydrobot.powerplant.model.request.OperationLogRequest;
import com.ydrobot.powerplant.service.OperationLogService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
* Created by Wyf on 2019/04/30.
*/
@Api(tags = "操作日志模块")
@RestController
@RequestMapping("/ui/operation-logs")
public class OperationLogController {
    @Resource
    private OperationLogService operationLogService;

    @ApiOperation("添加操作日志")
    @PostMapping
    public Result add(@Valid @RequestBody OperationLogRequest body) {
        operationLogService.saveOperationLog(body);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("删除操作日志")
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id) {
        operationLogService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("获取操作日志列表")
    @GetMapping
    public Result listOperationLog(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer size,
            OperationLogQueryParam queryParam) {
        List<OperationLogResult> operationLogResults = operationLogService.listOperationLog(queryParam, page, size);
        PageInfo<OperationLogResult> pageInfo = new PageInfo<>(operationLogResults);
        return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
    }
}
