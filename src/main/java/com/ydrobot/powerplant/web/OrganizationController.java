package com.ydrobot.powerplant.web;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ydrobot.powerplant.core.exception.ServiceException;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultCode;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.core.utils.ProjectObjectUtils;
import com.ydrobot.powerplant.model.Organization;
import com.ydrobot.powerplant.model.request.OrganizationRequest;
import com.ydrobot.powerplant.service.OrganizationService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
* Created by Wyf on 2019/04/12.
*/
@Api(tags = "组织机构模块")
@RestController
@RequestMapping("/ui/organizations")
public class OrganizationController {
    @Resource
    private OrganizationService organizationService;

    @ApiOperation("添加")
    @PostMapping
    public Result add(@Valid @RequestBody OrganizationRequest body) {
        organizationService.saveOrganization(body);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("删除")
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id) throws ServiceException {
        organizationService.deleteOrganization(id);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("更新")
    @PutMapping("/{id}")
    public Result update(@PathVariable Integer id, @RequestBody OrganizationRequest body){
        Organization organization = organizationService.findById(id);

        if (organization == null) {
            throw new ServiceException(ResultCode.DATA_NOT_FOUND);
        }

        organizationService.updateOrganization(organization, body);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("详情")
    @GetMapping("/{id}")
    public Result detail(@PathVariable Integer id) {
        Organization organization = organizationService.findById(id);
        Map<String, Object> map = ProjectObjectUtils.objectToMap(organization);
        map.put("parent", organizationService.findById(organization.getParentId()));
        return ResultGenerator.genSuccessResult(map);
    }

    @ApiOperation("获取组织机构树")
    @GetMapping("/tree")
    public Result listOrganizationTree() {
        List<Organization> organizations = organizationService.listOrganizationTree();
        return ResultGenerator.genSuccessResult(organizations);
    }
}
