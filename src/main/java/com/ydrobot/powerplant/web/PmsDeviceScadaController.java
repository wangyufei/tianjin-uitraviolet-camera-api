package com.ydrobot.powerplant.web;

import javax.annotation.Resource;

import org.apache.ibatis.exceptions.TooManyResultsException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ydrobot.powerplant.core.exception.ServiceException;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.PmsDeviceScada;
import com.ydrobot.powerplant.service.PmsDeviceScadaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
* Created by Wyf on 2020/01/10.
*/
@Api(tags = "PMS设备scada模块")
@RestController
@RequestMapping("/ui/pms-device-scadas")
public class PmsDeviceScadaController {
    @Resource
    private PmsDeviceScadaService pmsDeviceScadaService;

    @ApiOperation("通过对象id获取pms设备scada详情")
    @GetMapping("/objectid/{objectId}")
    public Result detail(@PathVariable String objectId) throws TooManyResultsException, ServiceException {
        PmsDeviceScada pmsDeviceScada = pmsDeviceScadaService.findBy("objectId", objectId);
        return ResultGenerator.genSuccessResult(pmsDeviceScada);
    }
}
