package com.ydrobot.powerplant.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.exception.ServiceException;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultCode;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.PointAlarmHistory;
import com.ydrobot.powerplant.model.Task;
import com.ydrobot.powerplant.model.condition.PointAlarmHistoryQueryParam;
import com.ydrobot.powerplant.model.dto.PointAlarmHistoryResult;
import com.ydrobot.powerplant.model.request.PointAlarmHistoryBatchRequest;
import com.ydrobot.powerplant.model.request.PointAlarmHistoryRequest;
import com.ydrobot.powerplant.service.PointAlarmHistoryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Created by Wyf on 2019/04/12.
 */
@Api(tags = "点位告警记录模块")
@RestController
@RequestMapping("/ui/point-alarm-historys")
public class PointAlarmHistoryController {
	@Resource
	private PointAlarmHistoryService pointAlarmHistoryService;

	@ApiOperation("删除点位告警记录")
	@DeleteMapping("/{id}")
	public Result delete(@PathVariable Integer id) {
		pointAlarmHistoryService.deleteById(id);
		return ResultGenerator.genSuccessResult();
	}

	@ApiOperation("更新点位告警信息")
	@PutMapping("/{id}")
	public Result update(@PathVariable Integer id, @RequestBody PointAlarmHistoryRequest body) {
		PointAlarmHistory pointAlarmHistory = pointAlarmHistoryService.findById(id);

		if (pointAlarmHistory == null) {
			throw new ServiceException(ResultCode.DATA_NOT_FOUND);
		}

		pointAlarmHistoryService.updatePointAlarmHistory(pointAlarmHistory, body);
		return ResultGenerator.genSuccessResult();
	}

	@ApiOperation("获取点位告警信息详情")
	@GetMapping("/{id}")
	public Result detail(@PathVariable Integer id) {
		PointAlarmHistory pointAlarmHistory = pointAlarmHistoryService.findById(id);
		return ResultGenerator.genSuccessResult(pointAlarmHistory);
	}

	@ApiOperation("获取点位告警信息列表")
	@PostMapping
	public Result list(@RequestBody PointAlarmHistoryQueryParam queryParam) {
		List<PointAlarmHistoryResult> pointAlarmHistoryResults = pointAlarmHistoryService
				.listPointAlarmHistory(queryParam);
		PageInfo<PointAlarmHistoryResult> pageInfo = new PageInfo<>(pointAlarmHistoryResults);
		return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
	}

	@ApiOperation("批量确认")
	@PutMapping("/batch-confirm")
	public Result batchConfirm(@RequestBody PointAlarmHistoryBatchRequest body) {
		pointAlarmHistoryService.batchConfirm(body);
		return ResultGenerator.genSuccessResult();
	}
	
	@ApiOperation("二次确认")
	@GetMapping("/{id}/confirm")
	public Result alarmTwiceConfirm(@PathVariable Integer id) {
		Task task = pointAlarmHistoryService.alarmTwiceConfirm(id);
		return ResultGenerator.genSuccessResult(task);
	}
}
