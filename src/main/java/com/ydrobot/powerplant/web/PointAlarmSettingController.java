package com.ydrobot.powerplant.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.condition.PointAlarmSettingQueryParam;
import com.ydrobot.powerplant.model.dto.PointAlarmSettingResult;
import com.ydrobot.powerplant.model.request.UpdatePointAlarmSettingRequest;
import com.ydrobot.powerplant.service.PointAlarmSettingService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Created by Wyf on 2019/04/12.
 */
@Api(tags = "点位告警设置模块")
@RestController
@RequestMapping("/ui/point-alarm-settings")
public class PointAlarmSettingController {
	@Resource
	private PointAlarmSettingService pointAlarmSettingService;

	@ApiOperation("获取点位告警设置列表")
	@GetMapping
	public Result list(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer size,
			PointAlarmSettingQueryParam queryParam) {
		List<PointAlarmSettingResult> pointAlarmSettings = pointAlarmSettingService.listPointAlarmSetting(queryParam,
				page, size);
		PageInfo<PointAlarmSettingResult> pageInfo = new PageInfo<>(pointAlarmSettings);
		return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
	}

	@ApiOperation("批量更新点位告警设置参数")
	@PutMapping("/batch")
	public Result batchUpdate(@RequestBody UpdatePointAlarmSettingRequest body) {
		pointAlarmSettingService.batchUpdate(body);
		return ResultGenerator.genSuccessResult();
	}
}
