package com.ydrobot.powerplant.web;

import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.PointCameraRelation;
import com.ydrobot.powerplant.model.condition.PointCameraQueryParam;
import com.ydrobot.powerplant.model.dto.PointCameraResult;
import com.ydrobot.powerplant.service.PointCameraRelationService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by Wyf on 2020/11/15.
*/
@Api(tags = "点位和摄像头关系模块")
@RestController
@RequestMapping("/ui/point-camera-relations")
public class PointCameraRelationController {
    @Resource
    private PointCameraRelationService pointCameraRelationService;

    @ApiOperation("添加")
    @PostMapping
    public Result add(@RequestBody PointCameraRelation pointCameraRelation) {
        pointCameraRelationService.save(pointCameraRelation);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("删除")
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id) {
        pointCameraRelationService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("更新")
    @PutMapping("/{id}")
    public Result update(@PathVariable Integer id,@RequestBody PointCameraRelation pointCameraRelation) {
    		pointCameraRelation.setId(id);
        pointCameraRelationService.update(pointCameraRelation);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("获取详情")
    @GetMapping("/{id}")
    public Result detail(@PathVariable Integer id) {
        PointCameraRelation pointCameraRelation = pointCameraRelationService.findById(id);
        return ResultGenerator.genSuccessResult(pointCameraRelation);
    }

    @ApiOperation("列表")
    @GetMapping
    public Result list(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer size,PointCameraQueryParam queryParam) {
    		List<PointCameraResult> list = pointCameraRelationService.listPointCamera(queryParam, page, size);
        PageInfo<PointCameraResult> pageInfo = new PageInfo<PointCameraResult>(list);
        return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
    }
}
