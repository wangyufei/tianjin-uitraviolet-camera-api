package com.ydrobot.powerplant.web;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.exception.ServiceException;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultCode;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.PointHistory;
import com.ydrobot.powerplant.model.Task;
import com.ydrobot.powerplant.model.condition.PointHistoryQueryParam;
import com.ydrobot.powerplant.model.condition.PointHistoryReportQueryParam;
import com.ydrobot.powerplant.model.condition.VerificationReportQueryParam;
import com.ydrobot.powerplant.model.dto.ObjectVerificationReportResult;
import com.ydrobot.powerplant.model.dto.PointHistoryContrastResult;
import com.ydrobot.powerplant.model.dto.PointHistoryCurveResult;
import com.ydrobot.powerplant.model.dto.PointHistoryReportResult;
import com.ydrobot.powerplant.model.dto.PointHistoryResult;
import com.ydrobot.powerplant.model.dto.PointHistoryVerificationReportResult;
import com.ydrobot.powerplant.model.dto.PointVerificationReportResult;
import com.ydrobot.powerplant.model.request.PointHistoryBatchRequest;
import com.ydrobot.powerplant.model.request.PointHistoryRequest;
import com.ydrobot.powerplant.service.PointHistoryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
* Created by Wyf on 2019/04/12.
*/
@Api(tags = "点位历史记录模块")
@RestController
@RequestMapping("/ui/point-historys")
public class PointHistoryController {

    @Resource
    private PointHistoryService pointHistoryService;
    
    @ApiOperation("更新")
    @PutMapping("/{id}")
    public Result update(@PathVariable Integer id, @RequestBody PointHistoryRequest body) {
        PointHistory pointHistory = pointHistoryService.findById(id);
        if (pointHistory == null) {
            throw new ServiceException(ResultCode.DATA_NOT_FOUND);
        }
        pointHistoryService.updatePointHistory(pointHistory, body);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("详情")
    @GetMapping("/{id}")
    public Result detail(@PathVariable Integer id) {
        PointHistory pointHistory = pointHistoryService.findById(id);
        return ResultGenerator.genSuccessResult(pointHistory);
    }

    @ApiOperation("列表")
    @PostMapping
    public Result list(@RequestBody PointHistoryQueryParam queryParam) {
    		List<PointHistoryResult> pointHistoryResults = pointHistoryService.listPointHistory(queryParam);
        PageInfo<PointHistoryResult> pageInfo = new PageInfo<>(pointHistoryResults);
        return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
    }

    @ApiOperation("获取点位历史对比数据")
    @PostMapping("contrast")
    public Result pointHistoryContrast(@RequestBody PointHistoryQueryParam queryParam) {
        List<PointHistoryContrastResult> maps = pointHistoryService.listPointHistoryContrast(queryParam);
        return ResultGenerator.genSuccessResult(maps);
    }

    @ApiOperation("获取关联点历史数据")
    @GetMapping("/relation-point")
    public Result getRelationPointHistory(@RequestParam(required = true) Integer pointId,
            @RequestParam(required = true) Integer taskHistoryId) {
        List<PointHistory> pointHistories = pointHistoryService.listRelationPointHistory(pointId, taskHistoryId);
        List<Map<String, Object>> maps = pointHistoryService.extendRelationPoint(pointHistories);
        return ResultGenerator.genSuccessResult(maps);
    }

    @ApiOperation("批量审核巡检结果")
    @PostMapping("/batch-confirm")
    public Result batchConfirm(@RequestBody PointHistoryBatchRequest body) {
        pointHistoryService.batchConfirm(body);
        return ResultGenerator.genSuccessResult();
    }
    
    @ApiOperation("获取巡检报表")
    @PostMapping("/report")
	public Result getReport(@RequestBody PointHistoryReportQueryParam queryParam) {
		List<PointHistoryReportResult> pointHistoryReportResults = pointHistoryService
				.listPointHistoryReport(queryParam);
		PageInfo<PointHistoryReportResult> pageInfo = new PageInfo<>(pointHistoryReportResults);
		return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
	}
    
    @ApiOperation("获取部件核查报告列表")
    @GetMapping("/point-verification-report")
    public Result getPointVerificationReport(VerificationReportQueryParam queryParam) {
    		List<PointVerificationReportResult> pointVerificationReportResults = pointHistoryService.listPointVerificationReport(queryParam);
    		return ResultGenerator.genSuccessResult(pointVerificationReportResults);
    }
    
    @ApiOperation("获取对象核查报告信息")
    @GetMapping("/object-verification-report")
    public Result getObjectVerificationReport(VerificationReportQueryParam queryParam) {
    		ObjectVerificationReportResult objectVerificationReportResult = pointHistoryService.getObjectVerificationReport(queryParam);
    		return ResultGenerator.genSuccessResult(objectVerificationReportResult);
    }
    
    @ApiOperation("获取某个组件的近一周历史数据")
    @GetMapping("/curve")
    public Result getPointHistoryCurve(@RequestParam(required = true) Integer pointId) {
    		List<PointHistoryCurveResult> pointHistoryCurveResults = pointHistoryService.countPointHistoryCurve(pointId);
    		return ResultGenerator.genSuccessResult(pointHistoryCurveResults);
    }
    
    @ApiOperation("二次确认")
	@GetMapping("/{id}/confirm")
	public Result twiceConfirm(@PathVariable Integer id) {
		Task task = pointHistoryService.twiceConfirm(id);
		return ResultGenerator.genSuccessResult(task);
	}
    
    @ApiOperation("获取某条记录的评价原因")
    @GetMapping("/{id}/appraise-reason")
    public Result getAppraiseReason(@PathVariable Integer id) {
    		List<PointHistoryVerificationReportResult> pointHistoryVerificationReportResults = pointHistoryService.getPointAppraiseReason(id);
    		return ResultGenerator.genSuccessResult(pointHistoryVerificationReportResults);
    }
}
