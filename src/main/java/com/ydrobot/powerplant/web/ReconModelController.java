package com.ydrobot.powerplant.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.dto.ReconModelResult;
import com.ydrobot.powerplant.service.ReconModelService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Created by Wyf on 2019/05/27.
 */
@Api(tags = "识别模型模块")
@RestController
@RequestMapping("/ui/recon-models")
public class ReconModelController {
	@Resource
	private ReconModelService reconModelService;

	@ApiOperation("列表")
	@GetMapping
	public Result list(@RequestParam(defaultValue = "1") Integer page,
			@RequestParam(defaultValue = "10") Integer size) {
		List<ReconModelResult> reconModelResults = reconModelService.listReconModel(page, size);
		PageInfo<ReconModelResult> pageInfo = new PageInfo<>(reconModelResults);
		return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
	}
}
