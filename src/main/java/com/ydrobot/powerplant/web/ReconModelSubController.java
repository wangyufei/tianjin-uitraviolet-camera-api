package com.ydrobot.powerplant.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.exception.ServiceException;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultCode;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.ReconModelSub;
import com.ydrobot.powerplant.model.condition.ReconModelSubQueryParam;
import com.ydrobot.powerplant.model.request.ReconModelSubRequest;
import com.ydrobot.powerplant.service.ReconModelSubService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
* Created by Wyf on 2019/05/27.
*/
@Api(tags = "识别模型子类模块")
@RestController
@RequestMapping("/ui/recon-model-subs")
public class ReconModelSubController {
    @Resource
    private ReconModelSubService reconModelSubService;

    @ApiOperation("添加")
    @PostMapping
    public Result add(@RequestBody ReconModelSubRequest body) {
        reconModelSubService.saveReconModelSub(body);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("删除")
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id) {
        reconModelSubService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("更新")
    @PutMapping("/{id}")
    public Result update(@PathVariable Integer id, @RequestBody ReconModelSubRequest body) {
        ReconModelSub reconModelSub = reconModelSubService.findById(id);

        if (reconModelSub == null) {
            throw new ServiceException(ResultCode.DATA_NOT_FOUND);
        }
        
        reconModelSubService.updateReconModelSub(reconModelSub, body);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("详情")
    @GetMapping("/{id}")
    public Result detail(@PathVariable Integer id) {
        ReconModelSub reconModelSub = reconModelSubService.findById(id);
        return ResultGenerator.genSuccessResult(reconModelSub);
    }

    @ApiOperation("列表")
    @GetMapping
    public Result list(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer size,
            ReconModelSubQueryParam queryParam) {
    		List<ReconModelSub> reconModelSubs = reconModelSubService.listReconModelSub(queryParam, page, size);
        PageInfo<ReconModelSub> pageInfo = new PageInfo<>(reconModelSubs);
        return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
    }
}
