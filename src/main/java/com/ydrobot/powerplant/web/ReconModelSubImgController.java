package com.ydrobot.powerplant.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.exception.ServiceException;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultCode;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.ReconModelSubImg;
import com.ydrobot.powerplant.model.condition.ReconModelSubImgQueryParam;
import com.ydrobot.powerplant.model.request.ReconModelSubImgRequest;
import com.ydrobot.powerplant.service.ReconModelSubImgService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
* Created by Wyf on 2019/05/27.
*/
@Api(tags = "识别模型子类图标模块")
@RestController
@RequestMapping("/ui/recon-model-sub-imgs")
public class ReconModelSubImgController {
    @Resource
    private ReconModelSubImgService reconModelSubImgService;

    @ApiOperation("添加")
    @PostMapping
    public Result add(@RequestBody ReconModelSubImgRequest body) {
        reconModelSubImgService.saveReconModelSubImg(body);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("删除")
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id) {
        reconModelSubImgService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("更新")
    @PutMapping("/{id}")
    public Result update(@PathVariable Integer id, @RequestBody ReconModelSubImgRequest body){
        ReconModelSubImg reconModelSubImg = reconModelSubImgService.findById(id);

        if (reconModelSubImg == null) {
            throw new ServiceException(ResultCode.DATA_NOT_FOUND);
        }
        reconModelSubImgService.updateReconModelSubImg(reconModelSubImg, body);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("详情")
    @GetMapping("/{id}")
    public Result detail(@PathVariable Integer id) {
        ReconModelSubImg reconModelSubImg = reconModelSubImgService.findById(id);
        return ResultGenerator.genSuccessResult(reconModelSubImg);
    }

    @ApiOperation("列表")
    @GetMapping
    public Result list(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer size,
            ReconModelSubImgQueryParam queryParam) {
        List<ReconModelSubImg> reconModelSubImgs = reconModelSubImgService.listReconModelSubImg(queryParam, page, size);
        PageInfo<ReconModelSubImg> pageInfo = new PageInfo<>(reconModelSubImgs);
        return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
    }
}
