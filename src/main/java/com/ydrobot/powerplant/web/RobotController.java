package com.ydrobot.powerplant.web;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.exception.ServiceException;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.PathPlanning;
import com.ydrobot.powerplant.model.dto.CurrentTaskInfoResult;
import com.ydrobot.powerplant.model.dto.RobotResult;
import com.ydrobot.powerplant.model.request.ControlAuthRequest;
import com.ydrobot.powerplant.service.RobotService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

/**
* Created by Wyf on 2019/04/12.
*/
@Api(tags = "机器人模块")
@RestController
@RequestMapping("/ui/robots")
public class RobotController {
    @Resource
    private RobotService robotService;

    @ApiOperation("列表")
    @GetMapping
    public Result list(@RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer size) {
    		List<RobotResult> robotResults = robotService.listRobot(page, size);
        PageInfo<RobotResult> pageInfo = new PageInfo<>(robotResults);
        return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
    }

    @ApiOperation("获取机器人当前任务信息")
    @ApiImplicitParam(name = "id", value = "机器人id", dataType = "int", paramType = "path")
    @GetMapping("/{id}/current-task")
    public Result getRobotCurrentTask(@PathVariable Integer id) throws ServiceException {
        CurrentTaskInfoResult currentTaskInfoResult = robotService.getRobotCurrentTask(id);
        return ResultGenerator.genSuccessResult(currentTaskInfoResult);
    }

    @ApiOperation("获取控制权限")
    @GetMapping("/{id}/control-auth")
    public Result getControlAuth(@PathVariable Integer id) {
        Map<String, Object> map = robotService.getControlAuth(id);
        return ResultGenerator.genSuccessResult(map);
    }

    @ApiOperation("控制权限确认")
    @PostMapping("/{id}/control-auth")
    public Result handleControlAuth(@PathVariable Integer id,
            @Valid @RequestBody ControlAuthRequest controlAuthRequest) {
        robotService.handleControlAuth(id, controlAuthRequest);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("检查是否有控制权限")
    @GetMapping("/{id}/check-control-auth")
    public Result checkControlAuth(@PathVariable Integer id) {
        Map<String, Object> map = robotService.checkControlAuth(id);
        return ResultGenerator.genSuccessResult(map);
    }

    @ApiOperation("获取一键返航")
    @GetMapping("/{id}/turn-back")
    public Result getTurnBack(@PathVariable Integer id) {
        PathPlanning pathPlanning = robotService.getTurnBack(id);
        return ResultGenerator.genSuccessResult(pathPlanning);
    }
}
