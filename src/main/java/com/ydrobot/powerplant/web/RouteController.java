package com.ydrobot.powerplant.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.Route;
import com.ydrobot.powerplant.model.dto.RouteResult;
import com.ydrobot.powerplant.model.request.RouteMaintainRequest;
import com.ydrobot.powerplant.service.RouteService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
* Created by Wyf on 2019/04/12.
*/
@Api(tags = "道路模块")
@RestController
@RequestMapping("/ui/routes")
public class RouteController {
    @Resource
    private RouteService routeService;

    @ApiOperation("获取所有道路列表")
    @GetMapping()
    public Result list(@RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer size) {
        PageHelper.startPage(page, size);
        List<Route> routes = routeService.findAll();
        PageInfo<Route> pageInfo = new PageInfo<>(routes);
        return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
    }
    
    @ApiOperation("获取所有道路和点关系列表")
    @GetMapping("with-route-pos")
    public Result getRouteWithRoutePos() {
        List<RouteResult> list = routeService.listRouteWithRoutePos();
        return ResultGenerator.genSuccessResult(list);
    }
    
    @ApiOperation("更新")
    @PutMapping("/{id}")
    public Result update(@PathVariable Integer id, @RequestBody Route route) {
        routeService.updateRoute(id,route);
        return ResultGenerator.genSuccessResult();
    }
    
    @ApiOperation("详情")
    @GetMapping("/{id}")
    public Result detail(@PathVariable Integer id) {
    		Route route = routeService.findById(id);
        return ResultGenerator.genSuccessResult(route);
    }
    
    @ApiOperation("添加道路维修")
    @PostMapping("/maintain")
    public Result addRouteMaintain(@RequestBody List<RouteMaintainRequest> routeMaintainRequests) {
    		routeService.saveRouteMaintain(routeMaintainRequests);
    		return ResultGenerator.genSuccessResult();
    }
}
