package com.ydrobot.powerplant.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.RoutePos;
import com.ydrobot.powerplant.service.RoutePosService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
* Created by Wyf on 2019/04/12.
*/
@Api(tags = "道路点模块")
@RestController
@RequestMapping("/ui/route-poss")
public class RoutePosController {
    @Resource
    private RoutePosService routePosService;

    @ApiOperation("获取所有道路点列表")
    @GetMapping()
    public Result list(@RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer size) {
        PageHelper.startPage(page, size);
        List<RoutePos> list = routePosService.findAll();
        PageInfo<RoutePos> pageInfo = new PageInfo<RoutePos>(list);
        return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
    }
    
    @ApiOperation("删除道路点")
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id) {
    		routePosService.deleteRoutePos(id);
        return ResultGenerator.genSuccessResult();
    }
}
