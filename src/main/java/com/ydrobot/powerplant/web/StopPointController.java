package com.ydrobot.powerplant.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.StopPoint;
import com.ydrobot.powerplant.service.StopPointService;

import io.swagger.annotations.Api;

/**
* Created by Wyf on 2019/04/12.
*/
@Api(tags = "停靠点模块")
@RestController
@RequestMapping("/ui/stop-points")
public class StopPointController {
    @Resource
    private StopPointService stopPointService;

    @GetMapping
    public Result list(@RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer size) {
        PageHelper.startPage(page, size);
        List<StopPoint> list = stopPointService.findAll();
        PageInfo<StopPoint> pageInfo = new PageInfo<>(list);
        return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
    }
}
