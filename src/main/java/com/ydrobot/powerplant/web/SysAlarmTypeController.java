package com.ydrobot.powerplant.web;

import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.SysAlarmType;
import com.ydrobot.powerplant.service.SysAlarmTypeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by Wyf on 2020/04/30.
*/
@Api(tags = "系统告警类型模块")
@RestController
@RequestMapping("/ui/sys-alarm-types")
public class SysAlarmTypeController {
    @Resource
    private SysAlarmTypeService sysAlarmTypeService;

    @ApiOperation("获取系统告警类型列表")
    @GetMapping
    public Result list(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer size) {
        PageHelper.startPage(page, size);
        List<SysAlarmType> list = sysAlarmTypeService.findAll();
        PageInfo<SysAlarmType> pageInfo = new PageInfo<SysAlarmType>(list);
        return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
    }
}
