package com.ydrobot.powerplant.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.condition.SysPointAlarmHistoryQueryParam;
import com.ydrobot.powerplant.model.dto.SysPointAlarmHistoryResult;
import com.ydrobot.powerplant.model.request.SysPointAlarmHistoryBatchCheckRequest;
import com.ydrobot.powerplant.service.SysPointAlarmHistoryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Created by Wyf on 2019/04/12.
 */
@Api(tags = "系统点位告警记录模块")
@RestController
@RequestMapping("/ui/sys-point-alarm-historys")
public class SysPointAlarmHistoryController {
	@Resource
	private SysPointAlarmHistoryService sysPointAlarmHistoryService;

	@ApiOperation("列表")
	@GetMapping
	public Result list(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer size,
			SysPointAlarmHistoryQueryParam queryParam) {
		List<SysPointAlarmHistoryResult> sysPointAlarmHistoryResults = sysPointAlarmHistoryService
				.listSysPointAlarmHistory(queryParam, page, size);
		PageInfo<SysPointAlarmHistoryResult> pageInfo = new PageInfo<>(sysPointAlarmHistoryResults);
		return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
	}
	
	@ApiOperation("批量审核")
	@PutMapping("/batch-check")
	public Result batchCheck(@RequestBody SysPointAlarmHistoryBatchCheckRequest request) {
		sysPointAlarmHistoryService.batchCheck(request);
		return ResultGenerator.genSuccessResult();
	}
	
	@ApiOperation("获取设备报警数")
	@GetMapping("/alarm-num")
	public Result getAlarmNum() {
		Integer alarmNum = sysPointAlarmHistoryService.getAlarmNum();
		Map<String, Object> map = new HashMap<>();
		map.put("alarmNum", alarmNum);
		return ResultGenerator.genSuccessResult(map);
	}
}
