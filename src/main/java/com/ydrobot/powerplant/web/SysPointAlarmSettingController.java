package com.ydrobot.powerplant.web;

import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.SysPointAlarmSetting;
import com.ydrobot.powerplant.model.condition.SysPointAlarmSettingQueryParam;
import com.ydrobot.powerplant.service.SysPointAlarmSettingService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Wyf on 2020/04/27.
 */
@Api(tags = "系统点位报警设置模块")
@RestController
@RequestMapping("/ui/sys-point-alarm-settings")
public class SysPointAlarmSettingController {
	@Resource
	private SysPointAlarmSettingService sysPointAlarmSettingService;

	@ApiOperation("添加")
	@PostMapping
	public Result add(@RequestBody SysPointAlarmSetting sysPointAlarmSetting) {
		sysPointAlarmSettingService.save(sysPointAlarmSetting);
		return ResultGenerator.genSuccessResult();
	}

	@ApiOperation("删除")
	@DeleteMapping("/{id}")
	public Result delete(@PathVariable Integer id) {
		sysPointAlarmSettingService.deleteById(id);
		return ResultGenerator.genSuccessResult();
	}

	@ApiOperation("更新")
	@PutMapping
	public Result update(@RequestBody SysPointAlarmSetting sysPointAlarmSetting) {
		sysPointAlarmSettingService.update(sysPointAlarmSetting);
		return ResultGenerator.genSuccessResult();
	}

	@ApiOperation("详情")
	@GetMapping("/{id}")
	public Result detail(@PathVariable Integer id) {
		SysPointAlarmSetting sysPointAlarmSetting = sysPointAlarmSettingService.findById(id);
		return ResultGenerator.genSuccessResult(sysPointAlarmSetting);
	}

	@ApiOperation("列表")
	@GetMapping
	public Result list(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer size,
			SysPointAlarmSettingQueryParam queryParam) {
		List<SysPointAlarmSetting> sysPointAlarmSettings = sysPointAlarmSettingService.listSysPointAlarmSetting(queryParam, page, size);
		PageInfo<SysPointAlarmSetting> pageInfo = new PageInfo<>(sysPointAlarmSettings);
		return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
	}
}
