package com.ydrobot.powerplant.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.SystemSetting;
import com.ydrobot.powerplant.model.request.SystemSettingBatchRequest;
import com.ydrobot.powerplant.service.SystemSettingService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
* Created by Wyf on 2019/06/19.
*/
@Api(tags = "系统设置模块")
@RestController
@RequestMapping("/ui/system-settings")
public class SystemSettingController {
    @Resource
    private SystemSettingService systemSettingService;

    @ApiOperation("批量更新")
    @PutMapping("/batch")
    public Result batchUpdate(@RequestBody SystemSettingBatchRequest body) {
        systemSettingService.batchUpdate(body);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("列表")
    @GetMapping
    public Result list(@RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer size) {
        PageHelper.startPage(page, size);
        List<SystemSetting> list = systemSettingService.findAll();
        PageInfo<SystemSetting> pageInfo = new PageInfo<>(list);
        return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
    }
}
