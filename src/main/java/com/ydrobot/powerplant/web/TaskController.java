package com.ydrobot.powerplant.web;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.exception.ServiceException;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultCode;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.PathPlanning;
import com.ydrobot.powerplant.model.Point;
import com.ydrobot.powerplant.model.StopPoint;
import com.ydrobot.powerplant.model.Task;
import com.ydrobot.powerplant.model.TaskPoint;
import com.ydrobot.powerplant.model.condition.TaskQueryParam;
import com.ydrobot.powerplant.model.dto.TaskResult;
import com.ydrobot.powerplant.model.condition.TaskExecutePlanQueryParam;
import com.ydrobot.powerplant.model.condition.TaskMonthShowQueryParam;
import com.ydrobot.powerplant.model.request.DeleteExecutePlanBatchRequest;
import com.ydrobot.powerplant.model.request.TaskPathRequest;
import com.ydrobot.powerplant.model.request.TaskRequest;
import com.ydrobot.powerplant.service.TaskPointService;
import com.ydrobot.powerplant.service.TaskService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
* Created by Wyf on 2019/04/12.
*/
@Api(tags = "任务模块")
@RestController
@RequestMapping("/ui/tasks")
public class TaskController {
    @Resource
    private TaskService taskService;
    @Resource
    private TaskPointService taskPointService;

    @ApiOperation("添加")
    @PostMapping
    public Result add(@RequestBody TaskRequest taskRequest) {
        Task task = taskService.saveTask(taskRequest);
        return ResultGenerator.genSuccessResult(task);
    }

    @ApiOperation("删除")
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id){
        Task task = taskService.findById(id);
        if (task == null) {
            throw new ServiceException(ResultCode.TASK_NOT_FOUND);
        }
        taskService.deleteTask(task);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("更新")
    @PutMapping("/{id}")
    public Result update(@PathVariable Integer id, @RequestBody TaskRequest taskRequest) {
        Task task = taskService.findById(id);
        if (task == null) {
            throw new ServiceException(ResultCode.TASK_NOT_FOUND);
        }
        taskService.updateTask(task, taskRequest);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("详情")
    @GetMapping("/{id}")
    public Result detail(@PathVariable Integer id) {
        Task task = taskService.findById(id);
        return ResultGenerator.genSuccessResult(task);
    }

    @ApiOperation("获取任务列表")
    @GetMapping
    public Result listTask(@RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer size, TaskQueryParam queryParam) {
    		List<TaskResult> taskResults = taskService.listTask(queryParam, page, size);
        PageInfo<TaskResult> pageInfo = new PageInfo<>(taskResults);
        return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
    }

    @ApiOperation("获取任务点位列表")
    @GetMapping("/{id}/points")
    public Result listTaskPoint(@PathVariable Integer id) {
        List<TaskPoint> taskPoints = taskService.listTaskPoint(id);
        return ResultGenerator.genSuccessResult(taskPoints);
    }

    @ApiOperation("获取任务是否可编辑")
    @GetMapping("/{id}/editStatus")
    public Result getEditStatus(@PathVariable Integer id){
        Task task = taskService.findById(id);
        if (task == null) {
            throw new ServiceException(ResultCode.TASK_NOT_FOUND);
        }
        boolean editStatus = taskService.getEditStatus(task);
        Map<String, Object> map = new HashMap<>();
        map.put("editStatus", editStatus);
        return ResultGenerator.genSuccessResult(map);
    }

    @ApiOperation("获取任务按月份展示")
    @GetMapping("/month-show")
    public Result listTaskMonthShow(TaskMonthShowQueryParam queryParam) {
        if (StringUtils.isBlank(queryParam.getExecuteTime())) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
            queryParam.setExecuteTime(sdf.format(new Date()));
        }

        List<Map<String, Object>> maps = taskService.listTaskMonthShow(queryParam);
        return ResultGenerator.genSuccessResult(maps);
    }

    @ApiOperation("获取任务执行计划列表")
    @GetMapping("/execute-plan")
    public Result listTaskExecutePlan(TaskExecutePlanQueryParam queryParam) {
        List<Map<String, Object>> maps = taskService.listTaskExecutePlan(queryParam);
        return ResultGenerator.genSuccessResult(maps);
    }

    @ApiOperation("删除任务执行计划")
    @PostMapping("/delete-execute-plan")
    public Result deleteExecutePlan(@RequestBody DeleteExecutePlanBatchRequest deleteExecutePlanBatchRequest)
            throws ServiceException {
        taskService.deleteExecutePlan(deleteExecutePlanBatchRequest);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("判断当前任务是否可执行")
    @GetMapping("/{id}/execut-status")
    public Result getTaskExecutStatus(@PathVariable Integer id) {
        boolean flag = taskService.getTaskExecutStatus(id);
        Map<String, Object> map = new HashMap<>();
        map.put("status", flag);
        return ResultGenerator.genSuccessResult(map);
    }

    @ApiOperation("获取某个任务的路径")
    @GetMapping("/{id}/path")
    public Result getTaskPath(@PathVariable Integer id) {
        PathPlanning pathPlanning = taskService.getTaskPath(id);
        return ResultGenerator.genSuccessResult(pathPlanning);
    }

    @ApiOperation("更新某个任务的路径")
    @PutMapping("/{id}/path")
    public Result updateTaskPath(@PathVariable Integer id, @RequestBody TaskPathRequest taskPathRequest){
        taskService.updateTaskPath(id, taskPathRequest);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("获取某个任务的巡检点")
    @GetMapping("/{id}/check-points")
    public Result listTaskCheckPoints(@PathVariable Integer id) {
        List<Point> points = taskService.listTaskCheckPoints(id);
        return ResultGenerator.genSuccessResult(points);
    }

    @ApiOperation("获取某个任务的停靠点")
    @GetMapping("/{id}/stop-points")
    public Result listTaskStopPoint(@PathVariable Integer id) {
        List<StopPoint> stopPoints = taskService.listTaskStopPoint(id);
        return ResultGenerator.genSuccessResult(stopPoints);
    }
}
