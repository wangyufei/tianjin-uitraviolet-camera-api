package com.ydrobot.powerplant.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.exception.ServiceException;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultCode;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.TaskCycle;
import com.ydrobot.powerplant.model.condition.TaskCycleQueryParam;
import com.ydrobot.powerplant.model.request.TaskCycleRequest;
import com.ydrobot.powerplant.service.TaskCycleService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Created by Wyf on 2019/04/12.
 */
@Api(tags = "任务周期执行模块")
@RestController
@RequestMapping("/ui/task-cycles")
public class TaskCycleController {
	@Resource
	private TaskCycleService taskCycleService;

	@ApiOperation("添加")
	@PostMapping
	public Result add(@RequestBody TaskCycleRequest taskCycleRequest) {
		taskCycleService.createTaskCycle(taskCycleRequest);
		return ResultGenerator.genSuccessResult();
	}

	@ApiOperation("删除")
	@DeleteMapping("/{id}")
	public Result delete(@PathVariable Integer id) {
		taskCycleService.deleteById(id);
		return ResultGenerator.genSuccessResult();
	}

	@ApiOperation("更新")
	@PutMapping("/{id}")
	public Result update(@PathVariable Integer id, @RequestBody TaskCycleRequest taskCycleRequest)
			throws ServiceException {
		TaskCycle taskCycle = taskCycleService.findById(id);
		if (taskCycle == null) {
			throw new ServiceException(ResultCode.DATA_NOT_FOUND);
		}

		taskCycleService.updateTaskCycle(taskCycle, taskCycleRequest);
		return ResultGenerator.genSuccessResult();
	}

	@ApiOperation("详情")
	@GetMapping("/{id}")
	public Result detail(@PathVariable Integer id) {
		TaskCycle taskCycle = taskCycleService.findById(id);
		return ResultGenerator.genSuccessResult(taskCycle);
	}

	@ApiOperation("列表")
	@GetMapping
	public Result list(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer size,
			TaskCycleQueryParam queryParam) {
		List<TaskCycle> taskCycles = taskCycleService.listTaskCycle(queryParam, page, size);
		PageInfo<TaskCycle> pageInfo = new PageInfo<>(taskCycles);
		return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
	}
}
