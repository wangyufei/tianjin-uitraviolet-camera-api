package com.ydrobot.powerplant.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.exception.ServiceException;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultCode;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.TObject;
import com.ydrobot.powerplant.model.TaskHistory;
import com.ydrobot.powerplant.model.TaskPoint;
import com.ydrobot.powerplant.model.condition.TaskHistoryQueryParam;
import com.ydrobot.powerplant.model.dto.TaskHistoryResult;
import com.ydrobot.powerplant.model.request.TaskHistoryRequest;
import com.ydrobot.powerplant.service.TaskHistoryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
* Created by Wyf on 2019/04/12.
*/
@Api(tags = "任务执行历史记录模块")
@RestController
@RequestMapping("/ui/task-historys")
public class TaskHistoryController {
    @Resource
    private TaskHistoryService taskHistoryService;

    @ApiOperation("更新")
    @PutMapping("/{id}")
    public Result update(@PathVariable Integer id, @RequestBody TaskHistoryRequest body) {
        TaskHistory taskHistory = taskHistoryService.findById(id);

        if (taskHistory == null) {
            throw new ServiceException(ResultCode.DATA_NOT_FOUND);
        }

        taskHistoryService.updateTaskHistory(taskHistory, body);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("详情")
    @GetMapping("/{id}")
    public Result detail(@PathVariable Integer id) {
        TaskHistory taskHistory = taskHistoryService.findById(id);
        return ResultGenerator.genSuccessResult(taskHistory);
    }

    @ApiOperation("列表")
    @GetMapping
    public Result list(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer size,
            TaskHistoryQueryParam queryParam) {
    		List<TaskHistoryResult> taskHistoryResults = taskHistoryService.listTaskHistory(queryParam, page, size);
        PageInfo<TaskHistoryResult> pageInfo = new PageInfo<>(taskHistoryResults);
        return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
    }

    @ApiOperation("巡检报告导出")
    @GetMapping("/exports")
    public void exports(@RequestParam(required = true) String taskHistoryIds, HttpServletResponse response) {
        if (StringUtils.isBlank(taskHistoryIds)) {
            throw new ServiceException("taskHistoryIds不能为空");
        }
        taskHistoryService.exportReport(taskHistoryIds, response);
    }

    @ApiOperation("获取当前执行任务的报警数量")
    @GetMapping("/current-task-alarm-num")
    public Result getCurrentTaskAlarmNum(@RequestParam(required = true) Integer robotId) {
        Integer currentTaskAlarNum = taskHistoryService.getCurrentTaskAlarmNum(robotId);
        Map<String, Object> map = new HashMap<>();
        map.put("currentTaskAlarmNum", currentTaskAlarNum);
        return ResultGenerator.genSuccessResult(map);
    }

    @ApiOperation("获取任务点位列表")
    @GetMapping("/points")
    public Result listTaskPoint(@RequestParam(required = true) Integer taskHistoryId) {
        List<TaskPoint> taskPoints = taskHistoryService.listTaskPoint(taskHistoryId);
        return ResultGenerator.genSuccessResult(taskPoints);
    }
    
    @ApiOperation("获取某个任务的最后一个巡检对象")
    @GetMapping("/{id}/last-patrol-object")
    public Result getLastPatrolObject(@PathVariable Integer id) {
    		TObject object = taskHistoryService.getLastPatrolObject(id);
    		return ResultGenerator.genSuccessResult(object);
    }
    
    @ApiOperation("获取某个任务的对象巡检顺序")
    @GetMapping("/{id}/patrol-objects")
    public Result getPatrolObjects(@PathVariable Integer id) {
    		List<TObject> objects = taskHistoryService.getPatrolObjects(id);
    		return ResultGenerator.genSuccessResult(objects);
    }
    
    @ApiOperation("获取某个任务的已巡检对象")
    @GetMapping("/{id}/already-patrol-objects")
    public Result getAlreadyPatrolObjects(@PathVariable Integer id) {
    		List<TObject> objects = taskHistoryService.getAlreadyPatrolObjects(id);
    		return ResultGenerator.genSuccessResult(objects);
    }
}
