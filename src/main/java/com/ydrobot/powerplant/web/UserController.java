package com.ydrobot.powerplant.web;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.common.ThreadCache;
import com.ydrobot.powerplant.core.constant.CacheConsts;
import com.ydrobot.powerplant.core.exception.ServiceException;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultCode;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.core.utils.ProjectCommonUtils;
import com.ydrobot.powerplant.core.utils.ProjectDateUtils;
import com.ydrobot.powerplant.core.utils.ProjectMD5Utils;
import com.ydrobot.powerplant.model.Permission;
import com.ydrobot.powerplant.model.User;
import com.ydrobot.powerplant.model.condition.UserQueryParam;
import com.ydrobot.powerplant.model.dto.UserResult;
import com.ydrobot.powerplant.model.request.LoginRequest;
import com.ydrobot.powerplant.model.request.UserRequest;
import com.ydrobot.powerplant.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Created by Wyf on 2019/04/12.
 */
@Api(tags = "用户模块")
@RestController
@RequestMapping("/ui")
public class UserController {
	@Resource
	private UserService userService;

	@ApiOperation("登录接口")
	@PostMapping("/users/login")
	public Result login(@Valid @RequestBody LoginRequest loginRequest, HttpServletRequest request) {

		// 通过用户账号查询用户信息
		User user = userService.getUserByAccount(loginRequest.getAccount());
		if (user == null) {
			throw new ServiceException(ResultCode.ACCOUNT_NOT_FOUND);
		}

		if (user.getLoginForbiddenTime() != null && user.getLoginForbiddenTime().getTime() > new Date().getTime()) {
			throw new ServiceException(ResultCode.LOGIN_FAIL_NUM_OVERRUN);
		}

		if (!ProjectMD5Utils.getMD5String(loginRequest.getPassword()).equals(user.getPassword())) {
			Integer loginFailNum = user.getLoginFailNum() + 1;
			if (loginFailNum < 5) {
				// 更新用户登录失败次数
				user.setLoginFailNum(loginFailNum);
				userService.update(user);
			} else {
				Calendar nowTime = Calendar.getInstance();
				nowTime.add(Calendar.MINUTE, 30);
				user.setLoginFailNum(loginFailNum);
				user.setLoginForbiddenTime(nowTime.getTime());
				userService.update(user);
			}

			throw new ServiceException(ResultCode.LOGIN_FAIL);
		}

		if (ProjectDateUtils.getIntervalDays(user.getValidityTime(), new Date()) > 0) {
			throw new ServiceException(ResultCode.ACCOUNT_EXPIRE);
		}

		String token = userService.getExistToken(user);
		if (token == null) {
			token = userService.getToken(user);
		}

		// 更新用户登录信息
		user.setLastLoginTime(new Date());
		user.setLoginIp(ProjectCommonUtils.getIpAddress(request));
		user.setLoginFailNum(0);
		userService.update(user);

		Map<String, Object> map = new HashMap<>();
		map.put("token", token);
		map.put("expires", CacheConsts.TOKEN_TIMEOUT);
		map.put("userId", user.getId());
		return ResultGenerator.genSuccessResult(map);
	}

	@ApiOperation("注销接口")
	@DeleteMapping("/users/logout")
	public Result logout(HttpServletRequest request) {
		String token = request.getHeader("token");
		userService.logout(token);
		return ResultGenerator.genSuccessResult();
	}

	@ApiOperation("获取当前用户信息")
	@GetMapping("/user")
	public Result getUserInfo() {
		User user = userService.findById(ThreadCache.getUser().getId());
		return ResultGenerator.genSuccessResult(user);
	}

	@ApiOperation("添加")
	@PostMapping("/users")
	public Result add(@Valid @RequestBody UserRequest userRequest) {
		userService.saveUser(userRequest);
		return ResultGenerator.genSuccessResult();
	}

	@ApiOperation("删除")
	@DeleteMapping("/users/{id}")
	public Result delete(@PathVariable Integer id) {
		userService.deleteById(id);
		return ResultGenerator.genSuccessResult();
	}

	@ApiOperation("更新")
	@PutMapping("/users/{id}")
	public Result update(@PathVariable Integer id, @RequestBody UserRequest body) {
		User user = userService.findById(id);

		if (user == null) {
			throw new ServiceException(ResultCode.DATA_NOT_FOUND);
		}
		userService.updateUser(user, body);
		return ResultGenerator.genSuccessResult();
	}

	@ApiOperation("详情")
	@GetMapping("/users/{id}")
	public Result detail(@PathVariable Integer id) {
		User user = userService.findById(id);
		return ResultGenerator.genSuccessResult(user);
	}

	@ApiOperation("列表")
	@GetMapping("/users")
	public Result list(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer size,
			UserQueryParam queryParam) {
		List<UserResult> userResults = userService.listUser(queryParam, page, size);
		PageInfo<UserResult> pageInfo = new PageInfo<>(userResults);
		return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
	}

	@ApiOperation("获取用户权限列表")
	@GetMapping("/user/permissions")
	public Result getUserPermissions() {
		User user = userService.findById(ThreadCache.getUser().getId());
		List<Permission> permissions = userService.listUserPermission(user);
		return ResultGenerator.genSuccessResult(permissions);
	}
}
