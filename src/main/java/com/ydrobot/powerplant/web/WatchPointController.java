package com.ydrobot.powerplant.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.WatchPoint;
import com.ydrobot.powerplant.service.WatchPointService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
* Created by Wyf on 2019/04/12.
*/
@Api(tags = "观察点模块")
@RestController
@RequestMapping("/ui/watch-points")
public class WatchPointController {
    @Resource
    private WatchPointService watchPointService;

    @GetMapping
    public Result list(@RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer size) {
        PageHelper.startPage(page, size);
        List<WatchPoint> list = watchPointService.findAll();
        PageInfo<WatchPoint> pageInfo = new PageInfo<WatchPoint>(list);
        return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
    }
    
    @ApiOperation("添加")
    @PostMapping
    public Result add(@RequestBody WatchPoint body) {
    		watchPointService.save(body);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("删除")
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id) {
    		watchPointService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("更新")
    @PutMapping("/{id}")
    public Result update(@PathVariable Integer id,@RequestBody WatchPoint body) {
    		watchPointService.updateWatchPoint(id, body);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("详情")
    @GetMapping("/{id}")
    public Result detail(@PathVariable Integer id) {
        WatchPoint watchPoint = watchPointService.findById(id);
        return ResultGenerator.genSuccessResult(watchPoint);
    }
}
